# 🦄 Pixelschubsen


## 🏗️ Build the Project

Go to the `source` directory and run the following command:

```txt
dotnet publish -p:PublishProfile=FolderProfile
```

The resulting binary will be located here:

```txt
fhws.pixelschubser.jpg.encoder\bin\Release\netcoreapp3.1\publish\fhws.pixelschubser.jpg.encoder.exe
```