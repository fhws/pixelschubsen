# TODO



## Generell

- Transformation RGB -> YCbCr mit offset 128

  > - [2020-12-02 08-15-57 @ 3:42]

- Unterabtastung fixen
- DCT auf jedem 8x8 Block jedes Farbkanals
- Padding am rand fixen? (Lisa hat was gesagt da wäre ein Bug)



## Quantisierung

- Es gibt eine 8x8 Matrix Q mit festgelegten Werten
- Jeder DCT Block wird elementweise durch Q geteilt
  - Ergebnis jedes Elements immer ganzzahlig => (effizient) runden

Marker **DQT** schreibt Quantisierungstabelle in Bild

>- [2020-12-02 08-15-57 @ 7:57]



## Zick-Zack-Abtastung

>- [2020-12-02 08-15-57 @ 10:55]

Lookup Tabelle (einfach) oder algorithmisch


### DC-Koeffizient

Der erste Koeffizient (erster Wert in der Liste die aus der
Zick-Zack-Abtastung entsteht) => homogener Anteil

DC-Koeffizienten eines Farbkanals werden aus den Blöcken von links oben nach
rechts unten "eingesammelt". Die daraus entstandene Liste wird mit einer
Einfachen Vorhersage kodiert: *Man nimmt immer den vorherigen Wert an und
speichert den Fehler*:

\[
    \Delta D_i = DC_i - DC_{i-1} \quad \textrm{mit} \quad DC_{-1} = 0
\]

DC-Koeffizienten: `20, 18, 11, 14, 8 , 7, 7, 9, 17`

Vorhersage: `20, -2, -7, 3, -6, -1, 0, 2, 8`

#### Kodierung mit "Kategorien-Bitrepräsentation" und Huffman

> Siehe *Kategorien-Bitrepräsentation Kodierung*

Jeder DC-Koeffizient wird mit *Kategorien-Bitrepräsentation* und die
jeweiligen Kategorien dann noch einmal mit Huffman kodiert:


Aus: `20, -2, -7, 3, -6, -1, 0, 2, 8`

Wird zunächst: `(5, 10100), (2, 01), (3, 000), (2, 11), ...`

Angenommen

- `5` ist Huffman `111010`
- `3` ist Huffman `1100`
- `2` ist Huffman `0010`

Dann entsteht:

```txt
111010 10100 0010 01 1100 000 0010 11 ...
            |       |        |       |
```


### AC-Koeffizienten

Die restlichen 63 Koeffizienten, ohne den DC-Koeffizienten.

Werden Lauflängen-Kodiert:

- Tupel mit `("Anzahl zusammenhängende Nullen vor Zahl", "Zahl")`
- *EOB*: Wenn bis zum Ende nur noch Nullen kommen, markiert `(0, 0)` den *End Of Block*.
  - Wenn Block nicht mit null endet, wird kein *EOB* geschrieben

Z.B.: `57, 10, 0, 0, 5, 0, 1, 0, 0, 0, 0, 0`
wird zu `(0, 57), (0, 10), (2, 5), (1, 1), (0, 0)`

Da für den ersten Wert des Tupels nur 4 Bit verfügbar sinn, können nur 15
Nullen vor einer Zahl gezählt werden. Wenn mehr als 15 Nullen vor einer Zahl
stehen, müssen *16 Nullen* als `(15, 0)` (Null-Tupel) kodiert werden. Das Tupel
mit der Zahl *ungleich Null* "zählt" dann nur die Nullen, die noch nicht in
einem Null-Tupel verwendet wurden.

Z.B.: `10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0`
wird zu `(0, 10), (15, 0), (3, 5), (0, 0)`


#### Kodierung mit "Kategorien-Bitrepräsentation"

Die Werte der Lauflängenkodierung (2. Tupel-Element) werden als
"Kategorien-Bitrepräsentation" gespeichert.

> Siehe *Kategorien-Bitrepräsentation Kodierung*

Aus dem obigen Beispiel `(0, 10), (15, 0), (4, 5), (0, 0)` wird also
`(0, (4, 1000)), (15, (0, _)), (4, (3, 101)), (0, (0, _))`.



#### Huffman Kodierung

Das verschachtelte Tupel aus der "Kategorien-Bitrepräsentation" wird dann
umgestellt, sodass sich eine Liste aus Tupeln mit `("Anzahl Nullen vor Zahl",
"Kategorie")` und der entsprechenden Bitfolgen ergibt:

`(0, 4), 1000, (15, 0), (4, 3), 101, (0, 0)`

Die Tupel bestehen jetzt aus zwei 4-Bit Zahlen => sind ein Byte groß.

Für den _Y-Kanal_, werden dann alle diese Tupel über alle Blöcke des Kanals mit
Huffman kodiert.

Für die _Cb und Cr Kanäle_, wird eine gemeinsame Huffman-Kodierung aus allen
Tupeln aller Blöcke erstellt.

> **Bsp.:**
>
> Huffman Codes:
>
> - (0, 4) => `0x04` = Huffman Code: `111000`
> - (15, 0) => `0xf0` = Huffman Code: `11001000`
> - (4, 3) => `0x43` = Huffman Code: `110110`
> - (0, 0) => `0xf0` = Huffman Code: `1010` (EOB)
>
> ```txt
> => 111000 1000 11001000 110110 101 1010
>           ----|        |       ---|
> ```


### DC-AC-Bitstrom Schreiben

Der Bitstrom für einen DCT-Block beginnt mit dem kodierten DC-Koeffizienten
des Blocks, auf den dann die kodierten AC-Koeffizienten dieses Blocks folgen.

Durch die Unterabtastung der Cb und Cr Kanäle, deckt ein Block dieser beiden
Kanäle so viele Pixel ab, wie 4 Blöcke des Y-Kanals (4 8x8 Blöcke Y => 16x16,
1 8x8 Block Cb => 16x16, 1 8x8 Block Cr => 16x16).

Es werden also immer 16x16 Blöcke geschrieben: ($ Y_1, Y_2, Y_3, Y_4, Cb, Cr $)

Im 16x16 block ist:

- $Y_1$: links oben
- $Y_2$: rechts oben
- $Y_3$: links unten
- $Y_4$: rechts unten

> **ACHTUNG:**
>
> Wenn `0xff` geschrieben werden soll, muss statdessen `0xff00` geschrieben
> werden um zu verhindern, dass es wie ein Segmentmarker aussieht.

Man schreibt 4 Huffman Segmente:

- Y-DC
- Y-AC
- CbCr-DC
- CbCr-AC



## Kategorien-Bitrepräsentation Kodierung

>- [2020-12-02 08-15-57 @ 23:23]

- Alle Zahlen von `-32767` bis `+32767` werden in Kategorien (0 - 15) aufgeteilt.
- Die Kategorie ist eine 4-Bit zahl, das Bitmuster ist je nach Kategorie unterschiedlich lang (Index der Kategorie = Bitlänge).
- Kategorie 0 repräsentiert die `0` und ist 0 Bit lang
- Kategorie 1 repräsentiert die `-1` und `+1` und ist 1 Bit lang, (`0`, `1`)
- Kategorie 2 repräsentiert die `-3`, `-2`, `+2` und `+2` und ist 2 Bit lang, (`00`, `01`, `10`, `11`)
- ...

> **Faustregel**:
>
> Um eine Kategorie zu bestimmen, schaut man sich den Betrag der Zahl an und
> überlegt, mit wie vielen Bit dieser in normaler Binärdarstellung kodiert
> werden kann:
>
> - `8` => `1000` => 4 Bit => Kategorie 4
> - `18` => `10010` => 5 Bit => Kategorie 5
>
> Positive zahlen können dann einfach Binär kodiert werden. (`9` => `1001`).
> Für negative Zahlen kodiert man den Betrag einfach Binär und bildet dann das Komplement (`-9` => `|-9|` => `~1001` => `0110`)

*TODO:*

- [ ] Funktion schreiben, die Zahl [-32767; 32767] entgegen nimmt und
      Kategorie + Bitmuster ausgibt.