%img_vgl = imread('RGB16Million.tif/RGB16Million.tif');

% Create all colors image
img=zeros(4096);
img = uint8(img);

for x = 1:4096
  for y = 1:4096
      img(y, x, 1) = mod(x-1, 256); % R
      img(y, x, 2) = mod(y-1, 256); % G
      img(y, x, 3) = fix((x-1)/256) + fix((y-1)/256)*16 ; % B
  end
end

%imshow(img)
%isequal(img_vgl,img) %true

%imwrite(img,'RGB16Million.ppm') % binary
imwrite(img,'RGB16Million2.ppm') % ASCII
