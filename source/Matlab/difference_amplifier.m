
% img1_ycbcr = imread('test_matlab_YCBCR_16Million.ppm');
% img2_ycbcr = imread('test_octave_YCBCR_16Million.ppm');
img1_ycbcr = imread('Gardasee Abschlussfahrt.jpg');
% img1_ycbcr = rgb2ycbcr(img1);
img3_ycbcr = imread('Gardasee Abschlussfahrt_m.jpg');
disp('oo')

differences = bsxfun(@minus, img1_ycbcr, img3_ycbcr);
amplified = min(differences*20, 255);
imwrite(amplified,'amplified.tif')
imwrite(differences,'differences.tif')
% imshow(amplified);
