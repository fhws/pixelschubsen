img = imread('RGB16Million.ppm');

img1 = imread('test_matlab_rgb16Million.ppm');
img2 = imread('test_octave_rgb16Million.ppm');
img3 = imread('test_nomacs_rgb16Million.ppm');
img4 = imread('test_irfanview_rgb16Million.ppm');
img5 = imread('test_photoshop_rgb16Million.ppm');
img6 = imread('test_gimp_rgb16Million.ppm');
img7 = imread('test_irfanview_ASCII_rgb16Million.ppm');

isequal(img, img1, img2, img3, img4, img5, img6, img7) % checked for single bit flip to trigger false!
disp("all are equal - all are generated from the RGB16Million.ppm and saved as binary ppm's");

% photoshop kann kein YCbCr & habe auf die schnelle auch keine ascii ppm speicher funktion gefunden.
% nomacs kann kein YCbCr
% gimp kann kein YCbCr, es kann decomposen aber mehr auch nicht... dann kann man es so nicht speichern..
% octave fehler?
% matlab fehler?
% irfanview kann kein YCbCr

% octave kann nicht als ASCII speichern

img1_ycbcr = imread('test_matlab_YCBCR_16Million.ppm');
img2_ycbcr = imread('test_octave_YCBCR_16Million.ppm');

isequal(img1_ycbcr, img2_ycbcr) % nope
% octave und matlab haben unterschiedliche ergebnisse! :(

% ist aber vl. auch garnicht weiter schlim... es sind rundungsfehler, und
% das verfahren ist sowieso verlust behaftet (wenn numerisch gerechnet)...
% visualisierbar durch: matlab_RGB2YCbCr2RGB_test.m

