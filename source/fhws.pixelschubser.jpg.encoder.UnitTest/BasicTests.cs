using System;
using System.IO;
using fhws.pixelschubser.jpg.encoder.Structures;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest
{
    [TestFixture]
    public class BasicTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var context = TestContext.CurrentContext;
            Directory.SetCurrentDirectory(context.WorkDirectory + "/../../..");
        }

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestFrameworkIsWorking()
        {
            Assert.That(true == true, Is.True);
        }

        [Test]
        public void ShowTestDirectoriePaths()
        {
            TestContext.WriteLine("CurrentDirectory: \t"
                + Directory.GetCurrentDirectory());
            TestContext.WriteLine("WorkDirectory: \t"
                + TestContext.CurrentContext.WorkDirectory);

            Assert.Pass();
        }

        [Test]
        public void RawReadPPMFileContent()
        {
            string text = File.ReadAllText("test-assets/test.ppm");
            TestContext.WriteLine("\n" + text);

            Assert.That(text.Length >= 20);
        }

    }
}