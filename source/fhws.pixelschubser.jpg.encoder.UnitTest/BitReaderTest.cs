using System;
using System.IO;
using NUnit.Framework;
using static fhws.pixelschubser.jpg.encoder.UnitTest.TestData.LargeRandomBits;
using static fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers.BitBufferAccess;

namespace fhws.pixelschubser.jpg.encoder.UnitTest
{
    [TestFixture]
    public class BitReaderTest
    {
        [Test]
        public void ConstructWithBuffer()
        {
            new BitReader(new byte[] { 13, 37 });
        }

        [Test]
        public void ConstructWithStream()
        {
            new BitReader(new MemoryStream());
        }

        [Test]
        public void GeneratesStreamFromBuffer()
        {
            var origBuffer = new byte[] { 13, 37 };
            var reader = new BitReader(origBuffer);
            Assert.That(reader.BaseStream, Is.InstanceOf<Stream>());

            var stream = reader.BaseStream;

            // Reset stream and get it's data
            stream.Position = 0;
            var data = new byte[stream.Length];
            stream.Read(data);

            // Check that the stream contains the data of the original buffer
            Assert.That(data, Is.EqualTo(origBuffer));

            // Modify the origBuffer to ensure the BaseStream does nut just make
            // a copy of it
            origBuffer[1] = 0;

            // Reset stream and get it's data
            stream.Position = 0;
            data = new byte[stream.Length];
            stream.Read(data);

            // Check that the streams data is now equal to the changed buffer
            Assert.That(data, Is.EqualTo(origBuffer));
        }

        [Test]
        public void UsesProvidedStreamAsBaseStream()
        {
            var stream = new MemoryStream();
            var reader = new BitReader(stream);

            Assert.That(reader.BaseStream, Is.SameAs(stream));
        }

        [Test]
        public void ReadFirstByteAsBits()
        {
            var buf = new byte[] { 0b_1001_0100 };
            var reader = new BitReader(buf);

            var data = new byte[8];
            for (var i = 0; i < data.Length; i++)
            {
                data[i] = reader.ReadBit();
            }

            Assert.That(data, Is.EqualTo(new byte[] { 1, 0, 0, 1, 0, 1, 0, 0 }));
        }

        [Test]
        public void ReadMultipleBytesAsBits()
        {
            var buf = new byte[] {
                0b_1001_0100, 0b_0101_0101, 0b_1111_0000, 0b_1000_0001 };
            var reader = new BitReader(buf);

            var data = new byte[buf.Length * 8];
            for (var i = 0; i < data.Length; i++)
            {
                data[i] = reader.ReadBit();
            }

            Assert.That(data, Is.EqualTo(new byte[] {
                1, 0, 0, 1, 0, 1, 0, 0,
                0, 1, 0, 1, 0, 1, 0, 1,
                1, 1, 1, 1, 0, 0, 0, 0,
                1, 0, 0, 0, 0, 0, 0, 1,
            }));
        }

        [TestCase(0b_1000_0000, 1)]
        [TestCase(0b_0111_1111, 0)]
        public void FirstBit(byte data, byte resultBit)
        {
            var buf = new byte[] { data };
            var reader = new BitReader(buf);
            var bit = reader.ReadBit();

            Assert.That(bit, Is.EqualTo(resultBit));
        }

        [TestCase(0b_0000_0001, 1)]
        [TestCase(0b_1111_1110, 0)]
        public void LastBit(byte data, byte resultBit)
        {
            var buf = new byte[] { data };
            var reader = new BitReader(buf);
            byte bit = 1; // Use opposite of expected value
            // Get 8th bit
            for (var i = 0; i < 8; i++)
            {
                bit = reader.ReadBit();
            }

            Assert.That(bit, Is.EqualTo(resultBit));
        }

        [Test]
        public void LargeRandomBitsTest()
        {
            var reader = new BitReader(LargeRandBitsBuffer);
            var result = new byte[LargeRandBits.Length];
            for (var i = 0; i < result.Length; i++)
            {
                result[i] = GetBitFromBuffer(LargeRandBitsBuffer, i);
            }

            Assert.That(result, Is.EqualTo(LargeRandBits));
        }

        [TestCase(new byte[] { 0b_0101_0101 }, 0)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_1111_0000 }, 1)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_1111_0000 }, 0)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_0101_0111, 0b_1111_0000 }, 1)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_0101_0111, 0b_1111_0000 }, 2)]
        public void ReadFullBytes(byte[] buffer, int index)
        {
            var reader = new BitReader(buffer);
            byte resultByte = 0;
            for (var i = 0; i <= index; i++)
            {
                resultByte = reader.ReadByte();
            }

            Assert.That(resultByte, Is.EqualTo(buffer[index]));
        }

        [TestCase(new byte[] { 0b_0101_0101, 0b_1111_0000 }, 0, 0, 0b_0101_0101)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_1111_0000 }, 1, 0, 0b_1010_1011)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_0111_0000 }, 1, 0, 0b_1010_1010)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_1111_0000 }, 7, 0, 0b_1111_1000)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_1111_0000 }, 8, 0, 0b_1111_0000)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_1111_0000, 0b_0010_0011 }, 9, 0, 0b_1110_0000)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_1111_0000, 0b_1010_0011 }, 9, 0, 0b_1110_0001)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_1111_0000, 0b_1010_0011 }, 1, 1, 0b_1110_0001)]
        [TestCase(new byte[] { 0b_0101_0101, 0b_1111_0000, 0b_1010_0011 }, 1, 1, 0b_1110_0001)]
        public void ReadPartiallyReadBytes(byte[] buffer, int bitOffset, int index, int expectedByte)
        {
            var reader = new BitReader(buffer);
            // Read some bits
            for (var i = 0; i < bitOffset; i++)
                reader.ReadBit();
            // Read some bytes
            for (var i = 0; i < index; i++)
                reader.ReadByte();
            // Read the actual byte
            var resultByte = reader.ReadByte();

            Assert.That(resultByte, Is.EqualTo(expectedByte));
        }
        
        [Test]
        public void ClosesStreamWhenReaderIsClosed()
        {
            var stream = new MemoryStream();
            var reader = new BitReader(stream);
            reader.Close();

            var isClosed = !(stream.CanRead || stream.CanSeek || stream.CanWrite);
            Assert.That(isClosed, Is.True);
        }

        [Test]
        public void ClosesStreamWhenReaderIsDisposed()
        {
            var stream = new MemoryStream();
            using (var reader = new BitReader(stream))
            {
                // Do nothing, just let the reader get disposed
            }

            var isClosed = !(stream.CanRead || stream.CanSeek || stream.CanWrite);
            Assert.That(isClosed, Is.True);
        }
    }
}