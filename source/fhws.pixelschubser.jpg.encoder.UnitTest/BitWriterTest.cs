using System;
using System.IO;
using NUnit.Framework;
using static fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers.BitBufferAccess;
using static fhws.pixelschubser.jpg.encoder.UnitTest.TestData.LargeRandomBits;

namespace fhws.pixelschubser.jpg.encoder.UnitTest
{
    [TestFixture]
    public class BitWriterTest
    {
        /// <summary>
        /// Tests if the provided buffer is used as ref and not copied internally.
        /// </summary>
        [Test]
        public void InternalStreamUsesProvidedBuffer()
        {
            var buffer = new byte[] { 13, 37 };
            var writer = new BitWriter(ref buffer);
            var baseStream = writer.BaseStream;

            // Reset the stream and get bytes
            baseStream.Seek(0, SeekOrigin.Begin);
            var byteA = baseStream.ReadByte();
            var byteB = baseStream.ReadByte();

            Assert.That(byteA, Is.EqualTo(13));
            Assert.That(byteB, Is.EqualTo(37));

            // Now change data in the buffer and test if stream is updated
            buffer[1] = 0;

            // Reset the stream and get bytes
            baseStream.Seek(0, SeekOrigin.Begin);
            byteA = baseStream.ReadByte();
            byteB = baseStream.ReadByte();

            Assert.That(byteA, Is.EqualTo(13));
            Assert.That(byteB, Is.EqualTo(00));
        }

        /// <summary>
        /// Tests if the provided stream is used as ref and not copied internally.
        /// </summary>
        [Test]
        public void InternalStreamIsProvidedStream()
        {
            using var stream = new MemoryStream();
            var writer = new BitWriter(stream);

            Assert.That(writer.BaseStream, Is.SameAs(stream));
        }

        public void WritesBuffered()
        {
            uint bufferSize = 2;
            var data = new byte[4];
            var writer = new BitWriter(ref data, bufferSize);

            // First write should be buffered and not update data
            writer.WriteByte(0xad);
            Assert.That(data, Is.EqualTo(new byte[] { 0x00, 0x00, 0x00, 0x00 }));

            // Second write should write the buffered bytes to data
            writer.WriteByte(0xf2);
            Assert.That(data, Is.EqualTo(new byte[] { 0xad, 0xf2, 0x00, 0x00 }));

            // Third write should again not update data
            writer.WriteByte(0xe5);
            Assert.That(data, Is.EqualTo(new byte[] { 0xad, 0xf2, 0x00, 0x00 }));

            // Fourth write should again add the buffered bytes to data
            writer.WriteByte(0x20);
            Assert.That(data, Is.EqualTo(new byte[] { 0xad, 0xf2, 0xe5, 0x20 }));
        }

        [TestCase((uint)1,
                  new byte[] { 1, 3, 8 },
                  new byte[] { 1, 0, 0 },
                  new byte[] { 1, 3, 0 })]
        [TestCase((uint)3,
                  new byte[] { 1, 3, 8, 7, 2, 4, 2, 9, 1 },
                  new byte[] { 1, 3, 8, 0, 0, 0, 0, 0, 0 },
                  new byte[] { 1, 3, 8, 7, 2, 4, 0, 0, 0 })]
        [TestCase((uint)5,
                  new byte[] { 1, 3, 8, 7, 2, 4, 2, 9, 1, 6, 1, 6, 9, 6, 8 },
                  new byte[] { 1, 3, 8, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                  new byte[] { 1, 3, 8, 7, 2, 4, 2, 9, 1, 6, 0, 0, 0, 0, 0 })]
        public void RespectsBufferSize(uint bufferSize, byte[] testData, byte[] first, byte[] second)
        {
            var data = new byte[testData.Length];
            var writer = new BitWriter(ref data, bufferSize);
            var nullBytes = new byte[data.Length];

            uint idx = 0;

            while (idx < bufferSize - 1) writer.WriteByte(testData[idx++]);
            // No write, everything cached
            Assert.That(data, Is.EqualTo(nullBytes));
            writer.WriteByte(testData[idx++]);
            // Cache full and should be written to data
            Assert.That(data, Is.EqualTo(first));

            while (idx < (bufferSize * 2) - 1) writer.WriteByte(testData[idx++]);
            // No new write, everything cached
            Assert.That(data, Is.EqualTo(first));
            writer.WriteByte(testData[idx++]);
            // Cache full and should be written to data
            Assert.That(data, Is.EqualTo(second));

            while (idx < (bufferSize * 3) - 1) writer.WriteByte(testData[idx++]);
            // No new write, everything cached
            Assert.That(data, Is.EqualTo(second));
            writer.WriteByte(testData[idx++]);
            // Cache full and should be written to data
            Assert.That(data, Is.EqualTo(testData));
        }

        [TestCase(0x01, 0x20, 0x0b)]
        [TestCase(0xff, 0x10, 0x05)]
        [TestCase(0xa3, 0x9d, 0x30)]
        public void FlushBufferWritesButesToStream(byte byteA, byte byteB, byte byteC)
        {
            var data = new byte[4];
            var writer = new BitWriter(ref data);

            writer.WriteByte(byteA);
            writer.WriteByte(byteB);
            writer.WriteByte(byteC);

            Assert.That(data, Is.EqualTo(new byte[] { 0x0, 0x0, 0x0, 0x0 }));

            writer.Flush();

            Assert.That(data, Is.EqualTo(new byte[] { byteA, byteB, byteC, 0x0 }));
        }

        /// <summary>
        /// Write 8 bits (using buffer)
        /// </summary>
        [Test]
        public void WriteFullByteAsBitsToBuffer()
        {
            var buffer = new byte[] { 0x0 };
            var writer = new BitWriter(ref buffer);
            var testData = new byte[] {
                0, 1, 1, 0,   1, 1, 1, 0,
            };
            foreach (byte b in testData)
            {
                writer.WriteBit(b);
            }
            writer.Flush();

            Assert.That(buffer[0], Is.EqualTo(0b_0110_1110));

            var baseStream = writer.BaseStream;
            // Reset the stream and get first byte
            baseStream.Seek(0, SeekOrigin.Begin);
            var streamData = baseStream.ReadByte();
        }

        /// <summary>
        /// Write 8 bits (using stream)
        /// </summary>
        [Test]
        public void WriteFullByteAsBitsToMemStream()
        {
            using var stream = new MemoryStream();
            var writer = new BitWriter(stream);
            var testData = new byte[] {
                1, 0, 1, 0,   0, 0, 1, 0,
            };
            foreach (byte b in testData)
            {
                writer.WriteBit(b);
            }
            writer.Flush();

            // Reset the stream and get first byte
            stream.Seek(0, SeekOrigin.Begin);
            var data = stream.ReadByte();

            Assert.That(data, Is.EqualTo(0b_1010_0010));
        }

        /// <summary>
        /// Writing 4 bits and test that these are not flushed
        /// </summary>
        [Test]
        public void WritingHalfByteShouldNotWriteInStream()
        {
            var buffer = new byte[] { 0x0 };
            var writer = new BitWriter(ref buffer);
            var testData = new byte[] {
                0, 1, 1, 0
            };
            foreach (byte b in testData)
            {
                writer.WriteBit(b);
            }

            Assert.That(buffer[0], Is.EqualTo(0x0));
        }

        [Test]
        public void WriteMultipleBytesAsBits()
        {
            var buffer = new byte[4];
            var writer = new BitWriter(ref buffer);
            var testData = new byte[] {
                0, 1, 1, 0,   1, 1, 1, 0,
                1, 0, 1, 0,   0, 0, 1, 0,
                0, 0, 0, 0,   1, 1, 1, 1,
                0, 1, 0, 0,   0, 0, 0, 0,
            };
            foreach (byte b in testData)
            {
                writer.WriteBit(b);
            }
            writer.Flush();

            Assert.That(buffer[0], Is.EqualTo(0b_0110_1110));
            Assert.That(buffer[1], Is.EqualTo(0b_1010_0010));
            Assert.That(buffer[2], Is.EqualTo(0b_0000_1111));
            Assert.That(buffer[3], Is.EqualTo(0b_0100_0000));
        }

        [TestCase(new byte[] { 0, 1 }, new byte[] { 0b_0100_0000 })]
        [TestCase(new byte[] { 0, 1, 1, 0 }, new byte[] { 0b_0110_0000 })]
        [TestCase(new byte[] { 0, 1, 1, 0, 1, 1 }, new byte[] { 0b_0110_1100 })]
        [TestCase(new byte[] { 0, 1, 1, 0, 1, 1, 0, 0,
                               0, 1 }, new byte[] { 0b_0110_1100, 0b_0100_0000 })]
        public void FillRemainingBitsWithZeros(byte[] testData, byte[] expected)
        {
            var buffer = new byte[expected.Length];
            var writer = new BitWriter(ref buffer);
            foreach (byte b in testData)
            {
                writer.WriteBit(b);
            }
            writer.FillRemainingBits();
            writer.Flush();

            Assert.That(buffer, Is.EqualTo(expected));
        }

        [TestCase(new byte[] { 1, 0 }, new byte[] { 0b_1011_1111 })]
        [TestCase(new byte[] { 1, 0, 0, 1 }, new byte[] { 0b_1001_1111 })]
        [TestCase(new byte[] { 1, 0, 0, 1, 0, 0 }, new byte[] { 0b_1001_0011 })]
        [TestCase(new byte[] { 1, 0, 0, 1, 0, 0, 1, 1,
                               1, 0 }, new byte[] { 0b_1001_0011, 0b_1011_1111 })]
        public void FillRemainingBitsWithOnes(byte[] testData, byte[] expected)
        {
            var buffer = new byte[expected.Length];
            var writer = new BitWriter(ref buffer);
            foreach (byte b in testData)
            {
                writer.WriteBit(b);
            }
            writer.FillRemainingBitsWithOnes();
            writer.Flush();

            Assert.That(buffer, Is.EqualTo(expected));
        }

        [Test]
        public void WriteWholeByteAtOnce()
        {
            var buffer = new byte[2];
            var writer = new BitWriter(ref buffer);
            writer.WriteByte(13);
            writer.WriteByte(37);
            writer.Flush();

            Assert.That(buffer[0], Is.EqualTo(13));
            Assert.That(buffer[1], Is.EqualTo(37));
        }

        [Test]
        public void WriteWholeByteAtOnceOnHalfByte()
        {
            var buffer = new byte[2];
            var writer = new BitWriter(ref buffer);
            var testData = new byte[] {
                0, 1, 1, 0,
            };
            foreach (byte b in testData)
            {
                writer.WriteBit(b);
            }
            writer.WriteByte(0b_0100_1001);
            writer.FillRemainingBits();
            writer.Flush();

            Assert.That(buffer[0], Is.EqualTo(0b_0110_0100));
            Assert.That(buffer[1], Is.EqualTo(0b_1001_0000));
        }

        [Test]
        public void WriteWholeByteAtOnceOnPartlyFilledByte()
        {
            var buffer = new byte[2];
            var writer = new BitWriter(ref buffer);
            var testData = new byte[] {
                0, 1, 1,
            };
            foreach (byte b in testData)
            {
                writer.WriteBit(b);
            }
            writer.WriteByte(0b_0100_1001);
            writer.FillRemainingBits();
            writer.Flush();

            Assert.That(buffer[0], Is.EqualTo(0b_0110_1001));
            Assert.That(buffer[1], Is.EqualTo(0b_0010_0000));
        }

        [TestCase(new byte[] { 0xff, 0xf1, 0x00, 0x15, 0x01 })]
        [TestCase(new byte[] { 0x1f, 0xab, 0x2d, 0xe0, 0x87 })]
        public void WriteByteArrayWithExactBufferLength(byte[] testData)
        {
            var data = new byte[testData.Length];
            var writer = new BitWriter(ref data, (uint)testData.Length);
            writer.WriteBytes(testData, 0, testData.Length);

            // This should just skip the buffer and write to the underlying
            // stream directly, therefore we do not call Flush() here.

            Assert.That(data, Is.EqualTo(testData));
        }

        [TestCase(new byte[] { 0xff, 0xf1, 0x00, 0x15, 0x01 })]
        [TestCase(new byte[] { 0x1f, 0xab, 0x2d, 0xe0, 0x87 })]
        public void WriteByteArraySmallerThanBuffer(byte[] testData)
        {
            var data = new byte[testData.Length + 1];
            var writer = new BitWriter(ref data, (uint)testData.Length + 1);
            writer.WriteBytes(testData, 0, testData.Length);

            // This should write to the internal buffer and should not update
            // the data array
            var nullBytes = new byte[data.Length];
            Assert.That(data, Is.EqualTo(nullBytes));

            writer.Flush();

            // Copy the test data to an array that is larger by one so it
            // machtes the `data` length
            var expectedData = new byte[testData.Length + 1];
            Buffer.BlockCopy(testData, 0, expectedData, 0, testData.Length);

            Assert.That(data, Is.EqualTo(expectedData));
        }

        [TestCase(new byte[] { 0xff, 0xf1, 0x00, 0x15, 0x01 })]
        [TestCase(new byte[] { 0x1f, 0xab, 0x2d, 0xe0, 0x87 })]
        public void WriteByteArrayLargerThanBuffer(byte[] testData)
        {
            var data = new byte[testData.Length];
            var writer = new BitWriter(ref data, (uint)testData.Length - 1);
            writer.WriteBytes(testData, 0, testData.Length);

            // This should write all bytes directly to the underlying stream
            // and should not write part of it to the buffer
            Assert.That(data, Is.EqualTo(testData));
        }

        // Buffer large enough to fit all testData in => no data in `data`
        [TestCase(5, new byte[] { 0xff },
                     new byte[] { 0xad, 0xf1, 0x01 },
                     new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00 },
                     new byte[] { 0xff, 0xad, 0xf1, 0x01, 0x00 })]
        // Buffer would be exactly filed => expectataion is that data is written
        [TestCase(4, new byte[] { 0xff },
                     new byte[] { 0x1f, 0xab, 0x2d },
                     new byte[] { 0xff, 0x1f, 0xab, 0x2d },
                     new byte[] { 0xff, 0x1f, 0xab, 0x2d })]
        // Buffer too small => should write directly and not buffer anything
        [TestCase(3, new byte[] { 0xff },
                     new byte[] { 0xd2, 0xc9, 0x33 },
                     new byte[] { 0xff, 0xd2, 0xc9, 0x33 },
                     new byte[] { 0xff, 0xd2, 0xc9, 0x33 })]
        // Buffer too small => should write directly and not buffer anything
        [TestCase(2, new byte[] { 0xff },
                     new byte[] { 0x1f, 0xab, 0x2d },
                     new byte[] { 0xff, 0x1f, 0xab, 0x2d },
                     new byte[] { 0xff, 0x1f, 0xab, 0x2d })]
        public void WriteByteArrayWithPartlyFilledBuffer(
            int bufferSize, byte[] prefillData, byte[] testData,
            byte[] expectedData, byte[] expectedDataAfterFlush
        )
        {
            var data = new byte[expectedData.Length];
            var writer = new BitWriter(ref data, (uint)bufferSize);
            // Prefill buffer with one byte
            foreach (var _byte in prefillData) writer.WriteByte(_byte);

            // Write the test data
            writer.WriteBytes(testData, 0, testData.Length);
            Assert.That(data, Is.EqualTo(expectedData));

            writer.Flush();
            Assert.That(data, Is.EqualTo(expectedDataAfterFlush));
        }


        [TestCase(1, new byte[] { 0b_1010_1011 },
                     new byte[] { 0b_1101_0101, 0b_1000_0000 })]
        [TestCase(7, new byte[] { 0b_1010_1011 },
                     new byte[] { 0b_1111_1111, 0b_0101_0110 })]
        [TestCase(8, new byte[] { 0b_1010_1011 },
                     new byte[] { 0b_1111_1111, 0b_1010_1011 })]
        [TestCase(9, new byte[] { 0b_1010_1011 },
                     new byte[] { 0b_1111_1111, 0b_1101_0101, 0b_1000_0000 })]
        [TestCase(1, new byte[] { 0b_1010_1011, 0b_1101_0010 },
                     new byte[] { 0b_1101_0101, 0b_1110_1001, 0b_0000_0000 })]
        [TestCase(7, new byte[] { 0b_1010_1011, 0b_1101_0010 },
                     new byte[] { 0b_1111_1111, 0b_0101_0111, 0b_1010_0100 })]
        [TestCase(8, new byte[] { 0b_1010_1011, 0b_1101_0010 },
                     new byte[] { 0b_1111_1111, 0b_1010_1011, 0b_1101_0010 })]
        [TestCase(9, new byte[] { 0b_1010_1011, 0b_1101_0010 },
                     new byte[] { 0b_1111_1111, 0b_1101_0101, 0b_1110_1001, 0b_0000_0000 })]
        public void WriteByteArrayWithPartiallyWrittenByte(
            int writtenBits, byte[] testData, byte[] expectedData
        )
        {
            var data = new byte[expectedData.Length];
            var writer = new BitWriter(ref data);
            // Fill bits
            for (var i = 0; i < writtenBits; i++) writer.WriteBit(1);
            writer.WriteBytes(testData, 0, testData.Length);
            writer.FillRemainingBits();
            writer.Flush();

            Assert.That(data, Is.EqualTo(expectedData));
        }

        [TestCase(1, 1, new byte[] { 0xf1, 0x02, 0x45 },
                        new byte[] { 0x02, 0x00, 0x00 })]
        [TestCase(2, 1, new byte[] { 0xc7, 0x46, 0x89, 0x8c },
                        new byte[] { 0x89, 0x00, 0x00, 0x00 })]
        [TestCase(1, 2, new byte[] { 0xf1, 0x02, 0x45, 0xcb },
                        new byte[] { 0x02, 0x45, 0x00 })]
        [TestCase(2, 2, new byte[] { 0xc7, 0x46, 0x89, 0x8c },
                        new byte[] { 0x89, 0x8c, 0x00, 0x00 })]
        public void WriteBytesWithOffsetAndLength(
            int offset, int length, byte[] testData, byte[] expectedData
        )
        {
            var data = new byte[expectedData.Length];
            var writer = new BitWriter(ref data);
            writer.WriteBytes(testData, offset, length);
            writer.Flush();

            Assert.That(data, Is.EqualTo(expectedData));
        }

        [TestCase(0, 0, new byte[] { 1 },
                        new byte[] { })]
        [TestCase(0, 1, new byte[] { 1 },
                        new byte[] { 0b_1000_0000 })]
        [TestCase(0, 5, new byte[] { 1, 1, 0, 1, 1 },
                        new byte[] { 0b_1101_1000 })]
        [TestCase(0, 8, new byte[] { 1, 1, 0, 1, 1, 0, 1, 1 },
                        new byte[] { 0b_1101_1011 })]
        [TestCase(0, 9, new byte[] { 1, 1, 0, 1, 1, 0, 0, 1, 1 },
                        new byte[] { 0b_1101_1001, 0b_1000_0000 })]
        [TestCase(0, 12, new byte[] { 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1 },
                        new byte[] { 0b_1101_1001, 0b_1001_0000 })]
        [TestCase(1, 1, new byte[] { 0, 1, 0 },
                        new byte[] { 0b_1000_0000 })]
        [TestCase(2, 1, new byte[] { 1, 0, 1, 0 },
                        new byte[] { 0b_1000_0000 })]
        [TestCase(2, 4, new byte[] { 0, 1, 0, 1, 0, 1, 0 },
                        new byte[] { 0b_0101_0000 })]
        [TestCase(2, 9, new byte[] { 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1 },
                        new byte[] { 0b_0110_0110, 0b_1000_0000 })]
        public void WriteBitArray(int offset, int length, byte[] testData, byte[] expectedData)
        {
            var data = new byte[expectedData.Length];
            var writer = new BitWriter(ref data);
            writer.WriteBits(testData, offset, length);
            writer.FillRemainingBits();
            writer.Flush();

            Assert.That(data, Is.EqualTo(expectedData));
        }

        // No Bits written before byte
        [TestCase(0, 0, 8, 0b_1010_1101, new byte[] { 0b_1010_1101 })]
        [TestCase(0, 0, 7, 0b_1010_1101, new byte[] { 0b_1010_1100 })]
        [TestCase(0, 0, 1, 0b_1110_1101, new byte[] { 0b_1000_0000 })]
        [TestCase(0, 1, 7, 0b_1010_1101, new byte[] { 0b_0101_1010 })]
        [TestCase(0, 1, 4, 0b_1010_1101, new byte[] { 0b_0101_0000 })]
        [TestCase(0, 1, 1, 0b_0110_1101, new byte[] { 0b_1000_0000 })]
        [TestCase(0, 5, 2, 0b_1010_0110, new byte[] { 0b_1100_0000 })]
        [TestCase(0, 7, 1, 0b_1010_0101, new byte[] { 0b_1000_0000 })]
        [TestCase(0, 0, 0, 0b_1010_0101, new byte[] { })]
        // One bit written before byte
        [TestCase(1, 0, 8, 0b_1010_1101, new byte[] { 0b_1101_0110, 0b_1000_0000 })]
        [TestCase(1, 0, 7, 0b_1010_1101, new byte[] { 0b_1101_0110, 0b_0000_0000 })]
        [TestCase(1, 0, 1, 0b_1110_1101, new byte[] { 0b_1100_0000, 0b_0000_0000 })]
        [TestCase(1, 1, 7, 0b_1010_1101, new byte[] { 0b_1010_1101, 0b_0000_0000 })]
        [TestCase(1, 1, 4, 0b_1010_1101, new byte[] { 0b_1010_1000, 0b_0000_0000 })]
        [TestCase(1, 1, 1, 0b_0110_1101, new byte[] { 0b_1100_0000, 0b_0000_0000 })]
        [TestCase(1, 5, 2, 0b_1010_0110, new byte[] { 0b_1110_0000, 0b_0000_0000 })]
        [TestCase(1, 7, 1, 0b_1010_0101, new byte[] { 0b_1100_0000, 0b_0000_0000 })]
        [TestCase(1, 0, 0, 0b_1010_0101, new byte[] { 0b_1000_0000 })]
        // Seven bits written before byte
        [TestCase(7, 0, 8, 0b_1010_1101, new byte[] { 0b_1111_1111, 0b_0101_1010 })]
        [TestCase(7, 0, 7, 0b_1010_1101, new byte[] { 0b_1111_1111, 0b_0101_1000 })]
        [TestCase(7, 0, 1, 0b_1110_1101, new byte[] { 0b_1111_1111, 0b_0000_0000 })]
        [TestCase(7, 1, 7, 0b_1010_1101, new byte[] { 0b_1111_1110, 0b_1011_0100 })]
        [TestCase(7, 1, 4, 0b_1010_1101, new byte[] { 0b_1111_1110, 0b_1010_0000 })]
        [TestCase(7, 1, 1, 0b_0110_1101, new byte[] { 0b_1111_1111, 0b_0000_0000 })]
        [TestCase(7, 5, 2, 0b_1010_0110, new byte[] { 0b_1111_1111, 0b_1000_0000 })]
        [TestCase(7, 7, 1, 0b_1010_0101, new byte[] { 0b_1111_1111, 0b_0000_0000 })]
        [TestCase(7, 0, 0, 0b_1010_0101, new byte[] { 0b_1111_1110 })]
        public void WriteBitsFromByte(
            int bitsToWrite, int offset, int count, byte _byte, byte[] expectedData
        )
        {
            var data = new byte[expectedData.Length];
            var writer = new BitWriter(ref data);
            for (var i = 0; i < bitsToWrite; i++) writer.WriteBit(1);
            writer.Write(_byte, offset, count);
            writer.FillRemainingBits();
            writer.Flush();

            Assert.That(data, Is.EqualTo(expectedData));
        }

        // No Bits written before byte (< 8 bit)
        [TestCase(0, 0, 8, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_1010_1101 })]
        [TestCase(0, 0, 7, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_1010_1100 })]
        [TestCase(0, 0, 1, new byte[] { 0b_1110_1101 },
                           new byte[] { 0b_1000_0000 })]
        [TestCase(0, 1, 7, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_0101_1010 })]
        [TestCase(0, 1, 4, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_0101_0000 })]
        [TestCase(0, 1, 1, new byte[] { 0b_0110_1101 },
                           new byte[] { 0b_1000_0000 })]
        [TestCase(0, 5, 2, new byte[] { 0b_1010_0110 },
                           new byte[] { 0b_1100_0000 })]
        [TestCase(0, 7, 1, new byte[] { 0b_1010_0101 },
                           new byte[] { 0b_1000_0000 })]
        [TestCase(0, 0, 0, new byte[] { 0b_1010_0101 },
                           new byte[] { })]
        // No Bits written before byte (> 8 bit)
        [TestCase(0, 0, 16, new byte[] { 0b_1010_1101, 0b_1001_1111 },
                            new byte[] { 0b_1010_1101, 0b_1001_1111 })]
        [TestCase(0, 0, 15, new byte[] { 0b_1010_1101, 0b_1001_1111 },
                            new byte[] { 0b_1010_1101, 0b_1001_1110 })]
        [TestCase(0, 0, 09, new byte[] { 0b_1110_1101, 0b_1001_1111 },
                            new byte[] { 0b_1110_1101, 0b_1000_0000 })]
        [TestCase(0, 1, 15, new byte[] { 0b_1010_1101, 0b_1001_1111 },
                            new byte[] { 0b_0101_1011, 0b_0011_1110 })]
        [TestCase(0, 1, 12, new byte[] { 0b_1010_1101, 0b_1001_1111 },
                            new byte[] { 0b_0101_1011, 0b_0011_0000 })]
        [TestCase(0, 1, 09, new byte[] { 0b_0110_1101, 0b_1001_1111 },
                            new byte[] { 0b_1101_1011, 0b_0000_0000 })]
        [TestCase(0, 5, 10, new byte[] { 0b_1010_0110, 0b_1001_1111 },
                            new byte[] { 0b_1101_0011, 0b_1100_0000 })]
        [TestCase(0, 7, 09, new byte[] { 0b_1010_0101, 0b_1001_1111 },
                            new byte[] { 0b_1100_1111, 0b_1000_0000 })]
        [TestCase(0, 0, 08, new byte[] { 0b_1010_0101, 0b_1001_1111 },
                            new byte[] { 0b_1010_0101, 0b_0000_000 })]
        // One bit written before byte (< 8 bit)
        [TestCase(1, 0, 8, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_1101_0110, 0b_1000_0000 })]
        [TestCase(1, 0, 7, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_1101_0110, 0b_0000_0000 })]
        [TestCase(1, 0, 1, new byte[] { 0b_1110_1101 },
                           new byte[] { 0b_1100_0000, 0b_0000_0000 })]
        [TestCase(1, 1, 7, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_1010_1101, 0b_0000_0000 })]
        [TestCase(1, 1, 4, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_1010_1000, 0b_0000_0000 })]
        [TestCase(1, 1, 1, new byte[] { 0b_0110_1101 },
                           new byte[] { 0b_1100_0000, 0b_0000_0000 })]
        [TestCase(1, 5, 2, new byte[] { 0b_1010_0110 },
                           new byte[] { 0b_1110_0000, 0b_0000_0000 })]
        [TestCase(1, 7, 1, new byte[] { 0b_1010_0101 },
                           new byte[] { 0b_1100_0000, 0b_0000_0000 })]
        [TestCase(1, 0, 0, new byte[] { 0b_1010_0101 },
                           new byte[] { 0b_1000_0000 })]
        // One bit written before byte (> 8 bit)
        [TestCase(1, 0, 16, new byte[] { 0b_1010_1101, 0b_0001_1111 },
                            new byte[] { 0b_1101_0110, 0b_1000_1111, 0b_1000_0000 })]
        [TestCase(1, 0, 15, new byte[] { 0b_1010_1101, 0b_0001_1111 },
                            new byte[] { 0b_1101_0110, 0b_1000_1111, 0b_0000_0000 })]
        [TestCase(1, 0, 09, new byte[] { 0b_1110_1101, 0b_0001_1111 },
                            new byte[] { 0b_1111_0110, 0b_1000_0000, 0b_0000_0000 })]
        [TestCase(1, 1, 15, new byte[] { 0b_1010_1101, 0b_0001_1111 },
                            new byte[] { 0b_1010_1101, 0b_0001_1111, 0b_0000_0000 })]
        [TestCase(1, 1, 12, new byte[] { 0b_1010_1101, 0b_0001_1111 },
                            new byte[] { 0b_1010_1101, 0b_0001_1000, 0b_0000_0000 })]
        [TestCase(1, 1, 09, new byte[] { 0b_0110_1101, 0b_0001_1111 },
                            new byte[] { 0b_1110_1101, 0b_0000_0000, 0b_0000_0000 })]
        [TestCase(1, 5, 10, new byte[] { 0b_1010_0110, 0b_0001_1111 },
                            new byte[] { 0b_1110_0001, 0b_1110_0000, 0b_0000_0000 })]
        [TestCase(1, 7, 09, new byte[] { 0b_1010_0101, 0b_0001_1111 },
                            new byte[] { 0b_1100_0111, 0b_1100_0000, 0b_0000_0000 })]
        [TestCase(1, 0, 08, new byte[] { 0b_1010_0101, 0b_0001_1111 },
                            new byte[] { 0b_1101_0010, 0b_1000_0000, 0b_0000_0000 })]
        // Seven bits written before byte (< 8 bit)
        [TestCase(7, 0, 8, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_1111_1111, 0b_0101_1010 })]
        [TestCase(7, 0, 7, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_1111_1111, 0b_0101_1000 })]
        [TestCase(7, 0, 1, new byte[] { 0b_1110_1101 },
                           new byte[] { 0b_1111_1111, 0b_0000_0000 })]
        [TestCase(7, 1, 7, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_1111_1110, 0b_1011_0100 })]
        [TestCase(7, 1, 4, new byte[] { 0b_1010_1101 },
                           new byte[] { 0b_1111_1110, 0b_1010_0000 })]
        [TestCase(7, 1, 1, new byte[] { 0b_0110_1101 },
                           new byte[] { 0b_1111_1111, 0b_0000_0000 })]
        [TestCase(7, 5, 2, new byte[] { 0b_1010_0110 },
                           new byte[] { 0b_1111_1111, 0b_1000_0000 })]
        [TestCase(7, 7, 1, new byte[] { 0b_1010_0101 },
                           new byte[] { 0b_1111_1111, 0b_0000_0000 })]
        [TestCase(7, 0, 0, new byte[] { 0b_1010_0101 },
                           new byte[] { 0b_1111_1110 })]
        // Seven bits written before byte (> 8 bit)
        [TestCase(7, 0, 16, new byte[] { 0b_1010_1101, 0b_0001_1111 },
                            new byte[] { 0b_1111_1111, 0b_0101_1010, 0b_0011_1110 })]
        [TestCase(7, 0, 15, new byte[] { 0b_1010_1101, 0b_0001_1111 },
                            new byte[] { 0b_1111_1111, 0b_0101_1010, 0b_0011_1100 })]
        [TestCase(7, 0, 09, new byte[] { 0b_1110_1101, 0b_0001_1111 },
                            new byte[] { 0b_1111_1111, 0b_1101_1010, 0b_0000_0000 })]
        [TestCase(7, 1, 15, new byte[] { 0b_1010_1101, 0b_0001_1111 },
                            new byte[] { 0b_1111_1110, 0b_1011_0100, 0b_0111_1100 })]
        [TestCase(7, 1, 12, new byte[] { 0b_1010_1101, 0b_0001_1111 },
                            new byte[] { 0b_1111_1110, 0b_1011_0100, 0b_0110_0000 })]
        [TestCase(7, 1, 09, new byte[] { 0b_0110_1101, 0b_0001_1111 },
                            new byte[] { 0b_1111_1111, 0b_1011_0100, 0b_0000_0000 })]
        [TestCase(7, 5, 10, new byte[] { 0b_1010_0110, 0b_0001_1111 },
                            new byte[] { 0b_1111_1111, 0b_1000_0111, 0b_1000_0000 })]
        [TestCase(7, 7, 09, new byte[] { 0b_1010_0101, 0b_0001_1111 },
                            new byte[] { 0b_1111_1111, 0b_0001_1111, 0b_0000_0000 })]
        [TestCase(7, 0, 08, new byte[] { 0b_1010_0101, 0b_0001_1111 },
                            new byte[] { 0b_1111_1111, 0b_0100_1010, 0b_0000_0000 })]
        // And a bit longer
        [TestCase(0, 0, 32, new byte[] { 0b_1010_1101, 0b_0001_1111, 0b_0101_0100, 0b_0100_1010 },
                            new byte[] { 0b_1010_1101, 0b_0001_1111, 0b_0101_0100, 0b_0100_1010 })]
        [TestCase(2, 0, 30, new byte[] { 0b_1010_1101, 0b_0001_1111, 0b_0101_0100, 0b_0100_1010 },
                            new byte[] { 0b_1110_1011, 0b_0100_0111, 0b_1101_0101, 0b_0001_0010 })]
        [TestCase(0, 2, 30, new byte[] { 0b_1010_1101, 0b_0001_1111, 0b_0101_0100, 0b_0100_1010 },
                            new byte[] { 0b_1011_0100, 0b_0111_1101, 0b_0101_0001, 0b_0010_1000 })]
        [TestCase(0, 0, 30, new byte[] { 0b_1010_1101, 0b_0001_1111, 0b_0101_0100, 0b_0100_1010 },
                            new byte[] { 0b_1010_1101, 0b_0001_1111, 0b_0101_0100, 0b_0100_1000 })]
        [TestCase(2, 2, 26, new byte[] { 0b_1010_1101, 0b_0001_1111, 0b_0101_0100, 0b_0100_1010 },
                            new byte[] { 0b_1110_1101, 0b_0001_1111, 0b_0101_0100, 0b_0100_0000 })]
        public void WriteBitsFromByteArray(
            int bitsToWrite, int offset, int count, byte[] bytes, byte[] expectedData
        )
        {
            var data = new byte[expectedData.Length];
            var writer = new BitWriter(ref data);
            for (var i = 0; i < bitsToWrite; i++) writer.WriteBit(1);
            writer.Write(bytes, offset, count);
            writer.FillRemainingBits();
            writer.Flush();

            Assert.That(data, Is.EqualTo(expectedData));
        }

        [Test]
        public void ClosesStreamWhenWriterIsClosed()
        {
            var stream = new MemoryStream();
            var writer = new BitWriter(stream);
            writer.Close();

            var isClosed = !(stream.CanRead || stream.CanSeek || stream.CanWrite);
            Assert.That(isClosed, Is.True);
        }

        [Test]
        public void ClosesStreamWhenWriterIsDisposed()
        {
            var stream = new MemoryStream();
            using (var writer = new BitWriter(stream))
            {
                // Do nothing, just let the writer get disposed
            }

            var isClosed = !(stream.CanRead || stream.CanSeek || stream.CanWrite);
            Assert.That(isClosed, Is.True);
        }

        [Test]
        public void LargeRandomBitsTest()
        {
            var buffer = new byte[LargeRandBitsBuffer.Length];
            var writer = new BitWriter(ref buffer);
            foreach (byte b in LargeRandBits)
            {
                writer.WriteBit(b);
            }
            writer.Flush();

            Assert.That(buffer, Is.EqualTo(LargeRandBitsBuffer));
        }
    }
}
