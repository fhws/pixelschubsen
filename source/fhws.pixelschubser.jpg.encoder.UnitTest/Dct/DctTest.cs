using System;
using System.Collections.Generic;
using fhws.pixelschubser.jpg.encoder.Dct;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest.Dct
{
    [TestFixture(typeof(DirectDct))]
    [TestFixture(typeof(SeparatedDct))]
    [TestFixture(typeof(DctArai))]
    public class DctTest<T_Dct> where T_Dct : IDct, new()
    {
        private IDct dct;

        [SetUp]
        public void CreateList()
        {
            this.dct = new T_Dct();
        }

        [TestCaseSource(nameof(_SingleChunks))]
        public void SingleChunks(double[,] data, double[,] expected)
        {
            var transformed = dct.Transform(data);
            Assert.That(transformed, Is.EqualTo(expected).Within(0.5));
        }

        [TestCaseSource(nameof(_MultipleChunks))]
        public void MultipleChunks(double[,] data, double[,] expected)
        {
            var transformed = dct.Transform(data);
            Assert.That(transformed, Is.EqualTo(expected).Within(0.5));
        }

        [TestCaseSource(nameof(_RectangleInput))]
        public void RectangleInput(double[,] data, double[,] expected)
        {

            var transformed = dct.Transform(data);
            Assert.That(transformed, Is.EqualTo(expected).Within(0.5));
        }

        static IEnumerable<object[]> _SingleChunks()
        {
            double[,] data;
            double[,] expected;
            var PI = Math.PI;
            Func<double, double> cos = (x) => Math.Cos(x);

            // --------------------------------
            data = new double[,] {
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
            };
            expected = new double[,] {
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
            };
            yield return new object[] { data, expected };

            // --------------------------------
            data = new double[,] {
                { 1, 1, 1, 1, 1, 1, 1, 1, },
                { 1, 1, 1, 1, 1, 1, 1, 1, },
                { 1, 1, 1, 1, 1, 1, 1, 1, },
                { 1, 1, 1, 1, 1, 1, 1, 1, },
                { 1, 1, 1, 1, 1, 1, 1, 1, },
                { 1, 1, 1, 1, 1, 1, 1, 1, },
                { 1, 1, 1, 1, 1, 1, 1, 1, },
                { 1, 1, 1, 1, 1, 1, 1, 1, },
            };
            expected = new double[,] {
                { 8, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
            };
            yield return new object[] { data, expected };

            // --------------------------------
            data = new double[,] {
                { 255, 255, 255, 255, 255, 255, 255, 255, },
                { 255, 255, 255, 255, 255, 255, 255, 255, },
                { 255, 255, 255, 255, 255, 255, 255, 255, },
                { 255, 255, 255, 255, 255, 255, 255, 255, },
                { 255, 255, 255, 255, 255, 255, 255, 255, },
                { 255, 255, 255, 255, 255, 255, 255, 255, },
                { 255, 255, 255, 255, 255, 255, 255, 255, },
                { 255, 255, 255, 255, 255, 255, 255, 255, },
            };
            expected = new double[,] {
                { 2040, 0, 0, 0, 0, 0, 0, 0, },
                {    0, 0, 0, 0, 0, 0, 0, 0, },
                {    0, 0, 0, 0, 0, 0, 0, 0, },
                {    0, 0, 0, 0, 0, 0, 0, 0, },
                {    0, 0, 0, 0, 0, 0, 0, 0, },
                {    0, 0, 0, 0, 0, 0, 0, 0, },
                {    0, 0, 0, 0, 0, 0, 0, 0, },
                {    0, 0, 0, 0, 0, 0, 0, 0, },
            };
            yield return new object[] { data, expected };

            // --------------------------------
            data = new double[,] {
                { cos(0.5/8*PI), cos(1.5/8*PI), cos(2.5/8*PI), cos(3.5/8*PI), cos(4.5/8*PI), cos(5.5/8*PI), cos(6.5/8*PI), cos(7.5/8*PI), },
                { cos(0.5/8*PI), cos(1.5/8*PI), cos(2.5/8*PI), cos(3.5/8*PI), cos(4.5/8*PI), cos(5.5/8*PI), cos(6.5/8*PI), cos(7.5/8*PI), },
                { cos(0.5/8*PI), cos(1.5/8*PI), cos(2.5/8*PI), cos(3.5/8*PI), cos(4.5/8*PI), cos(5.5/8*PI), cos(6.5/8*PI), cos(7.5/8*PI), },
                { cos(0.5/8*PI), cos(1.5/8*PI), cos(2.5/8*PI), cos(3.5/8*PI), cos(4.5/8*PI), cos(5.5/8*PI), cos(6.5/8*PI), cos(7.5/8*PI), },
                { cos(0.5/8*PI), cos(1.5/8*PI), cos(2.5/8*PI), cos(3.5/8*PI), cos(4.5/8*PI), cos(5.5/8*PI), cos(6.5/8*PI), cos(7.5/8*PI), },
                { cos(0.5/8*PI), cos(1.5/8*PI), cos(2.5/8*PI), cos(3.5/8*PI), cos(4.5/8*PI), cos(5.5/8*PI), cos(6.5/8*PI), cos(7.5/8*PI), },
                { cos(0.5/8*PI), cos(1.5/8*PI), cos(2.5/8*PI), cos(3.5/8*PI), cos(4.5/8*PI), cos(5.5/8*PI), cos(6.5/8*PI), cos(7.5/8*PI), },
                { cos(0.5/8*PI), cos(1.5/8*PI), cos(2.5/8*PI), cos(3.5/8*PI), cos(4.5/8*PI), cos(5.5/8*PI), cos(6.5/8*PI), cos(7.5/8*PI), },
            };
            expected = new double[,] {
                { 0, 6, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
            };
            yield return new object[] { data, expected };

            // --------------------------------
            data = new double[,] {
                { cos(0.5/8*PI), cos(0.5/8*PI), cos(0.5/8*PI), cos(0.5/8*PI), cos(0.5/8*PI), cos(0.5/8*PI), cos(0.5/8*PI), cos(0.5/8*PI), },
                { cos(1.5/8*PI), cos(1.5/8*PI), cos(1.5/8*PI), cos(1.5/8*PI), cos(1.5/8*PI), cos(1.5/8*PI), cos(1.5/8*PI), cos(1.5/8*PI), },
                { cos(2.5/8*PI), cos(2.5/8*PI), cos(2.5/8*PI), cos(2.5/8*PI), cos(2.5/8*PI), cos(2.5/8*PI), cos(2.5/8*PI), cos(2.5/8*PI), },
                { cos(3.5/8*PI), cos(3.5/8*PI), cos(3.5/8*PI), cos(3.5/8*PI), cos(3.5/8*PI), cos(3.5/8*PI), cos(3.5/8*PI), cos(3.5/8*PI), },
                { cos(4.5/8*PI), cos(4.5/8*PI), cos(4.5/8*PI), cos(4.5/8*PI), cos(4.5/8*PI), cos(4.5/8*PI), cos(4.5/8*PI), cos(4.5/8*PI), },
                { cos(5.5/8*PI), cos(5.5/8*PI), cos(5.5/8*PI), cos(5.5/8*PI), cos(5.5/8*PI), cos(5.5/8*PI), cos(5.5/8*PI), cos(5.5/8*PI), },
                { cos(6.5/8*PI), cos(6.5/8*PI), cos(6.5/8*PI), cos(6.5/8*PI), cos(6.5/8*PI), cos(6.5/8*PI), cos(6.5/8*PI), cos(6.5/8*PI), },
                { cos(7.5/8*PI), cos(7.5/8*PI), cos(7.5/8*PI), cos(7.5/8*PI), cos(7.5/8*PI), cos(7.5/8*PI), cos(7.5/8*PI), cos(7.5/8*PI), },
            };
            expected = new double[,] {
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 6, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
                { 0, 0, 0, 0, 0, 0, 0, 0, },
            };
            yield return new object[] { data, expected };

            // --------------------------------
            data = new double[,] {
                { 150, 170, 132, 185, 147, 190, 215, 220, },
                { 165, 185, 130, 190, 175, 196, 223, 199, },
                { 155, 163, 180, 220, 202, 173, 197, 170, },
                { 143, 154, 160, 170, 211, 185, 190, 166, },
                { 130, 140, 172, 190, 193, 150, 180, 140, },
                { 135, 164, 198, 180, 177, 141, 172, 135, },
                { 170, 190, 163, 140, 165, 132, 160, 140, },
                { 160, 200, 145, 135 ,170, 199, 190, 129, },
            };
            expected = new double[,] {
                { 1362, -49, -39,  10, -23,  12, -64,   5, },
                {   62, -78,  16, -13,  35,  24,  -6, -42, },
                {    8,  -7,  76,  27, -27, -13, -10, -44, },
                {  -31,   1,   8, -40,   6,  12,   4,   3, },
                {   -9, -34,   0,  24, -10,  -7,   7,  -8, },
                {   -9,  10,  14, -10,  10, -13,  10,  11, },
                {   11,   3, -29, -20,  -7,  14,   9,   0, },
                {   -8,  14,  -8, -17,  16,  13,  -2,   2, },
            };
            yield return new object[] { data, expected };
        }

        /// <summary>
        /// This method generates 16x16 matrices (2x2 chunks) and plces the
        /// chunks generated by the `_SingleChunk` data provider in one or more
        /// chunks of the 16x16 matrix.
        /// </summary>
        static IEnumerable<object[]> _MultipleChunks()
        {
            double[,] data;
            double[,] expected;
            double[,] dataChunk;
            double[,] expectedChunk;

            // Locations within the test data matrix where the chunk should be
            // copied to
            var offsets = new (int X, int Y)[][] {
                new (int X, int Y)[] { (0, 0), }, // Top left
                new (int X, int Y)[] { (8, 0), }, // Top right
                new (int X, int Y)[] { (0, 8), }, // Bottom left
                new (int X, int Y)[] { (8, 8), }, // Bottom right
                new (int X, int Y)[] { (0, 0), (8, 0) }, // Top left and right
                new (int X, int Y)[] { (0, 8), (8, 8) }, // Bottom left and right
                new (int X, int Y)[] { (0, 0), (0, 8) }, // Left top and bottom
                new (int X, int Y)[] { (8, 0), (8, 8) }, // Right top and bottom
                new (int X, int Y)[] { (0, 0), (8, 8) }, // Diagonal top left and bottom right
                new (int X, int Y)[] { (0, 8), (8, 0) }, // Diagonal bottom left and top right
                new (int X, int Y)[] { (0, 0), (8, 0), (0, 8), (8, 8) }, // All 4 chunks
            };

            foreach (var destOffsets in offsets)
            {
                // Copy source data to the test data matrix
                foreach (var source in _SingleChunks())
                {
                    // Create a 16x16 input matrix and expected output matrix
                    data = new double[16, 16];
                    expected = new double[16, 16];

                    // Get the 8x8 test chunks
                    dataChunk = (double[,])source[0];
                    expectedChunk = (double[,])source[1];

                    foreach (var destOffset in destOffsets)
                    {
                        // Copy the chunks into the data / expected matrix
                        BlockCopy2D<double>(dataChunk, (0, 0), ref data, destOffset, (8, 8));
                        BlockCopy2D<double>(expectedChunk, (0, 0), ref expected, destOffset, (8, 8));
                    }
                    // Yield result
                    yield return new object[] { data, expected };
                }
            }
        }

        /// <summary>
        /// This method combines the horizontal and vertical rectangle input
        /// data.
        /// </summary>
        static IEnumerable<object[]> _RectangleInput()
        {
            foreach (var source in _HorizontalRectangleInput())
            {
                yield return source;
            }
            foreach (var source in _VerticalRectangleInput())
            {
                yield return source;
            }
        }

        /// <summary>
        /// This method generates 24x16 matrices (3x2 chunks) and plces the
        /// chunks generated by the `_SingleChunk` data provider in one or more
        /// chunks of the 24x16 matrix.
        /// </summary>
        static IEnumerable<object[]> _HorizontalRectangleInput()
        {
            double[,] data;
            double[,] expected;
            double[,] dataChunk;
            double[,] expectedChunk;

            // Locations within the test data matrix where the chunk should be
            // copied to
            var offsets = new (int X, int Y)[][] {
                new (int X, int Y)[] { (0, 0), }, // Top left
                new (int X, int Y)[] { (8, 0), }, // Top middle
                new (int X, int Y)[] { (16, 0), }, // Top right
                new (int X, int Y)[] { (0, 8), }, // Bottom left
                new (int X, int Y)[] { (8, 8), }, // Bootm middle
                new (int X, int Y)[] { (16, 8), }, // Bottom right
                new (int X, int Y)[] { (0, 0), (0, 8) }, // Left top and bottom 
                new (int X, int Y)[] { (8, 0), (8, 8) }, // Middle top and bottom
                new (int X, int Y)[] { (16, 0), (16, 8) }, // Right top and bottom
                new (int X, int Y)[] { (0, 0), (8, 0), (16, 0),
                                       (0, 8), (8, 8), (16, 8), }, // All 6 chunks
            };

            foreach (var destOffsets in offsets)
            {
                // Copy source data to the test data matrix
                foreach (var source in _SingleChunks())
                {
                    // Create a 24x16 input matrix and expected output matrix
                    data = new double[16, 24];
                    expected = new double[16, 24];

                    // Get the 8x8 test chunks
                    dataChunk = (double[,])source[0];
                    expectedChunk = (double[,])source[1];

                    foreach (var destOffset in destOffsets)
                    {
                        // Copy the chunks into the data / expected matrix
                        BlockCopy2D<double>(dataChunk, (0, 0), ref data, destOffset, (8, 8));
                        BlockCopy2D<double>(expectedChunk, (0, 0), ref expected, destOffset, (8, 8));
                    }
                    // Yield result
                    yield return new object[] { data, expected };
                }
            }
        }

        /// <summary>
        /// This method generates 16x24 matrices (2x3 chunks) and plces the
        /// chunks generated by the `_SingleChunk` data provider in one or more
        /// chunks of the 16x24 matrix.
        /// </summary>
        static IEnumerable<object[]> _VerticalRectangleInput()
        {
            double[,] data;
            double[,] expected;
            double[,] dataChunk;
            double[,] expectedChunk;

            // Locations within the test data matrix where the chunk should be
            // copied to
            var offsets = new (int X, int Y)[][] {
                new (int X, int Y)[] { (0, 0), }, // Top left
                new (int X, int Y)[] { (8, 0), }, // Top right
                new (int X, int Y)[] { (0, 8), }, // Middle left
                new (int X, int Y)[] { (8, 8), }, // Middle right
                new (int X, int Y)[] { (0, 16), }, // Bottom left
                new (int X, int Y)[] { (8, 16), }, // Bottom right
                new (int X, int Y)[] { (0, 0), (8, 0) }, // Top left and right
                new (int X, int Y)[] { (0, 8), (8, 8) }, // Middle left and right
                new (int X, int Y)[] { (0, 16), (8, 16) }, // Middle left and right
                new (int X, int Y)[] { (0, 0), (8, 0),
                                       (0, 8), (8, 8),
                                       (0, 16), (8, 16), }, // All 6 chunks
            };

            foreach (var destOffsets in offsets)
            {
                // Copy source data to the test data matrix
                foreach (var source in _SingleChunks())
                {
                    // Create a 16x24 input matrix and expected output matrix
                    data = new double[24, 16];
                    expected = new double[24, 16];

                    // Get the 8x8 test chunks
                    dataChunk = (double[,])source[0];
                    expectedChunk = (double[,])source[1];

                    foreach (var destOffset in destOffsets)
                    {
                        // Copy the chunks into the data / expected matrix
                        BlockCopy2D<double>(dataChunk, (0, 0), ref data, destOffset, (8, 8));
                        BlockCopy2D<double>(expectedChunk, (0, 0), ref expected, destOffset, (8, 8));
                    }
                    // Yield result
                    yield return new object[] { data, expected };
                }
            }
        }

        private static void BlockCopy2D<T>(
                T[,] src, (int X, int Y) srcOffset,
                ref T[,] dst, (int X, int Y) dstOffset,
                (int X, int Y) count)
        {
            var xOffsetSrc = srcOffset.X;
            var xOffsetDst = dstOffset.X;
            for (var x = 0; x < count.X; x++, xOffsetSrc++, xOffsetDst++)
            {
                var yOffsetSrc = srcOffset.Y;
                var yOffsetDst = dstOffset.Y;
                for (var y = 0; y < count.Y; y++, yOffsetSrc++, yOffsetDst++)
                {
                    dst[yOffsetDst, xOffsetDst] = src[yOffsetSrc, xOffsetSrc];
                }
            }
        }
    }
}
