using System;
using System.Collections.Generic;
using System.Linq;
using fhws.pixelschubser.jpg.encoder.HuffmanEncoder;
using fhws.pixelschubser.jpg.encoder.HuffmanEncoder.HuffmanTree;
using fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers;
using Moq;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest.HuffmanEncoder
{
    [TestFixture]
    public class HuffmanEncoderTest
    {
        [Test]
        public void Constructor()
        {
            var encoder = new HuffmanEncoder<byte>();
        }

        [TestCase(0x01, 1)]
        [TestCase(0x10, 2)]
        [TestCase(0x5f, 20)]
        public void EncodeSingleSymbols(int symbol, int frequency)
        {
            var data = new int[frequency];
            for (int i = 0; i < frequency; i++) data[i] = symbol;

            var expectedData = new List<int[]>();
            for (int i = 0; i < frequency; i++)
                expectedData.Add(new int[] { 0 });

            var encoder = new HuffmanEncoder<int>();
            var encoded = encoder.Encode(data);

            Assert.That(encoded.Data.Count, Is.EqualTo(frequency));
            Assert.That(encoded.Codes.Count, Is.EqualTo(1));

            Assert.That(encoded.Codes[symbol].Frequency, Is.EqualTo(frequency));
            Assert.That(encoded.Codes[symbol].Code, Is.EqualTo(new byte[] { 0 }));

            var encodedData = encoded.Data.ConvertAll(x => x.Code);
            Assert.That(encodedData, Is.EqualTo(expectedData),
                "Encoded data");
        }

        static object[][] MultipleSymbols = new[] {
            new object[] {
                // Data
                new int[] { 0, 1 },
                // Expected frequencies
                new Dictionary<int, int>() { [0] = 1, [1] = 1 },
                // Expected codes
                new Dictionary<int, byte[]>() {
                    [0] = new byte[] { 1 },
                    [1] = new byte[] { 0 },
                },
                // Expected encoded data
                new List<byte[]>() { new byte[] { 1 }, new byte[] { 0 } } },
            new object[] {
                // Data
                new int[] { 0, 1, 0 },
                // Expected frequencies
                new Dictionary<int, int>() { [0] = 2, [1] = 1 },
                // Expected codes
                new Dictionary<int, byte[]>() {
                    [0] = new byte[] { 0 },
                    [1] = new byte[] { 1 },
                },
                // Expected encoded data
                new List<byte[]>() {
                    new byte[] { 0 }, new byte[] { 1 }, new byte[] { 0 }
                }, },
            new object[] {
                // Data
                new int[] { 0, 1, 1, 0, 1, 2 },
                // Expected frequencies
                new Dictionary<int, int>() { [0] = 2, [1] = 3, [2] = 1},
                // Expected codes
                new Dictionary<int, byte[]>() {
                    [0] = new byte[] { 1, 0 },
                    [1] = new byte[] { 0 },
                    [2] = new byte[] { 1, 1 },
                },
                // Expected encoded data
                new List<byte[]>() {
                    new byte[] { 1, 0 }, new byte[] { 0 }, new byte[] { 0 },
                    new byte[] { 1, 0 }, new byte[] { 0 }, new byte[] { 1, 1 },
                } },
            new object[] {
                // Data
                new int[] { 0, 1, 1, 0, 1, 2, 3, 2, 0, 1},
                // Expected frequencies
                new Dictionary<int, int>() {
                    [0] = 3, [1] = 4, [2] = 2, [3] = 1
                },
                // Expected codes
                new Dictionary<int, byte[]>() {
                    [0] = new byte[] { 1, 0 },
                    [1] = new byte[] { 0 },
                    [2] = new byte[] { 1, 1, 0 },
                    [3] = new byte[] { 1, 1, 1 },
                },
                // Expected encoded data
                new List<byte[]>() {
                    new byte[] { 1, 0 }, new byte[] { 0 },
                    new byte[] { 0 }, new byte[] { 1, 0 },
                    new byte[] { 0 }, new byte[] { 1, 1, 0 },
                    new byte[] { 1, 1, 1 }, new byte[] { 1, 1, 0 },
                    new byte[] { 1, 0 }, new byte[] { 0 },
                } },
        };
        [TestCaseSource(nameof(MultipleSymbols))]
        public void EncodeMultipleSymbols(
                int[] data,
                Dictionary<int, int> expectedFrequencies,
                Dictionary<int, byte[]> expectedCodes,
                List<byte[]> expectedEncodedData)
        {
            var encoder = new HuffmanEncoder<int>();
            var encoded = encoder.Encode(data);

            var frequencies = encoded.Codes.ToDictionary(
                kv => kv.Key,
                kv => kv.Value.Frequency);
            Assert.That(frequencies, Is.EqualTo(expectedFrequencies),
                    "Symbol frequencies");

            var codes = encoded.Codes.ToDictionary(kv => kv.Key,
                                                   kv => kv.Value.Code);
            Assert.That(codes, Is.EqualTo(expectedCodes),
                    "Generated codes");

            var encodedData = encoded.Data.ConvertAll(x => x.Code);
            Assert.That(encodedData, Is.EqualTo(expectedEncodedData),
                    "Encoded data");
        }

        [TestCase(0x01, 1)]
        [TestCase(0x10, 2)]
        [TestCase(0x5f, 20)]
        public void BuildHuffmanTreeWithSingleSymbol(int symbol, int frequency)
        {
            var data = new int[frequency];
            for (int i = 0; i < frequency; i++) data[i] = symbol;

            var encoder = new HuffmanEncoder<int>();
            var tree = encoder.BuildHuffmanTree(data);
            var root = tree.Root;

            Assert.That(root.Height, Is.EqualTo(0));
            Assert.That(root.Frequency, Is.EqualTo(frequency));
            Assert.That(root.IsLeaf, Is.True);
        }

        [TestCase(0x01, 1)]
        [TestCase(0x10, 2)]
        [TestCase(0x5f, 20)]
        public void BuildHuffmanTreeFromDictWithSingleSymbol(int symbol, int frequency)
        {
            var dict = new Dictionary<int, Symbol<int>>()
            {
                [symbol] = new Symbol<int>(symbol, frequency),
            };

            var encoder = new HuffmanEncoder<int>();
            var tree = encoder.BuildHuffmanTree(dict);
            var root = tree.Root;

            Assert.That(root.Height, Is.EqualTo(0));
            Assert.That(root.Frequency, Is.EqualTo(frequency));
            Assert.That(root.IsLeaf, Is.True);
        }

        static object[][] MultipleCharSymbols = new[] {
            new object[] {
                // Data
                "AB".ToArray(),
                // Expected frequencies
                new Dictionary<char, int>() { ['A'] = 1, ['B'] = 1 },
                // Expected depths
                new Dictionary<char, int>() { ['A'] = 1, ['B'] = 1 }, },
            new object[] {
                // Data
                "ABA".ToArray(),
                // Expected frequencies
                new Dictionary<char, int>() { ['A'] = 2, ['B'] = 1 },
                // Expected depths
                new Dictionary<char, int>() { ['A'] = 1, ['B'] = 1 }, },
            new object[] {
                // Data
                "ABBABC".ToArray(),
                // Expected frequencies
                new Dictionary<char, int>() { ['A'] = 2, ['B'] = 3, ['C'] = 1 },
                // Expected depths
                new Dictionary<char, int>() { ['A'] = 2, ['B'] = 1, ['C'] = 2 }, },
            new object[] {
                // Data
                "ABBABCDCAB".ToArray(),
                /// Expected frequencies
                new Dictionary<char, int>() {
                    ['A'] = 3, ['B'] = 4, ['C'] = 2, ['D'] = 1 },
                // Expected depths
                new Dictionary<char, int>() {
                    ['A'] = 2, ['B'] = 1, ['C'] = 3, ['D'] = 3 }, },
            new object[] {
                // Data
                "ABCDEFAAABBBCCCCCDDDDDEEEEEEFFFFFFFFF".ToArray(),
                /// Expected frequencies
                new Dictionary<char, int>() {
                    ['A'] = 4, ['B'] = 4, ['C'] = 6, ['D'] = 6, ['E'] = 7, ['F'] = 10 },
                // Expected depths
                new Dictionary<char, int>() {
                    ['A'] = 3, ['B'] = 3, ['C'] = 3, ['D'] = 3, ['E'] = 2, ['F'] = 2 }, },
        };
        [TestCaseSource(nameof(MultipleCharSymbols))]
        /// <summary>
        /// Calculates the ABL (average bits per letter) for the generated tree
        /// and tests if it is optimal
        /// </summary>
        public void TreeIsOptimal(
                char[] data,
                Dictionary<char, int> expectedFrequencies,
                Dictionary<char, int> expectedDepths)
        {
            var tree = new HuffmanEncoder<char>()
                .BuildHuffmanTree(data);
            var root = tree.Root;

            // Calculate ABL
            var symbolFrequencies = new Dictionary<char, int>();
            var symbolDepths = new Dictionary<char, int>();
            Tree.DepthFirst<Node<char>, int>(root, 0, (node, depth, next) =>
                {
                    if (node.HasSymbol)
                    {
                        var symbol = node.Symbol.Value;
                        var freq = node.Symbol.Frequency;
                        symbolDepths.Add(symbol, depth);
                        symbolFrequencies.Add(symbol, freq);
                        return;
                    }
                    next(node.Left, (depth + 1));
                    next(node.Right, (depth + 1));
                });

            Assert.That(symbolFrequencies, Is.EqualTo(expectedFrequencies),
                "Frequencies");
            Assert.That(symbolDepths, Is.EqualTo(expectedDepths),
                "Symbols");
        }

    }
}
