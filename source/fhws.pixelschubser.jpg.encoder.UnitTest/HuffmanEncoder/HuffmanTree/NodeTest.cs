using System;
using System.Collections.Generic;
using System.Linq;
using fhws.pixelschubser.jpg.encoder.HuffmanEncoder;
using fhws.pixelschubser.jpg.encoder.HuffmanEncoder.HuffmanTree;
using fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers;
using Moq;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest.HuffmanEncoder.HuffmanTree
{
    [TestFixture]
    public class NodeTest
    {
        private static Node<byte> Factory(
            Node<byte> left = null,
            Node<byte> right = null)
        {
            return new Node<byte>(left, right);
        }

        private Mock<Symbol<T>> SymbolMockFactory<T>(
          T symbol = default(T),
          int frequency = 0)
        {
            var mock = new Mock<Symbol<T>>(symbol, frequency);
            return mock;

        }

        [Test]
        public void ConstructorWithNoArguments()
        {
            Assert.That(
                () => new Node<byte>(),
                Throws.Nothing
            );
        }

        [Test]
        public void ConstructorWithOnlyOneChild()
        {
            var left = new Node<byte>();
            var right = new Node<byte>();

            Assert.That(
                () => new Node<byte>(left: left),
                Throws.Nothing
            );
            Assert.That(
                () => new Node<byte>(right: right),
                Throws.Nothing
            );
        }

        [Test]
        public void ConstructorWithTwoChildren()
        {
            var left = new Node<byte>();
            var right = new Node<byte>();

            Assert.That(
                () => new Node<byte>(left: left, right: right),
                Throws.Nothing
            );
        }

        [Test]
        public void ConstructorWithSymbol()
        {
            var symbol = SymbolMockFactory<byte>().Object;

            Assert.That(
                () => new Node<byte>(symbol),
                Throws.Nothing
            );
        }

        [Test]
        public void ConstructorStoresSubtrees()
        {
            var left = new Node<char>();
            var right = new Node<char>();

            var treeBoth = new Node<char>(left, right);
            var treeLeft = new Node<char>(left: left);
            var treeRight = new Node<char>(right: right);
            var treeNone = new Node<char>();

            Assert.That(treeBoth.Left, Is.SameAs(left));
            Assert.That(treeBoth.Right, Is.SameAs(right));

            Assert.That(treeLeft.Left, Is.SameAs(left));
            Assert.That(treeLeft.Right, Is.Null);

            Assert.That(treeRight.Left, Is.Null);
            Assert.That(treeRight.Right, Is.SameAs(right));

            Assert.That(treeNone.Left, Is.Null);
            Assert.That(treeNone.Right, Is.Null);

        }

        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(1, 0)]
        [TestCase(1, 1)]
        [TestCase(3, 1)]
        [TestCase(1, 3)]
        [TestCase(8, 3)]
        [TestCase(3, 10)]
        public void ConstructorCalculatesHeightBasedOnChildren(int leftHeight, int rightHeight)
        {
            // Build left tree
            var leftTree = new Node<byte>();
            for (var i = 0; i < leftHeight; i++)
            {
                leftTree = new Node<byte>(left: leftTree);
            }
            // Build right tree
            Node<byte> rightTree = new Node<byte>();
            for (var i = 0; i < rightHeight; i++)
            {
                rightTree = new Node<byte>(right: rightTree);
            }

            Assert.That(leftTree.Height, Is.EqualTo(leftHeight));
            Assert.That(rightTree.Height, Is.EqualTo(rightHeight));

            // Combining trees increases height by 1
            var expectedCombinedHeight = 1 + Math.Max(leftHeight, rightHeight);

            var combinedTree = Factory(left: leftTree, right: rightTree);
            Assert.That(combinedTree.Height, Is.EqualTo(expectedCombinedHeight));
        }

        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(1, 0)]
        [TestCase(1, 1)]
        [TestCase(3, 1)]
        [TestCase(1, 3)]
        [TestCase(8, 3)]
        [TestCase(3, 10)]
        public void ConstructorCalculatesMinHeightBasedOnChildren(int leftMinHeight, int rightMinHeight)
        {
            var leftTree = new Node<byte>() { MinHeight = leftMinHeight };
            var rightTree = new Node<byte>() { MinHeight = rightMinHeight };

            Assert.That(leftTree.MinHeight, Is.EqualTo(leftMinHeight));
            Assert.That(rightTree.MinHeight, Is.EqualTo(rightMinHeight));

            // Combining trees increases path length by 1
            var expectedCombinedHeight = 1 + Math.Min(leftMinHeight, rightMinHeight);

            var combinedTree = new Node<byte>(leftTree, rightTree);
            Assert.That(combinedTree.MinHeight, Is.EqualTo(expectedCombinedHeight));
        }

        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(1, 1)]
        [TestCase(2, 3)]
        [TestCase(5, 7)]
        public void ConstructorCalculatesFrequency(int leftFreq, int rightFreq)
        {
            var expected = leftFreq + rightFreq;

            var left = new Node<int>();
            left.Frequency = leftFreq;
            var right = new Node<int>();
            right.Frequency = rightFreq;

            var treeBoth = new Node<int>(left, right);
            var treeLeft = new Node<int>(left: left);
            var treeRight = new Node<int>(right: right);

            Assert.That(treeBoth.Frequency, Is.EqualTo(expected));
            Assert.That(treeLeft.Frequency, Is.EqualTo(leftFreq));
            Assert.That(treeRight.Frequency, Is.EqualTo(rightFreq));
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(5)]
        [TestCase(9)]
        public void ConstructorGetsFrequencyFromSymbol(int freq)
        {
            var symbol = SymbolMockFactory<int>(frequency: freq).Object;
            var node = new Node<int>(symbol);

            Assert.That(node.Frequency, Is.EqualTo(freq));
        }

        [Test]
        public void NodeWithoutChildNodesIsALeaf()
        {
            var tree = new Node<byte>();
            Assert.That(tree.IsLeaf, Is.True);
        }

        [Test]
        public void NodeWithChildNodesIsNotALeaf()
        {
            var child = new Node<byte>();
            var treeBoth = new Node<byte>(child, child);
            var treeLeft = new Node<byte>(left: child);
            var treeRight = new Node<byte>(right: child);

            Assert.That(treeBoth.IsLeaf, Is.False);
            Assert.That(treeLeft.IsLeaf, Is.False);
            Assert.That(treeRight.IsLeaf, Is.False);
        }

        [Test]
        public void StoresSymbol()
        {
            var symbol = SymbolMockFactory<int>().Object;
            var leaf = new Node<int>(symbol);

            Assert.That(leaf.Symbol, Is.EqualTo(symbol));
        }

        [Test]
        public void FindParentNode()
        {

            var childA = new Node<char>();
            var parentA = new Node<char>(left: childA);

            var childB = new Node<char>();
            var parentB = new Node<char>(right: childB);

            var tree = new Node<char>(
                left: new Node<char>(
                    left: new Node<char>(),
                    right: new Node<char>(
                        right: new Node<char>()
                    )
                ),
                right: new Node<char>(
                    left: parentA,
                    right: new Node<char>(
                        left: new Node<char>(
                            right: parentB
                        )
                    )
                )
            );

            Assert.That(tree.FindParentOf(childA), Is.SameAs(parentA));
            Assert.That(tree.FindParentOf(childB), Is.SameAs(parentB));
        }

        //[Test]
        //public void GenerateCodes()
        //{
        //    var tree = new Node<int>(
        //        left: new Node<int>()
        //    )
        //}

        [TestCase('A', new byte[] { 0 })]
        [TestCase('B', new byte[] { 0 })]
        public void GenerateCodes_TreeIsLeaf(char symbol, byte[] expectedCode)
        {
            var symbolObj = new Symbol<char>(symbol);
            var tree = new Node<char>(symbolObj);

            Assert.That(tree.Symbol.Code, Is.Null);

            var symbols = tree.GenerateCodes(1);

            Assert.That(tree.Symbol.Code, Is.EqualTo(expectedCode));
            Assert.That(symbols.Count, Is.EqualTo(1));
            Assert.That(symbols[symbol], Is.EqualTo(symbolObj));
        }

        [TestCase('A', new byte[] { }, new byte[] { 0 })]
        [TestCase('B', new byte[] { 0 }, new byte[] { 0 })]
        [TestCase('C', new byte[] { 1 }, new byte[] { 1 })]
        [TestCase('A', new byte[] { 0, 0 }, new byte[] { 0, 0 })]
        [TestCase('B', new byte[] { 0, 1 }, new byte[] { 0, 1 })]
        [TestCase('C', new byte[] { 1, 0 }, new byte[] { 1, 0 })]
        [TestCase('D', new byte[] { 1, 1 }, new byte[] { 1, 1 })]
        [TestCase('A', new byte[] { 0, 1, 0, 0 }, new byte[] { 0, 1, 0, 0 })]
        public void GenerateCodes_TreeIsLeaf_WithPrefix(char symbol, byte[] prefix, byte[] expectedCode)
        {
            var symbolObj = new Symbol<char>(symbol);
            var tree = new Node<char>(symbolObj);

            Assert.That(tree.Symbol.Code, Is.Null);

            var symbols = tree.GenerateCodes(expectedCode.Length, prefix.ToList());

            Assert.That(tree.Symbol.Code, Is.EqualTo(expectedCode));
            Assert.That(symbols.Count, Is.EqualTo(1));
            Assert.That(symbols[symbol], Is.EqualTo(symbolObj));
        }

        static object[][] SymbolCodes = new[] {
            new object[] {
                // Tree
                new Node<char>(
                    left: new Node<char>(new Symbol<char>('A')),
                    right: new Node<char>(new Symbol<char>('B'))
                ),
                // Expected symbol codes
                new Dictionary<char, byte[]>() {
                    ['A'] = new byte[]{ 0 }, ['B'] = new byte[]{ 1 } } },
            new object[] {
                // Tree
                new Node<char>(
                    left: new Node<char>(new Symbol<char>('A')),
                    right: new Node<char>(
                        left: new Node<char>(new Symbol<char>('B')),
                        right: new Node<char>(new Symbol<char>('C')))
                ),
                // Expected symbol codes
                new Dictionary<char, byte[]>() {
                    ['A'] = new byte[]{ 0 }, ['B'] = new byte[]{ 1, 0 },
                    ['C'] = new byte[]{ 1, 1} } },
            new object[] {
                // Tree
                new Node<char>(
                    left: new Node<char>(
                        left: new Node<char>(new Symbol<char>('A')),
                        right: new Node<char>(new Symbol<char>('B'))),
                    right: new Node<char>(new Symbol<char>('C'))
                ),
                // Expected symbol codes
                new Dictionary<char, byte[]>() {
                    ['A'] = new byte[]{ 0, 0 }, ['B'] = new byte[]{ 0, 1 },
                    ['C'] = new byte[]{ 1 } } },
            new object[] {
                // Tree
                new Node<char>(
                    left: new Node<char>(
                        left: new Node<char>(new Symbol<char>('A')),
                        right: new Node<char>(new Symbol<char>('B'))),
                    right: new Node<char>(
                        left: new Node<char>(new Symbol<char>('C')),
                        right: new Node<char>(
                            left: new Node<char>(new Symbol<char>('D')),
                            right: new Node<char>(new Symbol<char>('E'))))
                ),
                // Expected symbol codes
                new Dictionary<char, byte[]>() {
                    ['A'] = new byte[]{ 0, 0 }, ['B'] = new byte[]{ 0, 1 },
                    ['C'] = new byte[]{ 1, 0 }, ['D'] = new byte[]{ 1, 1, 0 },
                    ['E'] = new byte[]{ 1, 1, 1 } } },
        };
        [TestCaseSource(nameof(SymbolCodes))]
        public void GenerateCodes(Node<char> tree, Dictionary<char, byte[]> expectedCodes)
        {
            var symbols = tree.GenerateCodes(100);

            Assert.That(symbols.Keys, Is.EqualTo(expectedCodes.Keys));
            symbols.Keys.ToList().ForEach(
                k => Assert.That(symbols[k].Code, Is.EqualTo(expectedCodes[k]),
                        $"Symbol: {k}")
            );
        }

        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void TrimTree_TreeIsLeaf(int height)
        {
            var tree = new Node<byte>(new Symbol<byte>(1));
            var removedLeaves = tree.Trim(height);

            Assert.That(removedLeaves.Count, Is.EqualTo(0));
            Assert.That(tree.IsLeaf, Is.True);
        }


        [TestCase(10)]
        [TestCase(5)]
        [TestCase(3)]
        public void TrimTree_DoesNotRemoveWhenTreeIsSmaller(int height)
        {
            var tree = new Node<byte>(
                left: new Node<byte>(new Symbol<byte>(0)),
                right: new Node<byte>(
                    left: new Node<byte>(
                        left: new Node<byte>(new Symbol<byte>(1)),
                        right: new Node<byte>()),
                    right: null));

            var removedLeaves = tree.Trim(height);
            Assert.That(removedLeaves.Count, Is.EqualTo(0));
        }

        [TestCase(2, 1)]
        [TestCase(1, 1)]
        [TestCase(0, 2)]
        public void TrimTree_ReturnsRemovedLeavesWithSymbol(int height, int expectedSymbolNodeLength)
        {
            var tree = new Node<byte>(
                left: new Node<byte>(new Symbol<byte>(0)),
                right: new Node<byte>(
                    left: new Node<byte>(
                        left: new Node<byte>(new Symbol<byte>(1)),
                        right: new Node<byte>()),
                    right: null));

            var removedLeaves = tree.Trim(height);
            Assert.That(removedLeaves.Count, Is.EqualTo(expectedSymbolNodeLength));
        }

        [TestCase(10, 3)]
        [TestCase(3, 3)]
        [TestCase(2, 2)]
        [TestCase(1, 1)]
        [TestCase(0, 0)]
        public void TrimTree_NewHeightIsCorrect(int maxHeight, int expectedHeight)
        {
            var tree = new Node<byte>(
                    left: new Node<byte>(new Symbol<byte>(0)),
                    right: new Node<byte>(
                        left: new Node<byte>(
                            left: new Node<byte>(new Symbol<byte>(1)),
                            right: new Node<byte>()),
                        right: null));

            tree.Trim(maxHeight);

            var maxDepth = 0;
            Tree.DepthFirst(tree, 0, (node, depth, next) =>
            {
                if (node.IsLeaf) maxDepth = Math.Max(maxDepth, depth);
                if (node.Left != null) next(node.Left, depth + 1);
                if (node.Right != null) next(node.Right, depth + 1);
            });

            Assert.That(maxDepth, Is.EqualTo(expectedHeight));
        }

        [Test]
        public void Children_ReturnEnumerable()
        {
            var node = new Node<int>();
            Assert.That(node.Children, Is.InstanceOf<IEnumerable<Node<int>>>());

        }

        [Test]
        public void Children_ContainsLeftAndRightChild()
        {
            var left = new Node<char>();
            var right = new Node<char>();
            var node = new Node<char>(left, right);

            var children = node.Children.ToList();

            Assert.That(children.Count, Is.EqualTo(2));
            Assert.That(children[0], Is.SameAs(left));
            Assert.That(children[1], Is.SameAs(right));
        }

        [Test]
        public void Children_ContainsOnlyLeftChildWhenRightIsNull()
        {
            var child = new Node<char>();
            var node = new Node<char>(left: child);

            var children = node.Children.ToList();

            Assert.That(children.Count, Is.EqualTo(1));
            Assert.That(children[0], Is.SameAs(child));
        }

        [Test]
        public void Children_ContainsOnlyRightChildWhenLeftIsNull()
        {
            var child = new Node<char>();
            var node = new Node<char>(right: child);

            var children = node.Children.ToList();

            Assert.That(children.Count, Is.EqualTo(1));
            Assert.That(children[0], Is.SameAs(child));
        }

        [Test]
        public void CreateBinaryTree_FromList_SingleNode()
        {
            var node = new Node<int>();
            var nodes = new List<Node<int>>() { node };

            var tree = Node<int>.BuildBinaryTree(nodes);

            Assert.That(tree.Height, Is.EqualTo(0));
            Assert.That(tree, Is.SameAs(node));
        }

        [Test]
        public void CreateBinaryTree_FromList_TwoNodes()
        {
            var left = new Node<int>();
            var right = new Node<int>();
            var nodes = new List<Node<int>>() { left, right };

            var tree = Node<int>.BuildBinaryTree(nodes);

            Assert.That(tree.Height, Is.EqualTo(1));
            Assert.That(tree.Left, Is.SameAs(left));
            Assert.That(tree.Right, Is.SameAs(right));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        [TestCase(10)]
        [TestCase(11)]
        [TestCase(50)]
        [TestCase(51)]
        [TestCase(1000)]
        [TestCase(1001)]
        public void CreateBinaryTree_FromList_ProducesRightGrowingTrees(int numLeaves)
        {
            var leaves = new List<Node<char>>();
            for (var i = 0; i < numLeaves; i++)
                leaves.Add(new Node<char>());

            // Expected height is the number of bit necessary to store the
            // number of the leaves. Example for 10 nodes: 10 is binary 1010, so
            // 4 bits are required to store 10 => expected tree height is 4.
            // This can also be calculated by `ceil(log2(numNodes))`
            var expectedHeight = Math.Ceiling(Math.Log2(numLeaves));

            var tree = Node<char>.BuildBinaryTree(leaves);

            Assert.That(tree.Height, Is.EqualTo(expectedHeight));
            Assert.That(Tree.IsRightGrowing<char>(tree), Is.True);
        }

        [Test]
        public void CreateBinaryTree_FromQueue_SingleNode()
        {
            var node = new Node<int>();
            var nodes = new Queue<Node<int>>();
            nodes.Enqueue(node);

            var tree = Node<int>.BuildBinaryTree(ref nodes);

            Assert.That(tree.Height, Is.EqualTo(0));
            Assert.That(tree, Is.SameAs(node));
            Assert.That(nodes, Is.Empty);
        }

        [Test]
        public void CreateBinaryTree_FromQueue_TwoNodes()
        {
            var left = new Node<int>();
            var right = new Node<int>();
            var nodes = new Queue<Node<int>>();
            nodes.Enqueue(left);
            nodes.Enqueue(right);

            var tree = Node<int>.BuildBinaryTree(ref nodes);

            Assert.That(tree.Height, Is.EqualTo(1));
            Assert.That(tree.Left, Is.SameAs(left));
            Assert.That(tree.Right, Is.SameAs(right));
            Assert.That(nodes, Is.Empty);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        [TestCase(10)]
        [TestCase(11)]
        [TestCase(50)]
        [TestCase(51)]
        [TestCase(1000)]
        [TestCase(1001)]
        public void CreateBinaryTree_FromQueue_ProducesRightGrowingTrees(int numLeaves)
        {
            var leaves = new Queue<Node<char>>();
            for (var i = 0; i < numLeaves; i++)
                leaves.Enqueue(new Node<char>());

            // Expected height is the number of bit necessary to store the
            // number of the leaves. Example for 10 nodes: 10 is binary 1010, so
            // 4 bits are required to store 10 => expected tree height is 4.
            // This can also be calculated by `ceil(log2(numNodes))`
            var expectedHeight = Math.Ceiling(Math.Log2(numLeaves));

            var tree = Node<char>.BuildBinaryTree(ref leaves);

            Assert.That(tree.Height, Is.EqualTo(expectedHeight));
            Assert.That(Tree.IsRightGrowing<char>(tree), Is.True);
            Assert.That(leaves, Is.Empty);
        }
    }
}
