using System.IO;
using fhws.pixelschubser.jpg.encoder.Models;
using fhws.pixelschubser.jpg.encoder.Structures;
using NUnit.Framework;


namespace fhws.pixelschubser.jpg.encoder.UnitTest
{
    [TestFixture]
    public class PPMReaderTest
    {
        private Picture<RGBPixel<byte>> picture;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var context = TestContext.CurrentContext;
            Directory.SetCurrentDirectory(context.WorkDirectory + "/../../..");
        }

        [SetUp]
        public void Setup()
        {
            this.picture = PPMReader.Read("test-assets/test.ppm", 8);
        }

        [Test]
        public void PictureShouldNotBeNull()
        {
            Assert.That(picture, Is.Not.Null);
        }

        [Test]
        public void ReadFileWithComments()
        {
            var picture = PPMReader.Read("test-assets/test_with_comments.ppm", 8);
            Assert.That(picture.Metadata.OrigWidth, Is.GreaterThan(3));
        }

        [Test]
        public void ReadPictureWithOddWidth()
        {
            var picture = PPMReader.Read("test-assets/odd_size.ppm", 8);
            Assert.That(picture.Metadata.OrigWidth, Is.GreaterThan(3));
        }

        [Test]
        public void VerifyDimensions()
        {
            Assert.That(picture.Metadata.OrigWidth, Is.EqualTo(4));
            Assert.That(picture.Metadata.OrigHeight, Is.EqualTo(6));
        }
    }
}