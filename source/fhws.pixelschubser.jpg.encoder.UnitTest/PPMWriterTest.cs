using System;
using System.IO;
using NUnit.Framework;


namespace fhws.pixelschubser.jpg.encoder.UnitTest
{
    [TestFixture]
    public class PPMWriterTest
    {

        const String tempFile = "temp_delme_writeTest.ppm";

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var context = TestContext.CurrentContext;
            Directory.SetCurrentDirectory(context.WorkDirectory + "/../../..");
        }

        [SetUp]
        public void Setup()
        {
        }


        [Test]
        public void WrittenFileShouldBeEqualToReadFile()
        {
            var picture = PPMReader.Read("test-assets/test.ppm", 8);
            PPMWriter.Write(tempFile, picture);
            var picture2 = PPMReader.Read(tempFile, 8);
            Assert.That(picture.Data, Is.EqualTo(picture2.Data));
        }

        [TearDown]
        public void Cleanup()
        {
            File.Delete(tempFile);
        }
    }
}