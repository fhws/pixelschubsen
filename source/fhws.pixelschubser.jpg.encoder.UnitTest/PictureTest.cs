using System;
using fhws.pixelschubser.jpg.encoder.Models;
using fhws.pixelschubser.jpg.encoder.Structures;
using fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest
{
    [TestFixture]
    public class PictureTest
    {
        [Test]
        public void CreatePicture()
        {
            var data = new RGBPixel<byte>[0, 0];
            var metadata = new PictureMetadata();
            new Picture<RGBPixel<byte>>(data, metadata);
        }

        [Test]
        public void DataIsUsed()
        {
            var data = new RGBPixel<byte>[5, 5];
            var metadata = new PictureMetadata();

            for (byte x = 0; x < data.GetLength(0); x++)
            {
                for (byte y = 0; y < data.GetLength(1); y++)
                {
                    data[x, y] = new RGBPixel<byte> { R = x, G = y, B = (byte)(x * y) };
                }
            }
            var picture = new Picture<RGBPixel<byte>>(data, metadata);

            Assert.That(picture[0, 0].R, Is.EqualTo(data[0, 0].R));
            Assert.That(picture[1, 1].G, Is.EqualTo(data[1, 1].G));
            Assert.That(picture[3, 4].B, Is.EqualTo(data[3, 4].B));

            Assert.That(picture[4, 4], Is.EqualTo(data[4, 4])
                .Using(new FieldComparer<RGBPixel<byte>>()));
        }

        [Test]
        public void MetadataStructIsCopied()
        {
            var data = new RGBPixel<byte>[0, 0];
            var metadata = new PictureMetadata();
            var picture = new Picture<RGBPixel<byte>>(data, metadata);

            // Change value of original metadata. This should not change the
            // metadata in the picture
            metadata.MaxColorValue = 1;

            Assert.That(picture.Metadata.MaxColorValue, Is.EqualTo(0));
        }

        [TestCase(new byte[] { 1, 2, 3, 4, 5, 6 })]
        [TestCase(new byte[] { 28, 7, 120, 255, 0, 12 })]
        public void MetadataIsUsed(byte[] meta)
        {
            var data = new RGBPixel<byte>[0, 0];
            var metadata = new PictureMetadata
            {
                MaxColorValue = meta[0],
                OrigHeight = meta[1],
                OrigWidth = meta[2],
                PaddedHeight = meta[3],
                PaddedWidth = meta[4],
                StepSize = meta[5],
            };
            var picture = new Picture<RGBPixel<byte>>(data, metadata);

            Assert.That(picture.Metadata, Is.EqualTo(metadata)
                .Using(new FieldComparer<PictureMetadata>()));
        }
    }
}
