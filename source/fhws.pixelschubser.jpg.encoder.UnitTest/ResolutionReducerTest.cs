using NUnit.Framework;
using fhws.pixelschubser.jpg.encoder.Structures;
using fhws.pixelschubser.jpg.encoder.Models;

namespace fhws.pixelschubser.jpg.encoder.UnitTest
{
    [TestFixture]
    public class ResolutionReducerTest
    {
        private uint pictureWidth = 8;
        private uint pictureHeight = 8;

        private Picture<YCBCRPixel<byte>> createPicture()
        {
            var pixelData = new YCBCRPixel<byte>[pictureWidth, pictureWidth];
            for (byte x = 0; x < pictureWidth; x++)
            {
                for (byte y = 0; y < pictureHeight; y++)
                {
                    pixelData[x, y] = new YCBCRPixel<byte> { Y = x, CB = y, CR = y };
                }
            }

            uint maxValue = 255;
            uint paddedWidth = pictureWidth;
            uint paddedHeight = pictureHeight;

            return new Picture<YCBCRPixel<byte>>(pixelData, new PictureMetadata
            {
                OrigWidth = pictureWidth,
                OrigHeight = pictureHeight,
                PaddedWidth = paddedWidth,
                PaddedHeight = paddedHeight,
                MaxColorValue = maxValue,
                StepSize = 16,
            });
        }

        private ResolutionReducer factory()
        {
            var picture = createPicture();
            return new ResolutionReducer(ref picture);
        }

        [Test]
        public void NoReduction()
        {
            var reducer = factory();
            var reducedImage = reducer.ReduceBy(new SampleReduceRate(1, 1, 1));

            Assert.That(reducedImage.Y.Length, Is.EqualTo(pictureWidth * pictureHeight));
            Assert.That(reducedImage.Cr.Length, Is.EqualTo(pictureWidth * pictureHeight));
            Assert.That(reducedImage.Cb.Length, Is.EqualTo(pictureWidth * pictureHeight));
        }

        [Test]
        public void ReduceTo_4_2_2()
        {
            var reducer = factory();
            var reducedImage = reducer.ReduceBy(new SampleReduceRate(1, 2, 2));

            // Y channel has all pixles while Cr and Cb only have half the information
            Assert.That(reducedImage.Y.Length, Is.EqualTo(pictureWidth * pictureHeight));
            Assert.That(reducedImage.Cr.Length, Is.EqualTo(pictureWidth * pictureHeight / 2));
            Assert.That(reducedImage.Cb.Length, Is.EqualTo(pictureWidth * pictureHeight / 2));
        }
    }
}
