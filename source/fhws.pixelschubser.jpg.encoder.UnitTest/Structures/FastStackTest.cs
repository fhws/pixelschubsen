using System;
using fhws.pixelschubser.jpg.encoder.Structures;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest.Structures
{
    [TestFixture]
    public class FastStackTest
    {
        [Test]
        public void Constructor()
        {
            Assert.That(
                () => new FastStack<byte>(),
                Throws.Nothing);
        }

        [Test]
        public void ConstructorWithCapacity()
        {
            Assert.That(
                () => new FastStack<byte>(10),
                Throws.Nothing);
        }

        [Test]
        public void InitialCapacityIsZero()
        {
            var stack = new FastStack<int>();
            Assert.That(stack.Capacity, Is.EqualTo(0));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(20)]
        public void SetCapacityThroughConstructo(int capacity)
        {
            var stack = new FastStack<char>(capacity);
            Assert.That(stack.Capacity, Is.EqualTo(capacity));
        }

        [Test]
        public void IsInitiallyEmpty()
        {
            var stack = new FastStack<byte>(1);
            Assert.That(stack.Count, Is.EqualTo(0));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        public void PushIncreasesCount(int count)
        {
            var stack = new FastStack<byte>(count);
            for (var i = 0; i < count; i++)
            {
                stack.Push(1);
            }

            Assert.That(stack.Count, Is.EqualTo(count));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        public void PopDecreasesCount(int count)
        {
            var stack = new FastStack<byte>(count);
            for (var i = 0; i < count; i++)
            {
                stack.Push(1);
            }

            Assert.That(stack.Count, Is.EqualTo(count));
            stack.Pop();
            Assert.That(stack.Count, Is.EqualTo(count - 1));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        public void PopReturnsTopItem(int count)
        {
            var stack = new FastStack<byte>(count);
            for (var i = 0; i < count; i++)
            {
                var elem = (byte)(i + 1);
                stack.Push(elem);
            }

            Assert.That(stack.Pop(), Is.EqualTo(count));
        }

        [Test]
        public void ThrowsWhenPopingEmptyStack()
        {
            var stack = new FastStack<byte>(1);
            Assert.That(
                () => stack.Pop(),
                Throws.InstanceOf<System.IndexOutOfRangeException>());
        }

        [Test]
        public void UsesDefaultCapacityWhenNotProvided()
        {
            var stack = new FastStack<byte>();

            Assert.That(stack.Capacity, Is.EqualTo(0));
            stack.Push(1);
            Assert.That(stack.Capacity, Is.EqualTo(8));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(12)]
        public void DoublesCapacityWhenPushingToFullStack(int capacity)
        {
            var stack = new FastStack<int>(capacity);

            Assert.That(stack.Capacity, Is.EqualTo(capacity));

            // Fill until stack is full
            for (var i = 0; i < capacity; i++) stack.Push(i);
            Assert.That(stack.Capacity, Is.EqualTo(capacity));
            Assert.That(stack.Count, Is.EqualTo(capacity));

            // Push another element so that the capacity must be increased
            stack.Push(0);

            var newCapacity = capacity * 2;
            Assert.That(stack.Capacity, Is.EqualTo(newCapacity));
        }

        [TestCase(new byte[] { }, 0)]
        [TestCase(new byte[] { }, 10)]
        [TestCase(new byte[] { 1 }, 1)]
        [TestCase(new byte[] { 1 }, 10)]
        [TestCase(new byte[] { 1, 2, 3 }, 3)]
        [TestCase(new byte[] { 1, 2, 3 }, 10)]
        public void ToArrayBottomTop(byte[] data, int maxSize)
        {
            var stack = new FastStack<byte>(maxSize);
            foreach (var elem in data)
            {
                stack.Push(elem);
            }

            var result = stack.ToArrayBottomTop();

            Assert.That(result, Is.EqualTo(data));
        }

        [Test]
        public void EmptyPropertyIsTrueWhenStackIsEmpty()
        {
            var stack = new FastStack<int>(10);

            Assert.That(stack.Empty, Is.True);
        }

        [TestCase(1)]
        [TestCase(5)]
        [TestCase(10)]
        public void EmptyPropertyIsFalseWhenStackIsNotEmpty(int itemsToPush)
        {
            var stack = new FastStack<int>(100);

            for (int i = 0; i < itemsToPush; i++)
            {
                stack.Push(i);
            }

            Assert.That(stack.Empty, Is.False);
        }

        [TestCase(1)]
        [TestCase(5)]
        [TestCase(10)]
        public void EmptyPropertyIsTrueWhenStackIsEmptyAgain(int itemsToPush)
        {
            var stack = new FastStack<int>(100);

            for (int i = 0; i < itemsToPush; i++)
            {
                stack.Push(i);
            }
            for (int i = 0; i < itemsToPush; i++)
            {
                stack.Pop();
            }

            Assert.That(stack.Empty, Is.True);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        public void PeekDoesNotDecreaseCount(int count)
        {
            var stack = new FastStack<byte>(count);
            for (var i = 0; i < count; i++)
            {
                stack.Push(1);
            }

            Assert.That(stack.Count, Is.EqualTo(count));
            stack.Peek();
            Assert.That(stack.Count, Is.EqualTo(count));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        public void PeekReturnsTopItem(int count)
        {
            var stack = new FastStack<byte>(count);
            for (var i = 0; i < count; i++)
            {
                var elem = (byte)(i + 1);
                stack.Push(elem);
            }

            Assert.That(stack.Peek(), Is.EqualTo(count));
        }

        [Test]
        public void ThrowsWhenPeekingEmptyStack()
        {
            var stack = new FastStack<byte>(1);
            Assert.That(
                () => stack.Peek(),
                Throws.InstanceOf<System.IndexOutOfRangeException>());
        }
    }
}
