using System;
using fhws.pixelschubser.jpg.encoder.Structures;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest.Structures
{
    [TestFixture]
    public class RefTest
    {
        [Test]
        public void Constructor()
        {
            Assert.That(
                () => new Ref<int>(1),
                Throws.Nothing);
        }

        [TestCase(1, 2)]
        [TestCase(5, 7)]
        public void ValueIsReference(byte orig, byte changed)
        {
            var data = new byte[] { orig };
            var r = new Ref<byte[]>(data);

            data[0] = changed;

            Assert.That(r.Value[0], Is.EqualTo(changed));
        }

        [TestCase('a', 'b')]
        [TestCase('x', 'y')]
        public void ValueCanBeChanged(char orig, char changed)
        {
            var r = new Ref<char>(orig);
            r.Value = changed;

            Assert.That(r.Value, Is.EqualTo(changed));
        }
    }
}
