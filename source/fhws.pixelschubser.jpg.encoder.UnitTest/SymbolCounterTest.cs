using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest
{
    [TestFixture]
    public class SymbolCounterTest
    {
        [Test]
        public void Constructor()
        {
            new SymbolCounter<byte>();
        }

        [Test]
        public void ResultIsEmptyWhenNothingHasBeenCounted()
        {
            var counter = new SymbolCounter<byte>();
            var emptyDict = new Dictionary<byte, int>();

            Assert.That(counter.Result, Is.EqualTo(emptyDict));
        }

        [TestCase((byte)'a')]
        [TestCase((byte)'b')]
        [TestCase((byte)'c')]
        public void CountSingleSymbols(byte symbol)
        {
            var counter = new SymbolCounter<byte>();
            var expected = new Dictionary<byte, int>()
            {
                [symbol] = 1
            };

            counter.Count(symbol);

            Assert.That(counter.Result, Is.EqualTo(expected));
        }

        static object[][] MultipleSymbols = new[] {
            new object[] {
                "aaa",
                new Dictionary<byte, int>() {
                    [(byte)'a'] = 3,
                } },
            new object[] {
                "bbbb",
                new Dictionary<byte, int>() {
                    [(byte)'b'] = 4,
                } },
            new object[] {
                "aaabbbb",
                new Dictionary<byte, int>() {
                    [(byte)'a'] = 3, [(byte)'b'] = 4,
                } },
            new object[] {
                "abcaba",
                new Dictionary<byte, int>() {
                    [(byte)'a'] = 3, [(byte)'b'] = 2, [(byte)'c'] = 1,
                } },
        };
        [TestCaseSource(nameof(MultipleSymbols))]
        public void CountMultipleSymbols(string text, Dictionary<byte, int> expected)
        {
            var counter = new SymbolCounter<byte>();
            var symbols = text.ToCharArray().Select(c => (byte)c).ToArray();

            foreach (var symbol in symbols)
            {
                counter.Count(symbol);
            }

            Assert.That(counter.Result, Is.EqualTo(expected));
        }

        [Test]
        public void ResetCounter()
        {
            var counter = new SymbolCounter<byte>();
            counter.Count(1);

            Assert.That(counter.Result.Count, Is.EqualTo(1));
            counter.Reset();
            Assert.That(counter.Result.Count, Is.EqualTo(0));
        }
    }
}
