using System;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers
{
    public static class BitBufferAccess
    {
        public static byte GetBitFromBuffer(byte[] buffer, int pos)
        {
            var byteOffset = (int)(pos / 8);
            var bitOffset = pos % 8;
            return GetMsbFromByte(buffer[byteOffset], bitOffset);
        }

        public static byte GetLsbFromByte(byte b, int offset = 0)
        {
            return (byte)((b >> offset) & 0x1);
        }

        public static byte GetMsbFromByte(byte b, int offset = 0)
        {
            // return (byte)((b >> (7-offset)) & 0x1 );
            return (byte)(((b << offset) & 0b_1000_0000) >> 7);
        }
    }
}

namespace fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers
{
    using static fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers.BitBufferAccess;

    [TestFixture]
    class BitBufferAccessTest
    {
        [TestCase(0b_0000_0000, 0)]
        [TestCase(0b_0000_0001, 1)]
        [TestCase(0b_1111_1110, 0)]
        [TestCase(0b_1111_1111, 1)]
        [TestCase(0b_1011_0001, 1)]
        [TestCase(0b_0011_0011, 1)]
        [TestCase(0b_1011_0000, 0)]
        [TestCase(0b_0011_0010, 0)]
        public void TestGetLsbFromByte(byte _byte, byte bit)
        {
            Assert.That(GetLsbFromByte(_byte), Is.EqualTo(bit));
        }

        [TestCase(0b_0000_0000, 0)]
        [TestCase(0b_1000_0000, 1)]
        [TestCase(0b_0111_1111, 0)]
        [TestCase(0b_1111_1111, 1)]
        [TestCase(0b_1011_0001, 1)]
        [TestCase(0b_1011_0010, 1)]
        [TestCase(0b_0000_0011, 0)]
        [TestCase(0b_0011_0010, 0)]
        public void TestGetMsgFromByte(byte _byte, byte bit)
        {
            Assert.That(GetMsbFromByte(_byte), Is.EqualTo(bit));
        }

        [TestCase(0b_1011_0001, 0, 1)]
        [TestCase(0b_1011_0001, 1, 0)]
        [TestCase(0b_1011_0001, 2, 0)]
        [TestCase(0b_1011_0001, 3, 0)]
        [TestCase(0b_1011_0001, 4, 1)]
        [TestCase(0b_1011_0001, 5, 1)]
        [TestCase(0b_1011_0001, 6, 0)]
        [TestCase(0b_1011_0001, 7, 1)]
        public void TestGetLsbFromByteWithOffset(byte _byte, int offset, byte bit)
        {
            Assert.That(GetLsbFromByte(_byte, offset), Is.EqualTo(bit));
        }

        [TestCase(0b_1011_0001, 0, 1)]
        [TestCase(0b_1011_0001, 1, 0)]
        [TestCase(0b_1011_0001, 2, 1)]
        [TestCase(0b_1011_0001, 3, 1)]
        [TestCase(0b_1011_0001, 4, 0)]
        [TestCase(0b_1011_0001, 5, 0)]
        [TestCase(0b_1011_0001, 6, 0)]
        [TestCase(0b_1011_0001, 7, 1)]
        public void TestGetMsbFromByteWithOffset(byte _byte, int offset, byte bit)
        {
            Assert.That(GetMsbFromByte(_byte, offset), Is.EqualTo(bit));
        }

        [TestCase(new byte[] { 0b_0111_1111, 0b_1111_1111 }, 0, 0)]
        [TestCase(new byte[] { 0b_1000_0000, 0b_0000_0000 }, 0, 1)]
        [TestCase(new byte[] { 0b_1111_1111, 0b_1111_1110 }, 15, 0)]
        [TestCase(new byte[] { 0b_0000_0000, 0b_0000_0001 }, 15, 1)]
        [TestCase(new byte[] { 0b_0101_1111, 0b_0000_0011 }, 3, 1)]
        [TestCase(new byte[] { 0b_0101_1111, 0b_0000_0011 }, 7, 1)]
        [TestCase(new byte[] { 0b_0101_1111, 0b_0000_0011 }, 8, 0)]
        [TestCase(new byte[] { 0b_0101_1111, 0b_0000_0011 }, 11, 0)]
        [TestCase(new byte[] { 0b_0101_1111, 0b_0000_0011 }, 14, 1)]
        [TestCase(new byte[] { 0b_0101_1111, 0b_0000_0011 }, 15, 1)]
        public void TestGetBitFromBuffer(byte[] buffer, int offset, byte bit)
        {

            Assert.That(GetBitFromBuffer(buffer, offset), Is.EqualTo(bit));
        }
    }
}
