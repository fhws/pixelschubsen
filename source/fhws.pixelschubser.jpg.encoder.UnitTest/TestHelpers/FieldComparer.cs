﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers
{
    class FieldComparer<T> : IEqualityComparer<T>
    {
        bool IEqualityComparer<T>.Equals(T actual, T expected)
        {
            bool allEq = true;
            foreach (var property in typeof(T).GetFields())
            {
                var actualVal = property.GetValue(actual);
                var expectedVal = property.GetValue(expected);
                var _actualValJson = JsonSerializer.Serialize(actualVal);
                var _expectedValJson = JsonSerializer.Serialize(expectedVal);

                if (!_actualValJson.Equals(_expectedValJson))
                {
                    allEq = false;
                    TestContext.WriteLine($"Property {property.Name}");
                    TestContext.WriteLine($"Expected: {expectedVal}");
                    TestContext.WriteLine($"But was:  {actualVal}\n");

                }
            }
            return allEq;
        }

        int IEqualityComparer<T>.GetHashCode(T obj)
        {
            return obj.GetHashCode();
        }
    }
}
