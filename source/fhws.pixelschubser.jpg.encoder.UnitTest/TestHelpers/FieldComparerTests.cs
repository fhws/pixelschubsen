using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using fhws.pixelschubser.jpg.encoder.Structures;
using fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers;
using NUnit.Framework.Internal;
using System.Linq;

namespace fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers
{
    [TestFixture]
    public class FieldComparerTests
    {
        TestStruct defaultData;
        FieldComparer<TestStruct> testComparer = new FieldComparer<TestStruct>();

        struct TestStruct
        {
            public int _int;
            public uint _uint;
            public float _float;
            public double _double;
            public long _long;
            public byte _byte;
            public short _short;
            public string _sting;
            public char[] _charArray;
            public List<int> _listOfInts;
            public List<string[]> _listOfStringArrays;
        }

        [SetUp]
        public void Setup()
        {
            defaultData = new TestStruct()
            {
                _int = -129,
                _uint = 1023,
                _float = 12.33f,
                _double = 48.5848834,
                _long = 54568864868525,
                _byte = 0b0110_1011,
                _short = 12155,
                _sting = "test String",
                _charArray = new char[] { 't', 'e', '\n', 's', 't' },
                _listOfInts = new List<int>() { 12, 158, 68, 66, -58, 3, 5 },
                _listOfStringArrays = new List<string[]>() {
                    new string[] { "test", "of strings", "in a list" },
                    new string[] { "some", "more in a", "string array" }}
            };
        }

        [Test]
        public void ShouldBeEqual()
        {
            TestStruct cpy = defaultData, cpy2 = defaultData;

            Assert.That(cpy, Is.EqualTo(cpy2).Using(testComparer));
        }

        [Test]
        public void NullShouldBeEqualToNull()
        {
            TestStruct cpy = defaultData, cpy2 = defaultData;
            cpy._sting = null;
            cpy2._sting = null;

            Assert.That(cpy2, Is.EqualTo(cpy).Using(testComparer));
        }

        [Test]
        public void FirstNullShouldNotBeEqual()
        {
            TestStruct cpy = defaultData, cpy2 = defaultData;
            cpy._sting = null;

            Assert.That(cpy2, Is.Not.EqualTo(cpy).Using(testComparer));
        }

        [Test]
        public void SecondNullShouldNotBeEqual()
        {
            TestStruct cpy = defaultData, cpy2 = defaultData;
            cpy2._sting = null;

            Assert.That(cpy2, Is.Not.EqualTo(cpy).Using(testComparer));
        }

        [TestCase("_int", -245)]
        [TestCase("_int", 2345)]
        [TestCase("_uint", (uint)332202)]
        [TestCase("_float", 0.3313f)]
        [TestCase("_float", -3.3313f)]
        [TestCase("_double", 48.5848123)]
        [TestCase("_double", 412.5848834)]
        [TestCase("_double", -48.5848834)]
        [TestCase("_long", (long)35556184868525)]
        [TestCase("_long", (long)-34868555544425)]
        [TestCase("_byte", (byte)0b0111_1000)]
        [TestCase("_short", (short)-12155)]
        [TestCase("_short", (short)12151)]
        [TestCase("_sting", "tast Strang")]
        [TestCase("_sting", null)]
        [TestCase("_charArray", new char[] { 'd', 'e', '\n', 's', 'd' })]
        [TestCase("_charArray", new char[] { 'd', 'e', '\n', 's', 'd', 'd' })]
        [TestCase("_charArray", null)]
        [TestCase("_listOfInts", null)]
        [TestCase("_listOfStringArrays", null)]
        public void ShouldNotBeEqual(string field, object value)
        {
            object _cpy = defaultData; // Boxed copy of expected
            // Dynamically sets the 'field' to the 'value' on the 'cpy' object
            _cpy.GetType().GetField(field).SetValue(_cpy, value);
            // Copy Boxed "_cpy" into the unboxed (value type) 'cpy'
            var modifiedCpy = (TestStruct)_cpy;

            Assert.That(defaultData,
                   Is.Not.EqualTo(modifiedCpy)
                   .Using(testComparer));
        }

        [Test]
        public void ShouldBeEqualListOfStringArrays()
        {
            TestStruct cpy = defaultData, cpy2 = defaultData;
            // same as in defaultData
            cpy._listOfStringArrays = new List<string[]>() {
                    new string[] { "test", "of strings", "in a list" },
                    new string[] { "some", "more in a", "string array" }};

            Assert.That(cpy, Is.EqualTo(cpy2).Using(testComparer));
        }

        [Test]
        public void ShouldBeEqualListOfInts()
        {
            TestStruct cpy = defaultData, cpy2 = defaultData;
            // same as in defaultData
            cpy._listOfInts = new List<int>() { 12, 158, 68, 66, -58, 3, 5 };

            Assert.That(cpy, Is.EqualTo(cpy2).Using(testComparer));
        }

        [TestCase("_int", -129)]
        [TestCase("_uint", (uint)1023)]
        [TestCase("_float", 12.33f)]
        [TestCase("_double", 48.5848834)]
        [TestCase("_long", (long)54568864868525)]
        [TestCase("_byte", (byte)0b0110_1011)]
        [TestCase("_short", (short)12155)]
        [TestCase("_sting", "test String")]
        [TestCase("_charArray", new char[] { 't', 'e', '\n', 's', 't' })]
        public void ShouldBeEqual(string field, object value)
        {
            object _cpy = defaultData; // Boxed copy of expected
            // Dynamically sets the 'field' to the 'value' on the 'cpy' object
            _cpy.GetType().GetField(field).SetValue(_cpy, value);
            // Copy Boxed "_cpy" into the unboxed (value type) 'cpy'
            var modifiedCpy = (TestStruct)_cpy;

            Assert.That(defaultData,
                   Is.EqualTo(modifiedCpy)
                   .Using(testComparer));
        }

        [TestCase(new int[] { 12, 158, 68, 66, -58, 3, 4 })]
        [TestCase(new int[] { 12, 158, 68, 66, -58, 3 })]
        public void ShouldNotBeEqualListOfInts(int[] arr)
        {
            List<int> list = arr.ToList<int>();
            TestStruct cpy = defaultData, cpy2 = defaultData;
            cpy._listOfInts = list;

            Assert.That(cpy, Is.Not.EqualTo(cpy2).Using(testComparer));
        }

        [Test]
        public void ShouldNotBeEqualListOfStringArraysContentChange()
        {
            var _ = new List<string[]>() {
                        new string[] { "test", "of strings", "in a list" },
                        new string[] { "only one ", "changed ", "a little" }};

            TestStruct cpy = defaultData, cpy2 = defaultData;
            cpy._listOfStringArrays = _;

            Assert.That(cpy, Is.Not.EqualTo(cpy2).Using(testComparer));
        }

        [Test]
        public void ShouldNotBeEqualListOfStringArraysOneMissing()
        {
            var _ = new List<string[]>() {
                        new string[] { "test", "of strings", "in a list" }};

            TestStruct cpy = defaultData, cpy2 = defaultData;
            cpy._listOfStringArrays = _;

            Assert.That(cpy, Is.Not.EqualTo(cpy2).Using(testComparer));
        }

        [Test]
        public void ShouldNotBeEqualListOfStringArraysOneNull()
        {
            TestStruct cpy = defaultData, cpy2 = defaultData;
            cpy._listOfStringArrays = null;

            Assert.That(cpy, Is.Not.EqualTo(cpy2).Using(testComparer));
        }

        [Test]
        public void nullStringShouldNotBeEqualNull()
        {
            TestStruct cpy = defaultData, cpy2 = defaultData;
            cpy._sting = null;
            cpy2._sting = "null";

            Assert.That(cpy, Is.Not.EqualTo(cpy2).Using(testComparer));
        }
    }
}