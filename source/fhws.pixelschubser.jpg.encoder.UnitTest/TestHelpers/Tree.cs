using System;
using System.Collections.Generic;
using fhws.pixelschubser.jpg.encoder.HuffmanEncoder.HuffmanTree;
using NUnit.Framework;

namespace fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers
{
    public class Tree
    {
        public delegate T_Return DepthFirstNext<T_Tree, T_Data, T_Return>(T_Tree tree, T_Data data);
        public delegate void DepthFirstNext<T_Tree, T_Data>(T_Tree tree, T_Data data);

        // TODO: Write tests
        public static void DepthFirst<T_Tree, T_Data>(
            T_Tree tree,
            T_Data data,
            Action<T_Tree, T_Data, DepthFirstNext<T_Tree, T_Data>> action)
        {
            DepthFirstNext<T_Tree, T_Data> next = null;
            next = new DepthFirstNext<T_Tree, T_Data>((tree, data) =>
            {
                action(tree, data, next);
            });
            next(tree, data);
        }

        // TODO: Write tests
        public static T_Return DepthFirst<T_Tree, T_Data, T_Return>(
            T_Tree tree,
            T_Data data,
            Func<T_Tree, T_Data, DepthFirstNext<T_Tree, T_Data, T_Return>, T_Return> action)
        {
            DepthFirstNext<T_Tree, T_Data, T_Return> next = null;
            next = new DepthFirstNext<T_Tree, T_Data, T_Return>((tree, data) =>
            {
                return action(tree, data, next);
            });
            return next(tree, data);
        }

        public static bool IsRightGrowing<T>(Node<T> node, List<Node<T>> failedNodes = null)
        {
            var (ok, _, _) = _IsRightGrowing<T>(node, failedNodes);
            return ok;
        }

        private static (bool, int, int) _IsRightGrowing<T>(Node<T> node, List<Node<T>> failedNodes = null)
        {
            if (node == null) return (true, -1, -1);
            if (node.IsLeaf) return (true, 0, 0);

            var (leftOk, leftMin, leftMax) = _IsRightGrowing<T>(node.Left, failedNodes);
            var (rightOk, rightMin, rightMax) = _IsRightGrowing<T>(node.Right, failedNodes);

            var ok = leftMax <= rightMin;

            if (!ok)
            {
                failedNodes?.Add(node);
            }

            return (
                ok && leftOk && rightOk,
                Math.Min(leftMin, rightMin) + 1,
                Math.Max(leftMax, rightMax) + 1
            );
        }
    }
}

namespace fhws.pixelschubser.jpg.encoder.UnitTest.TestHelpers
{
    [TestFixture]
    public class TreeTest
    {
        [Test]
        public void IsRightGrowing_SingleNode()
        {
            var tree = new Node<int>();
            Assert.That(Tree.IsRightGrowing<int>(tree), Is.True);
        }

        [Test]
        public void IsRightGrowing_OnlyRightChildIsRightGrowing()
        {
            var tree = new Node<int>(
                right: new Node<int>()
            );
            Assert.That(Tree.IsRightGrowing<int>(tree), Is.True);
        }

        [Test]
        public void IsRightGrowing_OnlyLeftChildIsNotRightGrowing()
        {
            var tree = new Node<int>(
                left: new Node<int>()
            );
            Assert.That(Tree.IsRightGrowing<int>(tree), Is.False);
        }

        [Test]
        public void IsRightGrowing_NullIsValid()
        {
            Assert.That(Tree.IsRightGrowing<int>(null), Is.True);
        }

        [Test]
        public void IsRightGrowing_TreeIsInvalidWhenSubtreeIsInvalid()
        {
            var validTree = new Node<byte>(
                left: new Node<byte>(),
                right: new Node<byte>()
            );
            var invalidTree = new Node<byte>(
                left: new Node<byte>()
            );

            Assert.That(Tree.IsRightGrowing<byte>(validTree), Is.True);
            Assert.That(Tree.IsRightGrowing<byte>(invalidTree), Is.False);

            var treeLeftInvalid = new Node<byte>(
                left: invalidTree,
                right: validTree
            );
            var treeRightInvalid = new Node<byte>(
                left: invalidTree,
                right: validTree
            );

            var leftInvalidResult = Tree.IsRightGrowing<byte>(treeLeftInvalid);
            var rightInvalidResult = Tree.IsRightGrowing<byte>(treeRightInvalid);

            Assert.That(leftInvalidResult, Is.False);
            Assert.That(rightInvalidResult, Is.False);
        }

        [Test]
        public void IsRightGrowing_CollectInvalidNodes()
        {
            var validTree = new Node<byte>(
                left: new Node<byte>(),
                right: new Node<byte>()
            );
            var invalidTree = new Node<byte>(
                left: new Node<byte>()
            );

            Assert.That(Tree.IsRightGrowing<byte>(validTree), Is.True);
            Assert.That(Tree.IsRightGrowing<byte>(invalidTree), Is.False);

            var treeLeftInvalid = new Node<byte>(
                left: invalidTree,
                right: validTree
            );
            var treeRightInvalid = new Node<byte>(
                left: invalidTree,
                right: validTree
            );

            var leftList = new List<Node<byte>>();
            var rightList = new List<Node<byte>>();
            Tree.IsRightGrowing<byte>(treeLeftInvalid, leftList);
            Tree.IsRightGrowing<byte>(treeRightInvalid, rightList);

            Assert.That(leftList.Count, Is.EqualTo(1));
            Assert.That(leftList[0], Is.SameAs(invalidTree));

            Assert.That(rightList.Count, Is.EqualTo(1));
            Assert.That(rightList[0], Is.SameAs(invalidTree));
        }
    }
}
