using System;
using System.IO;
using fhws.pixelschubser.jpg.encoder.Structures;
using NUnit.Framework;
using static fhws.pixelschubser.jpg.encoder.ZickZackSampler;

namespace fhws.pixelschubser.jpg.encoder.UnitTest
{
    [TestFixture]
    public class ZickZackSamplerTests
    {
        sbyte[,] array = new sbyte[10, 10]
        {
            {-1, -1,  -1, -1, -1, -1, -1, -1, -1, -1 },
            {-1,  12, -3,  1,  0,  0,  0,  0,  0, -1 },
            {-1, -5,   3, -1,  1,  0,  0,  0,  0, -1 },
            {-1,  2,   0, -1,  1, -1,  0,  0,  0, -1 },
            {-1, -1,  -2,  1,  0,  0,  0,  0,  0, -1 },
            {-1,  0,   0,  0,  0,  0,  0,  0,  0, -1 },
            {-1,  0,   0,  0,  0,  0,  0,  0,  0, -1 },
            {-1,  0,   0,  0,  0,  0,  0,  0, 11, -1 },
            {-1,  0,   0,  0,  0,  0,  0, -3, 16, -1 },
            {-1, -1,  -1, -1, -1, -1, -1, -1, -1, -1 }
        };

        [SetUp]
        public void Setup()
        {
            
        }

        [Test]
        public void TestZickZackSample()
        {
            var expected = new sbyte[] { 12, -3, -5, 2, 3, 1, 0, -1, 0, -1, 0,
                -2, -1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 11, -3, 16 };

            Assert.That(true == true, Is.True);
            var temp = ZickZackSampleChunk(array, 1, 1);
            Assert.That(temp, Is.EquivalentTo(expected));
        }
    }
}