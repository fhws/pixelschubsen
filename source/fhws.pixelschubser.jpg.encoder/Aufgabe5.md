Aufgabenblatt 5
==================

Aufgabe 5a
-------------
### Nicht verwendet
- `Arithmetic coding` **S. 54**
- `Annex G – Progressive DCT-based mode of operation` **S. 119**
  - Mehr Details pro Durchlauf
- `Annex H – Lossless mode of operation` **S. 132**
  - Verlustfreier Modus
- `Annex J – Hierarchical mode of operation` **S. 137**
  - Bilder in mehreren Auflösungen
- `DCT Sample precision of 12-bit`
- `Extended DCT-based processes` 
  - Hierzu zählen sequential/progressive

### Verwendet
- `Annex F – Sequential DCT-based mode of operation` $\widehat{=}$ `Baseline process` **S. 87**
- `Huffman coding`
- `DCT Sample precision of 8-bit`


### Quellen
- `Modes of operation` **S. 17**
- `Entropy coding alternatives` **S. 18**
- `Summary of coding processes` **S. 22 ff.**
- `Zig-zag sequence` **S. 28-30**

Aufgabe 5b
-------------
In der Luminiszenz werden Details viel feiner Quantisiert als in den Farbwerten.
![The Quantization Table from ITU.81 for Y](assets/a5bQuantTableY.png)
![The Quantization Table from ITU.81 for CbCr](assets/a5bQuantTableCbCr.png)

Source: `Annex K – Examples and guidelines - S. 143`

Aufgabe 5c
-------------

- Aus den Bildern auslesen - da diese für die Darstellung benötigt werden, sind sie auch in den Bildern gespeichert. 
- JPEG Snoop zeigt sie auch an, hier ein Beispiel von meinem Galaxy S7:

```
  *** Marker: DQT (xFFDB) ***
  Define a Quantization Table.
  OFFSET: 0x000071BE
  Table length = 132
  ----
  Precision=8 bits
  Destination ID=0 (Luminance)
    DQT, Row #0:   1   1   1   1   2   3   4   5 
    DQT, Row #1:   1   1   1   2   2   5   5   4 
    DQT, Row #2:   1   1   1   2   3   5   6   4 
    DQT, Row #3:   1   1   2   2   4   7   6   5 
    DQT, Row #4:   1   2   3   4   5   9   8   6 
    DQT, Row #5:   2   3   4   5   6   8   9   7 
    DQT, Row #6:   4   5   6   7   8  10  10   8 
    DQT, Row #7:   6   7   8   8   9   8   8   8 
    Approx quality factor = 96.06 (scaling=7.87 variance=0.69)
  ----
  Precision=8 bits
  Destination ID=1 (Chrominance)
    DQT, Row #0:   1   1   2   4   8   8   8   8 
    DQT, Row #1:   1   2   2   5   8   8   8   8 
    DQT, Row #2:   2   2   4   8   8   8   8   8 
    DQT, Row #3:   4   5   8   8   8   8   8   8 
    DQT, Row #4:   8   8   8   8   8   8   8   8 
    DQT, Row #5:   8   8   8   8   8   8   8   8 
    DQT, Row #6:   8   8   8   8   8   8   8   8 
    DQT, Row #7:   8   8   8   8   8   8   8   8 
    Approx quality factor = 96.02 (scaling=7.97 variance=0.33)
```

Zu Beginn hat man schöne Werte von 0 - 255.
Durch die DCT werden daraus Floats durch die Quantisierung können wir nun die hochfrequenten Details (in der Quantisierungstabelle unten rechts) stärker herausfiltern lassen.
Die DCT Koeffizienten werden dann durch die Quantisierungswerte geteilt und zum nächsten Integer gerundet. dadurch entstehen unten rechts häufig viele 0er (da unten rechts die werte stärker Quantisiert werden [höhere zahlen])

Aufgabe 5d
-------------

![DQT revesed to see what precision in the formula means](assets/a5DQTReverse.png)

Aufgabe 5f
-------------

DC AC is checked to have the right order in the image!
![SOS revesed](assets/a5SOSReverse.png)

