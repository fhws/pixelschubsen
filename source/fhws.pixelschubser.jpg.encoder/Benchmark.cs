using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace fhws.pixelschubser.jpg.encoder
{
    public class Benchmark
    {
        private class BenchmarkConfig
        {
            public int Iterations = 0;
            public int WarmupIterations = 0;
            public Action Test;
            public Action BeforeEach;
            public Action AfterEach;
            public bool CollectGarbageAfterRun = false;

            //Run at highest priority to minimize fluctuations caused by other processes/threads
            public ProcessPriorityClass ProcessPriority = ProcessPriorityClass.High;
            public ThreadPriority ThreadPriority = ThreadPriority.Highest;
        }

        private struct ProcessState
        {
            public ProcessPriorityClass ProcessPriority;
            public ThreadPriority ThreadPriority;
            public ConsoleColor ConsoleForegroundColor;
        }

        public readonly string Name;

        private BenchmarkConfig Config = new BenchmarkConfig();
        private ProcessState StateBackup = new ProcessState();
        private ConsoleCancelEventHandler CancelHandler;
        private bool Cancel = false;

        public Benchmark(string name)
        {
            Name = name;
        }

        [Obsolete("Use the `Run(Action test)` method instead")]
        public double Run()
        {
            CheckBenchmarkSetup();

            // NOTE: This has been moved in this obsolete method to keep the old
            //       check because the new API does not need this config field
            //       anymore
            if (Config.Test == null)
                throw new ArgumentOutOfRangeException(
                    $@"Benchmark ""{Name}"" has not assigned Test");

            Setup();

            PrintName();
            RunWarmup(Config.Test);

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            RunBenchmark(Config.Test);

            Teardown();

            return stopwatch.Elapsed.TotalMilliseconds;
        }

        public void Run(Action test)
        {
            CheckBenchmarkSetup();

            Setup();

            PrintName();
            RunWarmup(test);

            RunBenchmark(test);

            Teardown();
        }

        private void CheckBenchmarkSetup()
        {
            if (Config.Iterations < 1)
                throw new ArgumentOutOfRangeException(
                    $@"Iterations for benchmark ""{Name}"" must be greater than 0");
        }

        private void RunWarmup(Action test)
        {
            for (var i = 0; i < Config.WarmupIterations; i++)
            {
                var currentIteration = i + 1;
                WriteWarmupProgress(currentIteration);

                if (Cancel)
                {
                    WriteWarmupProgress(currentIteration);
                    ExitCancel();
                }

                RunIteration(test);
            }
        }

        private void RunBenchmark(Action test)
        {
            var minTime = double.MaxValue;
            var maxTime = 0.0;
            var median = 0.0;
            var average = 0.0;
            var totalTime = 0.0;
            var lastMeasurement = 0.0;
            List<double> laps = new List<double>();

            var stopwatch = new Stopwatch();

            for (int i = 0; i < Config.Iterations; i++)
            {
                var currentIteration = i + 1;
                WriteStatus(currentIteration, median, average, minTime, maxTime, lastMeasurement);

                if (Cancel)
                {
                    WriteStatus(currentIteration, median, average, minTime, maxTime, lastMeasurement);
                    ExitCancel();
                }

                stopwatch.Reset();
                RunIteration(test, stopwatch);
                var elapsedTime = stopwatch.Elapsed.TotalMilliseconds;
                laps.Add(elapsedTime);

                median = CalculateMedian(laps);
                minTime = Math.Min(minTime, elapsedTime);
                maxTime = Math.Max(maxTime, elapsedTime);
                totalTime += elapsedTime;
                average = totalTime / currentIteration;

                lastMeasurement = elapsedTime;
            }

            WriteResult(median, average, minTime, maxTime, totalTime);
        }

        private void RunIteration(Action test, Stopwatch stopwatch = null)
        {
            Config.BeforeEach?.Invoke();
            stopwatch?.Start();
            test.Invoke();
            stopwatch?.Stop();
            Config.AfterEach?.Invoke();

            if (Config.CollectGarbageAfterRun) CollectGarbage();
        }

        private void Setup()
        {
            // Backup state
            StateBackup.ProcessPriority = Process.GetCurrentProcess().PriorityClass;
            StateBackup.ThreadPriority = Thread.CurrentThread.Priority;
            StateBackup.ConsoleForegroundColor = Console.ForegroundColor;

            // Set process priority
            Process.GetCurrentProcess().PriorityClass = Config.ProcessPriority;
            Thread.CurrentThread.Priority = Config.ThreadPriority;

            // Register event handlers
            CancelHandler = new ConsoleCancelEventHandler(ConsoleCancelHandler);
            Console.CancelKeyPress += CancelHandler;
        }

        private void Teardown()
        {
            // Restore state
            Process.GetCurrentProcess().PriorityClass = StateBackup.ProcessPriority;
            Thread.CurrentThread.Priority = StateBackup.ThreadPriority;
            Console.ForegroundColor = StateBackup.ConsoleForegroundColor;

            // Remove event handlers
            Console.CancelKeyPress -= CancelHandler;
        }
        private void CollectGarbage()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        private double CalculateMedian(List<double> list)
        {
            list.Sort();

            var isOdd = (list.Count % 2) == 1;
            var middleIdx = (int)(list.Count / 2);

            if (isOdd)
            {
                // Select the middle element
                return list[middleIdx];
            }

            // For an even number of elements, select the both middle elements
            // and calculate their average
            var middleLeft = list[middleIdx - 1];
            var middleRight = list[middleIdx];
            return (middleLeft + middleRight) / 2;
        }

        #region ConsoleOutput

        private void PrintName()
        {
            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            ConsoleAnsiSetBold();
            ConsoleAnsiSetUnderline();
            Console.WriteLine(Name + ":");
            ConsoleAnsiReset();
            ConsoleResetForegroundColor();
        }

        private void WriteWarmupProgress(int currentIteration)
        {
            ConsoleClearCurrentLine();

            ConsoleResetForegroundColor();
            System.Console.Write($"    {currentIteration}/{Config.WarmupIterations}");
            Console.ForegroundColor = ConsoleColor.Yellow;
            System.Console.Write($" (warmup)");
            ConsoleResetForegroundColor();
            ConsoleSetCursorToBeginningOfLine();
            System.Console.Out.Flush();
        }

        private void WriteStatus(int currentIteration,
                                 double median,
                                 double average,
                                 double minTime,
                                 double maxTime,
                                 double lastMilliseconds)
        {
            ConsoleClearCurrentLine();
            System.Console.Write($"    {currentIteration}/{Config.Iterations} (");
            if (average > 0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                System.Console.Write($@"med: {median:0.0000};");
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                System.Console.Write($@" avg: {average:0.0000};");
                Console.ForegroundColor = ConsoleColor.Green;
                System.Console.Write($@" min: {minTime:0.0000};");
                Console.ForegroundColor = ConsoleColor.Red;
                System.Console.Write($@" max: {maxTime:0.0000};");
                Console.ForegroundColor = ConsoleColor.White;
                System.Console.Write($@" last: {lastMilliseconds:0.0000}");
                ConsoleResetForegroundColor();
                System.Console.Write($@")");
            }
            ConsoleSetCursorToBeginningOfLine();
            System.Console.Out.Flush();
        }

        private void WriteResult(double median,
                                 double average,
                                 double minTime,
                                 double maxTime,
                                 double totalTime)
        {
            ConsoleClearCurrentLine();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"    Total:   {totalTime:0.0000} ms ({Config.Iterations} iterations)");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"    Minimum: {minTime:0.0000} ms");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"    Maximum: {maxTime:0.0000} ms");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine($"    Average: {average:0.0000} ms");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"    Median:  {median:0.0000} ms");
            ConsoleResetForegroundColor();
            System.Console.WriteLine("");
        }

        private void ConsoleCancelHandler(object sender, ConsoleCancelEventArgs args)
        {
            if (!Cancel)
            {
                args.Cancel = true;
                Cancel = true;
                ConsoleAnsiReset();
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Red;
                System.Console.WriteLine("");
                System.Console.WriteLine("Cancelling after iteration... (Press Ctrl+C again to force cancel)");
                return;
            }

            ConsoleAnsiReset();
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Red;
            System.Console.WriteLine("");
            System.Console.WriteLine("FORCE CANCELED");
            Environment.Exit(130);
        }

        private void ExitCancel()
        {
            Console.Out.Flush();
            ConsoleAnsiReset();
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Red;
            System.Console.WriteLine("");
            System.Console.WriteLine("CANCELED");
            Teardown();
            Environment.Exit(130);
        }


        #endregion

        #region ConsoleHelpers

        private void ConsoleClearCurrentLine()
        {
            int currentLine = Console.CursorTop;
            Console.SetCursorPosition(0, currentLine);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLine);
        }

        private void ConsoleSetCursorToBeginningOfLine()
        {
            int currentLine = Console.CursorTop;
            Console.SetCursorPosition(0, currentLine);
        }

        private void ConsoleResetForegroundColor()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private void ConsoleAnsiSetUnderline()
        {
            Console.Write("\x1b[4m");
        }

        private void ConsoleAnsiSetBold()
        {
            Console.Write("\x1b[1m");
        }

        private void ConsoleAnsiReset()
        {
            Console.Write("\x1b[0m");
        }

        #endregion

        #region BenchmarkBuilder

        public Benchmark Iterations(int iterations)
        {
            Config.Iterations = iterations;
            return this;
        }

        public Benchmark WarmupIterations(int iterations)
        {
            Config.WarmupIterations = iterations;
            return this;
        }

        [Obsolete("Use the `Run(Action test)` method instead")]
        public Benchmark Test(Action test)
        {
            Config.Test = test;
            return this;
        }

        public Benchmark BeforeEach(Action beforeEach)
        {
            Config.BeforeEach = beforeEach;
            return this;
        }

        public Benchmark AfterEach(Action afterEach)
        {
            Config.AfterEach = afterEach;
            return this;
        }

        public Benchmark CollectGarbageAfterRun(bool collect = true)
        {
            Config.CollectGarbageAfterRun = collect;
            return this;
        }

        public Benchmark SetProcessPriority(ProcessPriorityClass priority)
        {
            Config.ProcessPriority = priority;
            return this;
        }

        public Benchmark SetThreadPriority(ThreadPriority priority)
        {
            Config.ThreadPriority = priority;
            return this;
        }

        #endregion
    }
}