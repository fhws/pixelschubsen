using System;
using System.IO;

namespace fhws.pixelschubser.jpg.encoder
{
    public class BitReader : IDisposable
    {
        public Stream BaseStream;
        private byte BufferByte;
        private int BitPos = -1;

        public BitReader(byte[] buffer)
        {
            BaseStream = new MemoryStream(buffer);
        }

        public BitReader(Stream stream)
        {
            BaseStream = stream;
        }

        public byte ReadBit()
        {
            if (BitPos == -1)
            {
                BufferByte = (byte)BaseStream.ReadByte();
                BitPos = 7;
            }

            var bit = (byte)((BufferByte >> BitPos) & 0x1);
            BitPos--;

            return bit;
        }

        public byte ReadByte()
        {
            if (BitPos == -1)
            {
                return (byte)BaseStream.ReadByte();
            }

            // Move unread bits to the left most position
            var _byte = (byte)(BufferByte << (7 - BitPos));
            // Get new byte from stream
            BufferByte = (byte)BaseStream.ReadByte();
            // Move BufferByte to the right so it contains the remaining bits
            // to fill up the byte
            return (byte)(_byte | (BufferByte >> (BitPos + 1)));
        }

        public void Close() {
            BaseStream.Close();
        }

        public void Dispose()
        {
            BaseStream.Dispose();
            Close();
        }
    }
}
