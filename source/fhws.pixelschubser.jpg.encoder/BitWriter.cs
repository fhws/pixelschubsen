using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace fhws.pixelschubser.jpg.encoder
{
    public class BitWriter : IDisposable
    {
        public Stream BaseStream;
        private byte[] ByteBuffer;
        private int BufferOffset = 0;
        private byte BufferByte = 0x0;
        private int BitPos = 0;

        public bool _HACK_DEV_MAKE_BETTER_EscapeFFBytes = false;

        public BitWriter(Stream stream, uint bufferSize = 1024)
        {
            ByteBuffer = new byte[bufferSize];
            BaseStream = stream;
        }

        public BitWriter(ref byte[] buffer, uint bufferSize = 1024)
        {
            ByteBuffer = new byte[bufferSize];
            BaseStream = new MemoryStream(buffer);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(byte bits, int offset, int count)
        {
            if (BitPos + count <= 8)
            {
                WriteUnchecked(bits, offset, count);
                if (BitPos >= 8)
                {
                    finishByte();
                }
                return;
            }

            var bitsToFill = 8 - BitPos;
            // Fill bit buffer
            WriteUnchecked(bits, offset, bitsToFill);
            finishByte();
            // Write remaining bits
            WriteUnchecked(bits, offset + bitsToFill, count - bitsToFill);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteFromInt(int bits, int offset, int count)
        {
            var bytes = new byte[] {
                (byte)(bits >> 3*8),
                (byte)(bits >> 2*8),
                (byte)(bits >> 1*8),
                (byte)(bits >> 0*8),
            };

            var limit = offset + count;

            for (var i = 0; offset > 0 || limit > 0; i++, offset -= 8, limit -= 8)
            {
                if (offset > 7) continue;
                var off = Math.Max(0, offset);
                var cnt = Math.Min(8 - off, limit);
                Write(bytes[i], off, cnt);
            }
        }

        public void WriteUShort(ushort value)
        {
            WriteByte((byte)(value >> 8));
            WriteByte((byte)value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(byte[] bits, int offset, int count)
        {
            var byteOffset = offset / 8;

            // Write first partial byte
            var bitOffset = offset % 8;
            if (bitOffset != 0)
            {
                var _byte = bits[byteOffset];
                var bitCount = 8 - bitOffset;
                if (bitOffset + count <= 8)
                {
                    // We only read bits from this current byte
                    bitCount = count;
                }
                Write(_byte, bitOffset, bitCount);
                // Update count and byteOffset
                count -= bitCount;
                byteOffset++;

                if (count <= 0) return;
            }

            // Write all whole bytes
            var fullBytesCount = count / 8;
            if (fullBytesCount > 0)
            {
                WriteBytes(bits, byteOffset, fullBytesCount);
            }

            // Write last partial byte
            var remainingBitsCount = count % 8;
            if (remainingBitsCount != 0)
            {
                byteOffset += fullBytesCount;
                var _byte = bits[byteOffset];
                Write(_byte, 0, remainingBitsCount);
            }
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void WriteUnchecked(byte bits, int offset, int count)
        {
            var mask = (byte)(0b_1111_1111 >> (8 - count));
            bits >>= 8 - count - offset;
            BufferByte <<= count;
            BufferByte |= (byte)(mask & bits);
            BitPos += count;
        }

        /// <summary>
        /// Write a single bit into the stream. The byte may only contain a `1`
        /// or a `0`, every other value is undefined behaviour.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBit(byte bit)
        {
            BufferByte = (byte)(BufferByte << 1 | bit);
            AdvanceBit();
        }

        public void WriteByte(byte data)
        {
            if (BitPos == 0)
            {
                writeBufferd(data);
                return;
            }

            // Move bits in BufferByte to the MSB
            BufferByte = (byte)(BufferByte << (8 - BitPos));
            // Get MSBs from data to fill LSBs in the the BufferByte
            byte tmp = (byte)(data >> BitPos);
            BufferByte |= tmp;
            writeBufferd(BufferByte);

            // Isolate the remaining MSBs by first shifting the LSBs outof the
            // byte and then shifting the LSBs back to the beginning
            BufferByte = (byte)(data << (8 - BitPos));
            BufferByte = (byte)(BufferByte >> (8 - BitPos));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBytes(byte[] buffer, int offset, int count)
        {
            if (BitPos == 0)
            {
                WriteBytesUnchecked(buffer, offset, count);
                return;
            }

            var limit = offset + count;
            for (; offset < limit; offset++)
            {
                WriteByte(buffer[offset]);
            }
        }

        public void WriteBytes(byte[] buffer)
        {
            this.WriteBytes(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// Write bytes directly to the buffer or the underlying stream if buffer
        /// would be too short.
        /// <par/>
        /// ATTENTION:
        /// This method should only be called if there are no bits in the
        /// `BufferByte`. Otherwise the partially written byte will be appended
        /// later when it is filled completely.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void WriteBytesUnchecked(byte[] buffer, int offset, int count)
        {
            var bytesToWrite = count - offset;
            var freeBytesInBuffer = ByteBuffer.Length - BufferOffset;
            if (bytesToWrite >= freeBytesInBuffer)
            {
                if (BufferOffset > 0) writeBufferToStream();
                BaseStream.Write(buffer, offset, count);
                return;
            }
            Buffer.BlockCopy(buffer, offset, ByteBuffer, BufferOffset, count);
            BufferOffset += count;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBits(byte[] buffer)
        {
            WriteBits(buffer, 0, buffer.Length);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBits(byte[] buffer, int offset, int count)
        {
            var limit = offset + count;
            for (; offset < limit; offset++)
            {
                WriteBit(buffer[offset]);
            }
        }

        /// <summary>
        /// Fill up the remaining bits of the current byte with zeroes and write
        /// the byte to the stream.
        /// </summary>
        public void FillRemainingBits()
        {
            if (BitPos == 0) return;

            fillBufferWithNull();
            finishByte();
        }

        /// <summary>
        /// Fill up the remaining bits of the current byte with zeroes and write
        /// the byte to the stream.
        /// </summary>
        public void FillRemainingBitsWithOnes()
        {
            if (BitPos == 0) return;

            fillBufferWithNull();
            BufferByte |= (byte)(0xff >> BitPos);
            finishByte();
        }

        public void Close()
        {
            Dispose();
        }

        public void Dispose()
        {
            try
            {
                if (BaseStream != null)
                {
                    FillRemainingBits();
                    Flush();
                }
            }
            finally
            {
                if (BaseStream != null)
                {
                    try
                    {
                        BaseStream.Close();
                    }
                    finally
                    {
                        BaseStream = null;
                        ByteBuffer = null;
                    }
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Flush()
        {
            // TODO: What to do if the BufferByte is partially filled?
            writeBufferToStream();
            BaseStream.Flush();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void writeBufferToStream()
        {
            BaseStream.Write(ByteBuffer, 0, BufferOffset);
            BufferOffset = 0;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void AdvanceBit()
        {
            BitPos++;

            if (BitPos >= 8)
            {
                finishByte();
            }

        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void finishByte()
        {
            writeBufferd(BufferByte);
            BitPos = 0;
            BufferByte = 0x0;
        }

        private void fillBufferWithNull()
        {
            BufferByte = (byte)(BufferByte << (8 - BitPos));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void writeBufferd(byte data)
        {
            ByteBuffer[BufferOffset++] = data;

            if (BufferOffset >= ByteBuffer.Length)
            {
                writeBufferToStream();
            }

            if (_HACK_DEV_MAKE_BETTER_EscapeFFBytes && data == 0xff)
            {
                writeBufferd(0x00);
            }
        }
    }
}