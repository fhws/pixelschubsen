﻿using fhws.pixelschubser.jpg.encoder.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace fhws.pixelschubser.jpg.encoder
{
    public class DataEncoding
    {
        public struct EncodedDc
        {
            public List<(byte category, int bits)> Y;
            public List<(byte category, int bits)> Cb;
            public List<(byte category, int bits)> Cr;
        }

        public static EncodedDc EncodeDC((List<int>[,] Y, List<int>[,] Cb, List<int>[,] Cr) quantizedCoefficients)
        {
            var dcLists = (
                Y: CollectDcCoefficients((2, 2), quantizedCoefficients.Y),
                Cb: CollectDcCoefficients(quantizedCoefficients.Cb),
                Cr: CollectDcCoefficients(quantizedCoefficients.Cr)
            );

            var predictedDcLists = (
                Y: SimplePrediction(dcLists.Y),
                Cb: SimplePrediction(dcLists.Cb),
                Cr: SimplePrediction(dcLists.Cr)
            );

            var categoryEncodedDcLists = new EncodedDc
            {
                Y = predictedDcLists.Y.Select(x => EncodeCategoryBitRepresentation(x)).ToList(),
                Cb = predictedDcLists.Cb.Select(x => EncodeCategoryBitRepresentation(x)).ToList(),
                Cr = predictedDcLists.Cr.Select(x => EncodeCategoryBitRepresentation(x)).ToList(),
            };

            return categoryEncodedDcLists;
        }

        private static List<T> CollectDcCoefficients<T>(List<T>[,] coefficients)
        {
            return inlinedCollectDcCoefficients<T>((1, 1), coefficients);
        }

        private static List<T> CollectDcCoefficients<T>((int X, int Y) chunkSize, List<T>[,] coefficients)
        {
            return inlinedCollectDcCoefficients<T>(chunkSize, coefficients);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static List<T> inlinedCollectDcCoefficients<T>((int X, int Y) chunkSize, List<T>[,] coefficients)
        {
            var sizeX = coefficients.GetLength(1);
            var sizeY = coefficients.GetLength(0);
            var dcList = new List<T>(sizeX * sizeY);

            coefficients.ForAll(chunkSize, x => dcList.Add(x[0]));

            return dcList;
        }


        public struct EncodedAc
        {
            public List<(byte combined, byte zeroes, byte category, int bits)>[,] Y;
            public List<(byte combined, byte zeroes, byte category, int bits)>[,] Cb;
            public List<(byte combined, byte zeroes, byte category, int bits)>[,] Cr;
        }

        public static EncodedAc EncodeAC((List<int>[,] Y, List<int>[,] Cb, List<int>[,] Cr) quantizedDctCoefficients)
        {
            var acLists = (
                Y: quantizedDctCoefficients.Y.ConvertAll(x => x.GetRange(1, x.Count - 1)),
                Cb: quantizedDctCoefficients.Cb.ConvertAll(x => x.GetRange(1, x.Count - 1)),
                Cr: quantizedDctCoefficients.Cr.ConvertAll(x => x.GetRange(1, x.Count - 1))
            );
            var runLengthEncodedAcLists = (
                Y: acLists.Y.ConvertAll((2, 2), x => EncodeRunLength(x)),
                Cb: acLists.Cb.ConvertAll(x => EncodeRunLength(x)),
                Cr: acLists.Cr.ConvertAll(x => EncodeRunLength(x))
            );
            var categoryEncodedAcLists = (
                Y: runLengthEncodedAcLists.Y.ConvertAll(list => ConvertAcNumberToCategoryEncoded(list)),
                Cb: runLengthEncodedAcLists.Cb.ConvertAll(list => ConvertAcNumberToCategoryEncoded(list)),
                Cr: runLengthEncodedAcLists.Cr.ConvertAll(list => ConvertAcNumberToCategoryEncoded(list))
            );
            var combinedEncodedAcLists = new EncodedAc
            {
                Y = categoryEncodedAcLists.Y.ConvertAll(list => list.Select(
                    x => CombineEncodedAc(x)).ToList()),
                Cb = categoryEncodedAcLists.Cb.ConvertAll(list => list.Select(
                    x => CombineEncodedAc(x)).ToList()),
                Cr = categoryEncodedAcLists.Cr.ConvertAll(list => list.Select(
                    x => CombineEncodedAc(x)).ToList()),
            };

            return combinedEncodedAcLists;
        }

        static (byte combined, byte zeroes, byte category, int bits) CombineEncodedAc((byte zeroes, byte category, int bits) x)
        {
            return (combined: (byte)((x.zeroes << 4) | x.category), zeroes: x.zeroes, category: x.category, bits: x.bits);
        }

        static List<(byte zeroes, int number)> EncodeRunLength(List<int> inp)
        {
            byte zeroes = 0;
            var outp = new List<(byte zeroes, int number)>();
            var tail = new List<(byte zeroes, int number)>();
            foreach (var number in inp)
            {
                if (number != 0)
                {
                    outp.AddRange(tail);
                    tail.Clear();
                    outp.Add((zeroes, number));
                    zeroes = 0;
                    continue;
                }

                if (zeroes < 15)
                {
                    zeroes++;
                    continue;
                }

                tail.Add((zeroes, number));
                zeroes = 0;
            }

            if (zeroes > 0 || tail.Count > 0)
            {
                // Add EOB marker
                outp.Add((0, 0));
            }

            return outp;
        }

        private static List<(byte zeroes, byte category, int bits)> ConvertAcNumberToCategoryEncoded(List<(byte zeroes, int number)> list)
        {
            return list.Select(x =>
            {
                var enc = EncodeCategoryBitRepresentation(x.number);
                return (zeroes: x.zeroes, category: enc.category, bits: enc.bits);
            }).ToList();
        }

        /// <summary>
        /// Takes numbers in the interval `[-32767; 32767]` and converts it to a
        /// Category-Bit-Representation.
        /// <par>
        /// The category determines the length `l` of the bit representation. To
        /// get the bits, just read the lowest `l` bits from the `bits` field.
        /// <par>
        /// The `bits` in the returned tuple 
        /// </summary>
        static (byte category, int bits) EncodeCategoryBitRepresentation(int num)
        {
            var negative = num < 0;
            var abs = Math.Abs(num);

            // The category is also the minimal number of bits needed to represent the absolute
            // value of the number.
            // Find the category by shifting the number until it has no *ones*
            // left and is therefore zero.
            byte category = 0;
            for (var tmp = abs; tmp != 0; tmp >>= 1)
            {
                category++;
            }

            var bits = abs;
            if (negative)
            {
                bits = ~bits; // Create complement
                var mask = unchecked((uint)(~0x0)); // Fill with ones
                mask >>= (sizeof(int) * 8 - category); // Shift right
                bits = bits & (int)mask;
            }

            return (category, bits);
        }

        static List<int> SimplePrediction(List<int> inp)
        {
            var len = inp.Count;
            var outp = new List<int>(len);

            var prev = 0;
            foreach (var el in inp)
            {
                outp.Add(el - prev);
                prev = el;
            }

            return outp;
        }
    }
}
