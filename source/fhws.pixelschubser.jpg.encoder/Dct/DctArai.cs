using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace fhws.pixelschubser.jpg.encoder.Dct
{
    public class DctArai : IDct
    {
        private const int ChunkSize = 8;

        static double C1 = Math.Cos(1.0 * Math.PI / 16.0);
        static double C2 = Math.Cos(2.0 * Math.PI / 16.0);
        static double C3 = Math.Cos(3.0 * Math.PI / 16.0);
        static double C4 = Math.Cos(4.0 * Math.PI / 16.0);
        static double C5 = Math.Cos(5.0 * Math.PI / 16.0);
        static double C6 = Math.Cos(6.0 * Math.PI / 16.0);
        static double C7 = Math.Cos(7.0 * Math.PI / 16.0);

        static double A0 = C4;
        static double A1 = C2 - C6;
        static double A2 = C4;
        static double A3 = C6 + C2;
        static double A4 = C6;

        static double S0 = 1.0 / (2.0 * Math.Sqrt(2));
        static double S1 = 1.0 / (4.0 * C1);
        static double S2 = 1.0 / (4.0 * C2);
        static double S3 = 1.0 / (4.0 * C3);
        static double S4 = 1.0 / (4.0 * C4);
        static double S5 = 1.0 / (4.0 * C5);
        static double S6 = 1.0 / (4.0 * C6);
        static double S7 = 1.0 / (4.0 * C7);

        // TODO: Refactor: Extract
        private static IEnumerable<int> SteppedIterator(int startIndex, int endIndex, int stepSize)
        {
            for (int i = startIndex; i < endIndex; i = i + stepSize)
            {
                yield return i;
            }
        }

        public double[,] Transform(double[,] input)
        {
            var data = (double[,])input.Clone();
            TransformInplace(data);
            return data;
        }

        public void TransformInplace(double[,] data)
        {
            var sizeX = data.GetLength(1);
            var sizeY = data.GetLength(0);

            var splitSizeX = 8 * 120 * 2; // TODO: remove `*2` to make 4k run on 8 cores
            var splitSizeY = 8 * 135;

            // Go through each 8x8 chunk of the input matrix
            Parallel.ForEach(SteppedIterator(0, sizeX, splitSizeX), (xOffset) =>
            {
                Parallel.ForEach(SteppedIterator(0, sizeY, splitSizeY), (yOffset) =>
                {
                    TransformSplit(ref data, xOffset, yOffset, splitSizeX, splitSizeY);
                });
            });
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void TransformSplit(ref double[,] data, int offsetX, int offsetY, int limitX, int limitY)
        {
            limitX = Math.Min(limitX + offsetX, data.GetLength(1));
            limitY = Math.Min(limitY + offsetY, data.GetLength(0));
            for (var xOffset = offsetX; xOffset < limitX; xOffset += 8)
            {
                for (var yOffset = offsetY; yOffset < limitY; yOffset += 8)
                {
                    TransformChunk(ref data, xOffset, yOffset);
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void TransformChunk(ref double[,] data, int offsetX, int offsetY)
        {
            for (var y = offsetY; y < offsetY + ChunkSize; y++)
            {
                TransformChunkRow(ref data, offsetX, y);
            }

            for (var x = offsetX; x < offsetX + ChunkSize; x++)
            {
                TransformChunkColumn(ref data, x, offsetY);
            }

        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void TransformChunkRow(ref double[,] data, int offsetX, int offsetY)
        {
            Arai1D(
                ref data[offsetY, 0 + offsetX],
                ref data[offsetY, 1 + offsetX],
                ref data[offsetY, 2 + offsetX],
                ref data[offsetY, 3 + offsetX],
                ref data[offsetY, 4 + offsetX],
                ref data[offsetY, 5 + offsetX],
                ref data[offsetY, 6 + offsetX],
                ref data[offsetY, 7 + offsetX]
            );
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void TransformChunkColumn(ref double[,] data, int offsetX, int offsetY)
        {
            Arai1D(
                ref data[0 + offsetY, offsetX],
                ref data[1 + offsetY, offsetX],
                ref data[2 + offsetY, offsetX],
                ref data[3 + offsetY, offsetX],
                ref data[4 + offsetY, offsetX],
                ref data[5 + offsetY, offsetX],
                ref data[6 + offsetY, offsetX],
                ref data[7 + offsetY, offsetX]
            );
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Arai1D(
            ref double data0,
            ref double data1,
            ref double data2,
            ref double data3,
            ref double data4,
            ref double data5,
            ref double data6,
            ref double data7
        )
        {
            var a0 = data0 + data7;
            var a1 = data1 + data6;
            var a2 = data2 + data5;
            var a3 = data3 + data4;
            var a4 = data3 - data4;
            var a5 = data2 - data5;
            var a6 = data1 - data6;
            var a7 = data0 - data7;

            var b0 = a0 + a3;
            var b1 = a1 + a2;
            var b2 = a1 - a2;
            var b3 = a0 - a3;
            var b4 = -a4 - a5;
            var b5 = a5 + a6;
            var b6 = a6 + a7;

            a0 = b0 + b1;
            a1 = b0 - b1;
            a2 = b2 + b3;

            var t = (b4 + b6) * A4;

            b2 = a2 * A0;
            b4 = -t - (b4 * A1);
            b5 = b5 * A2;
            b6 = -t + (b6 * A3);

            a2 = b2 + b3;
            a3 = b3 - b2;
            a5 = b5 + a7;
            a7 = a7 - b5;

            a4 = a7 + b4;
            b5 = a5 + b6;
            b6 = a5 - b6;
            var b7 = a7 - b4;

            // Output
            data0 = a0 * S0;
            data1 = b5 * S1;
            data2 = a2 * S2;
            data3 = b7 * S3;
            data4 = a1 * S4;
            data5 = a4 * S5;
            data6 = a3 * S6;
            data7 = b6 * S7;
        }
    }
}
