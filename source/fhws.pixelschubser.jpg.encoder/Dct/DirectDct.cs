using System;
using System.Linq;

namespace fhws.pixelschubser.jpg.encoder.Dct
{
    public class DirectDct : IDct
    {
        private const int ChunkSize = 8;

        public double[,] Transform(double[,] input)
        {
            var sizeX = input.GetLength(1);
            var sizeY = input.GetLength(0);

            var output = new double[sizeY, sizeX];

            // Go ghrough each 8x8 chunk of the input matrix
            for (var xOffset = 0; xOffset < sizeX; xOffset += 8)
            {
                for (var yOffset = 0; yOffset < sizeY; yOffset += 8)
                {
                    TransformChunk(input, xOffset, yOffset, ref output);
                }
            }

            return output;
        }

        private void TransformChunk(double[,] input, int offsetX, int offsetY, ref double[,] output)
        {
            var N = ChunkSize;
            var C = 1.0 / Math.Sqrt(2);

            // Initialize both values with C for their first loop round, then at
            // the end of the first round, set the corresponding value to 1.
            var Ci = C;
            var Cj = C;

            var iOffset = offsetX;
            for (var i = 0; i < N; i++, iOffset++)
            {
                // As `j` gets reset to `0`, `Cj` has to be reset to `C`
                Cj = C;

                var jOffset = offsetY;
                for (var j = 0; j < N; j++, jOffset++)
                {
                    double sum = CalcCosSum(input, offsetX, offsetY, N, i, j);
                    output[jOffset, iOffset] = 2.0 / N * Ci * Cj * sum;

                    // `Cj` should only be `C` for the first round
                    // (while `j == 0`). After that it should always be `1`.
                    Cj = 1;
                }

                // `Ci` should only be `C` for the first round
                // (while `i == 0`). After that it should always be `1`.
                Ci = 1;
            }
        }

        private static double CalcCosSum(double[,] input, int offsetX, int offsetY, int N, int i, int j)
        {
            var sum = 0.0;
            var xOffset = offsetX;
            for (var x = 0; x < N; x++, xOffset++)
            {
                var yOffset = offsetY;
                for (var y = 0; y < N; y++, yOffset++)
                {
                    sum += input[yOffset, xOffset]
                        * Math.Cos(((2.0 * x + 1.0) * i * Math.PI) / (2.0 * N))
                        * Math.Cos(((2.0 * y + 1.0) * j * Math.PI) / (2.0 * N));
                }
            }

            return sum;
        }
    }
}
