using System;

namespace fhws.pixelschubser.jpg.encoder.Dct
{
    public interface IDct
    {
        /// <symmary>
        /// Accepts a "line major" 2D array and runs a "Discrete Cosine
        /// Transformation" on every 8x8 block of the input.
        /// </summary>
        double[,] Transform(double[,] input);
    }
}
