﻿using fhws.pixelschubser.jpg.encoder.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using static fhws.pixelschubser.jpg.encoder.ZickZackSampler;

namespace fhws.pixelschubser.jpg.encoder.Dct
{
    public class Quantization
    {
        internal static byte[,] noneYQuantTable = new byte[8, 8]
        {
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 }
        };
        internal static byte[,] noneCbCrQuantTable = new byte[8, 8]
        {
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 },
                {1, 1, 1, 1, 1, 1, 1, 1 }
        };

        internal static byte[,] lowYQuantTable = new byte[8, 8]
        {
            { 1,  1,  1,  1,  2,  3,  4,  5 },
            { 1,  1,  1,  2,  2,  5,  5,  4 },
            { 1,  1,  1,  2,  3,  5,  6,  4 },
            { 1,  1,  2,  2,  4,  7,  6,  5 },
            { 1,  2,  3,  4,  5,  9,  8,  6 },
            { 2,  3,  4,  5,  6,  8,  9,  7 },
            { 4,  5,  6,  7,  8, 10, 10,  8 },
            { 6,  7,  8,  8,  9,  8,  8,  8 },
            };
        internal static byte[,] lowCbCrQuantTable = new byte[8, 8]
        {
            { 1,  1,  2,  4,  8,  8,  8,  8 },
            { 1,  2,  2,  5,  8,  8,  8,  8 },
            { 2,  2,  4,  8,  8,  8,  8,  8 },
            { 4,  5,  8,  8,  8,  8,  8,  8 },
            { 8,  8,  8,  8,  8,  8,  8,  8 },
            { 8,  8,  8,  8,  8,  8,  8,  8 },
            { 8,  8,  8,  8,  8,  8,  8,  8 },
            { 8,  8,  8,  8,  8,  8,  8,  8 },
        };

        internal static byte[,] mediumYQuantTable = new byte[8, 8]
        {
            {6,4,4,6,10,16,20,24},
            {5,5,6,8,10,23,24,22 },
            {6,5,6,10,16,23,28,22 },
            {6,7,9,12,20,35,32,25 },
            {7,9,15,22,27,44,41,31 },
            {10,14,22,26,32,42,45,37 },
            {20,26,31,35,41,48,48,40 },
            {29,37,38,39,45,40,41,40}
        };
        internal static byte[,] mediumCbCrQuantTable = new byte[8, 8]
        {
            {7,7,10,19,40,40,40,40},
            {7,8,10,26,40,40,40,40,},
            {10,10,22,40,40,40,40,40},
            {19,26,40,40,40,40,40,40},
            {40,40,40,40,40,40,40,40},
            {40,40,40,40,40,40,40,40},
            {40,40,40,40,40,40,40,40},
            {40,40,40,40,40,40,40,40}
        };

        internal static byte[,] highYQuantTable = new byte[8, 8]
        {
            {16, 11, 10, 16,  24,  40,  51,  61 },
            {12, 12, 14, 19,  26,  58,  60,  55 },
            {14, 13, 16, 24,  40,  57,  69,  56 },
            {14, 17, 22, 29,  51,  87,  80,  62 },
            {18, 22, 37, 56,  68,  109, 103, 77 },
            {24, 35, 55, 64,  81,  104, 113, 92 },
            {49, 64, 78, 87,  103, 121, 120, 101},
            {72, 92, 95, 98,  112, 100, 103, 99 }
        };
        internal static byte[,] highCbCrQuantTable = new byte[8, 8]
        {
            {17, 18, 24, 47,  99, 99, 99, 99},
            {18, 21, 26, 66,  99, 99, 99, 99},
            {24, 26, 56, 99,  99, 99, 99, 99},
            {47, 66, 99, 99,  99, 99, 99, 99},
            {99, 99, 99, 99,  99, 99, 99, 99},
            {99, 99, 99, 99,  99, 99, 99, 99},
            {99, 99, 99, 99,  99, 99, 99, 99},
            {99, 99, 99, 99,  99, 99, 99, 99}
        };

        public static (List<int>[,] Y, List<int>[,] Cb, List<int>[,] Cr) Quantize((List<double>[,] Y, List<double>[,] Cb, List<double>[,] Cr) dctCoefficents, CompressionLevel compressionLevel)
        {
            var zickZackQuantTables = GetQuantTableZickZackSorted(compressionLevel);
            var yQuantTableList = zickZackQuantTables.yQuantTable;
            var cbCrQuantTableList = zickZackQuantTables.CbCrQuantTable;

            return (
                    Y: dctCoefficents.Y.ConvertAll(list => list.Zip(yQuantTableList, (x, y) => (int)Math.Round((double)x / y)).ToList()),
                    Cb: dctCoefficents.Cb.ConvertAll(list => list.Zip(cbCrQuantTableList, (x, y) => (int)Math.Round((double)x / y)).ToList()),
                    Cr: dctCoefficents.Cr.ConvertAll(list => list.Zip(cbCrQuantTableList, (x, y) => (int)Math.Round((double)x / y)).ToList())
                );
        }


        public static (List<byte> yQuantTable, List<byte> CbCrQuantTable) GetQuantTableZickZackSorted(CompressionLevel level)
        {
            switch (level)
            {
                case CompressionLevel.None:
                    return (ZickZackSampleChunk(noneYQuantTable, 0, 0), ZickZackSampleChunk(noneCbCrQuantTable, 0, 0));
                case CompressionLevel.Low:
                    return (ZickZackSampleChunk(lowYQuantTable, 0, 0), ZickZackSampleChunk(lowCbCrQuantTable, 0, 0));
                case CompressionLevel.Medium:
                    return (ZickZackSampleChunk(mediumYQuantTable, 0, 0), ZickZackSampleChunk(mediumCbCrQuantTable, 0, 0));
                case CompressionLevel.High:
                    return (ZickZackSampleChunk(highYQuantTable, 0, 0), ZickZackSampleChunk(highCbCrQuantTable, 0, 0));
            }

            throw new ArgumentException("invalid compression level");
        }
    }

    public enum CompressionLevel
    {
        None,
        Low,
        Medium,
        High
    }
}
