using System;
using System.Linq;

namespace fhws.pixelschubser.jpg.encoder.Dct
{
    public class SeparatedDct : IDct
    {
        private const int ChunkSize = 8;

        private double[,] TransformMat;
        private double[,] TransformMatTransposed;

        public SeparatedDct()
        {
            TransformMat = GenerateTransformMatrix();
            TransformMatTransposed = MatTranspose(TransformMat);
        }

        private double[,] GenerateTransformMatrix()
        {
            var N = ChunkSize;
            var transformMat = new double[N, N];

            // Initialize with value and set to `1` once `k` is not `0` anymore
            var C0 = 1.0 / Math.Sqrt(2);

            for (var k = 0; k < N; k++)
            {
                for (var n = 0; n < N; n++)
                {
                    transformMat[n, k] = C0 * Math.Sqrt(2.0 / N) * Math.Cos((2 * n + 1) * ((k * Math.PI) / (2.0 * N)));
                }

                // `k` is becoming greater than `0` so set `C0` to `1`
                C0 = 1;
            }

            return transformMat;
        }

        public double[,] Transform(double[,] input)
        {
            var sizeX = input.GetLength(1);
            var sizeY = input.GetLength(0);

            var output = new double[sizeY, sizeX];

            // Go ghrough each 8x8 chunk of the input matrix
            for (var xOffset = 0; xOffset < sizeX; xOffset += 8)
            {
                for (var yOffset = 0; yOffset < sizeY; yOffset += 8)
                {
                    TransformChunk(input, xOffset, yOffset, ref output);
                }
            }

            return output;
        }

        private void TransformChunk(double[,] input, int offsetX, int offsetY, ref double[,] output)
        {
            var N = ChunkSize;
            var A = TransformMat;
            var At = TransformMatTransposed;

            var X = new double[N, N];
            BlockCopy2D<double>(input, (offsetX, offsetY), ref X, (0, 0), (N, N));

            var AX = MatMultiply(A, X);
            var AXAt = MatMultiply(AX, At);

            BlockCopy2D<double>(AXAt, (0, 0), ref output, (offsetX, offsetY), (N, N));
        }

        private double[,] MatTranspose(double[,] mat)
        {
            var lenX = mat.GetLength(0);
            var lenY = mat.GetLength(1);
            var transMat = new double[lenX, lenY];

            for (var x = 0; x < lenX; x++)
            {
                for (var y = 0; y < lenY; y++)
                {
                    transMat[y, x] = mat[x, y];
                }
            }
            return transMat;
        }

        private double[,] MatMultiply(double[,] matA, double[,] matB)
        {
            var heightA = matA.GetLength(0);
            var widthA = matA.GetLength(1);
            var heightB = matB.GetLength(0);
            var widthB = matB.GetLength(1);

            var height = heightA; // Height of left matrix
            var width = widthB; // Width of right matrix
            var mat = new double[height, width];

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    var sum = 0.0;
                    for (var i = 0; i < widthA; i++)
                    {
                        sum += matA[i, x] * matB[y, i];
                    }
                    mat[y, x] = sum;
                }
            }

            return mat;
        }

        private static void BlockCopy2D<T>(
                T[,] src, (int X, int Y) srcOffset,
                ref T[,] dst, (int X, int Y) dstOffset,
                (int X, int Y) count)
        {
            var xOffsetSrc = srcOffset.X;
            var xOffsetDst = dstOffset.X;
            for (var x = 0; x < count.X; x++, xOffsetSrc++, xOffsetDst++)
            {
                var yOffsetSrc = srcOffset.Y;
                var yOffsetDst = dstOffset.Y;
                for (var y = 0; y < count.Y; y++, yOffsetSrc++, yOffsetDst++)
                {
                    dst[yOffsetDst, xOffsetDst] = src[yOffsetSrc, xOffsetSrc];
                }
            }
        }
    }
}
