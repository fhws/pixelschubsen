﻿using fhws.pixelschubser.jpg.encoder.Models;
using System;
using System.Runtime.CompilerServices;

namespace fhws.pixelschubser.jpg.encoder.Extensions
{
    public static class ArrayExtensions
    {
        public static TOutput[,] ConvertAll<TInput, TOutput>(this TInput[,] data, Converter<TInput, TOutput> converter)
        {
            return inlinedConvertAll<TInput, TOutput>(data, (X: 1, Y: 1), converter);
        }

        public static TOutput[,] ConvertAll<TInput, TOutput>(this TInput[,] data, (int X, int Y) chunkSize, Converter<TInput, TOutput> converter)
        {
            return inlinedConvertAll<TInput, TOutput>(data, chunkSize, converter);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TOutput[,] inlinedConvertAll<TInput, TOutput>(this TInput[,] data, (int X, int Y) chunkSize, Converter<TInput, TOutput> converter)
        {
            var sizeX = data.GetLength(1);
            var sizeY = data.GetLength(0);
            var outp = new TOutput[sizeY, sizeX];
            var limX = sizeX / chunkSize.X;
            var limY = sizeY / chunkSize.Y;
            for (var y = 0; y < limY; y++)
            {
                for (var x = 0; x < limX; x++)
                {
                    var offsetX = x * chunkSize.X;
                    var offsetY = y * chunkSize.Y;

                    for (var k = 0; k < chunkSize.Y; k++)
                    {
                        for (var j = 0; j < chunkSize.X; j++)
                        {
                            outp[offsetY + k, offsetX + j] = converter(data[offsetY + k, offsetX + j]);
                        }
                    }
                }
            }

            return outp;
        }

        public static (T[,] Y, T[,] Cb, T[,] Cr) SeparateColorChannels<T>(this YCBCRPixel<T>[,] data)
        {
            var sizeX = data.GetLength(1);
            var sizeY = data.GetLength(0);
            var Y = new T[sizeY, sizeX];
            var Cb = new T[sizeY, sizeX];
            var Cr = new T[sizeY, sizeX];

            for (var y = 0; y < sizeY; y++)
            {
                for (var x = 0; x < sizeX; x++)
                {
                    Y[y, x] = data[y, x].Y;
                    Cb[y, x] = data[y, x].CB;
                    Cr[y, x] = data[y, x].CR;
                }
            }

            return (Y, Cb, Cr);
        }

        public static T[,] SubSample<T>(this T[,] inp, int rateX, int rateY)
        {
            var sizeX = inp.GetLength(1);
            var sizeY = inp.GetLength(0);

            var reducedSizeX = sizeX / rateX;
            var reducedSizeY = sizeY / rateY;
            var reduced = new T[reducedSizeY, reducedSizeX];
            var posX = 0;
            var posY = 0;

            for (var y = 0; y < sizeY; y += rateY, posY++)
            {
                posX = 0;
                for (var x = 0; x < sizeX; x += rateX, posX++)
                {
                    reduced[posY, posX] = inp[y, x];
                }
            }

            return reduced;
        }

        public static void ForAll<T>(this T[,] data, Action<T> action)
        {
            inlinedForAll(data, (1, 1), (val, _) => action(val));
        }

        public static void ForAll<T>(this T[,] data, Action<T, (int X, int Y)> action)
        {
            inlinedForAll<T>(data, (1, 1), action);
        }

        public static void ForAll<T>(this T[,] data, (int X, int Y) chunkSize, Action<T> action)
        {
            inlinedForAll(data, chunkSize, (val, _) => action(val));
        }

        public static void ForAll<T>(this T[,] data, (int X, int Y) chunkSize, Action<T, (int X, int Y)> action)
        {
            inlinedForAll<T>(data, chunkSize, action);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void inlinedForAll<T>(this T[,] data, (int X, int Y) chunkSize, Action<T, (int X, int Y)> action)
        {
            var sizeX = data.GetLength(1);
            var sizeY = data.GetLength(0);
            var limX = sizeX / chunkSize.X;
            var limY = sizeY / chunkSize.Y;
            for (var y = 0; y < limY; y++)
            {
                for (var x = 0; x < limX; x++)
                {
                    var offsetX = x * chunkSize.X;
                    var offsetY = y * chunkSize.Y;

                    for (var k = 0; k < chunkSize.Y; k++)
                    {
                        for (var j = 0; j < chunkSize.X; j++)
                        {
                            action(data[offsetY + k, offsetX + j], (offsetX + j, offsetY + k));
                        }
                    }
                }
            }
        }
    }
}
