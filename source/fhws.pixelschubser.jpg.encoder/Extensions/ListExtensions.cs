using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace fhws.pixelschubser.jpg.encoder.Extensions
{
    static class ListExtensions
    {
        /// <summary>
        /// Add if item is not Null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="value"></param>
        public static void AddIfNotNull<T>(this List<T> list, T value)
        {
            if (value != null) list.Add(value);
        }

        /// <summary>
        /// Reshape a list into a 2D array. If the length of the input is not
        /// integer dividable by `width` then the last values will be dropped.
        /// <summary>
        public static T[,] Reshape<T>(this List<T> inp, int width)
        {
            return inlinedReshape<T>(inp, (1, 1), width);
        }

        /// <summary>
        /// Reshape a list into a 2D array. If the length of the input is not
        /// integer dividable by `width` then the last values will be dropped.
        /// <summary>
        public static T[,] Reshape<T>(this List<T> inp, (int X, int Y) chunkSize, int width)
        {
            return inlinedReshape<T>(inp, chunkSize, width);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static T[,] inlinedReshape<T>(this List<T> inp, (int X, int Y) chunkSize, int width)
        {
            var len = inp.Count;
            var height = len / width;
            var arr = new T[height, width];
            height /= chunkSize.Y;
            width /= chunkSize.X;
            var idx = 0;
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var offsetX = x * chunkSize.X;
                    var offsetY = y * chunkSize.Y;

                    for (var k = 0; k < chunkSize.Y; k++)
                    {
                        for (var j = 0; j < chunkSize.X; j++)
                        {
                            arr[offsetY + k, offsetX + j] = inp[idx++];
                        }
                    }
                }
            }

            return arr;
        }
    }

}
