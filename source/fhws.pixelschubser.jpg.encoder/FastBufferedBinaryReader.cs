﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace fhws.pixelschubser.jpg.encoder
{
    class FastBufferedBinaryReader : IDisposable
    {
        private readonly Stream stream;
        private readonly byte[] buffer;
        private readonly int bufferSize;
        private int bufferOffset;
        private int numBufferedBytes;
		private readonly StringBuilder lineReaderBuilder = new StringBuilder(200);

		public FastBufferedBinaryReader(Stream stream, int bufferSize)
        {
            this.stream = stream;
            this.bufferSize = bufferSize;
            buffer = new byte[bufferSize];
            bufferOffset = bufferSize;
        }

		public void Dispose()
		{
			Close();
		}

		public int NumBytesAvailable 
		{ 
			get { return Math.Max(0, numBufferedBytes - bufferOffset); } 
		}

		/// <summary>
		/// Attempt to fill the buffer. Buffer might not be completely 
		/// filled after that attempt. Check with NumBytesAvailable().
		/// <br></br><br></br>
		/// <b>Returns</b> False if the buffer is empty after the attempt 
		/// to fill it
		/// </summary>
		public bool FillBuffer()
		{
			var numBytesUnread = NumBytesAvailable;
			var numBytesToRead = bufferSize - numBytesUnread;
			
			numBufferedBytes = numBytesUnread;
			if (numBytesUnread > 0) // Buffer still has bytes
			{
				// Move the remaining bytes to the beginning
				Buffer.BlockCopy(buffer, bufferOffset, buffer, 0, numBytesUnread);
			}
			bufferOffset = 0;
			while (numBytesToRead > 0)
			{
				var numBytesRead = stream.Read(buffer, numBytesUnread, numBytesToRead);
				if (numBytesRead == 0) return NumBytesAvailable != 0;
				numBufferedBytes += numBytesRead;
				numBytesToRead -= numBytesRead;
				numBytesUnread += numBytesRead;
			}
			return true;
		}

		/// <summary>
		/// Not fast - Don't use it to much
		/// </summary>
		/// <returns></returns>
		internal string ReadLine()
		{
			FillBuffer();
			lineReaderBuilder.Clear();
			char c;
			while ((c = (char)buffer[bufferOffset]) != '\n')
			{
				if (c == '\r') { ++bufferOffset; continue; }; // multiplatform comp :see_no_evil:
				lineReaderBuilder.Append(c);
				bufferOffset += 1; // 1 ASCII char == 1 Byte
				if (bufferSize - bufferOffset == 0) FillBuffer();
			}

			bufferOffset += 1; // because of the \n 
			return lineReaderBuilder.ToString();
		}

		public ushort ReadUInt16()
		{
			var val = (ushort)((int)buffer[bufferOffset] | (int)buffer[bufferOffset + 1] << 8);
			bufferOffset += 2;
			return val;
		}

		public char ReadChar()
		{
			var tmpC = (char)buffer[bufferOffset];
			bufferOffset += 1;
			return tmpC;
		}

		internal void Close()
		{
			stream?.Close();
		}
	}
}
