using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using fhws.pixelschubser.jpg.encoder.Extensions;
using fhws.pixelschubser.jpg.encoder.HuffmanEncoder.HuffmanTree;
using Shields.GraphViz.Components;
using Shields.GraphViz.Models;
using Shields.GraphViz.Services;

namespace fhws.pixelschubser.jpg.encoder
{
    static class GraphvizHelper
    {
        /// <summary>
        /// Saves the graph to a temp file and then shows it with the default
        /// png viewer.
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="pathToGraphvizBin">
        /// e.g.: "C:\Program Files (x86)\Graphviz2.38\bin" 
        /// if nothing is supplied it searches for 
        /// "C:\Program Files (x86)\Graphviz*\bin" NOTE the wildcard. </param>
        /// <param name="tempFile">default generates a random filename</param>
        public static void ShowGraph(
            Graph graph,
            bool openAfterSave = false,
            string pathToGraphvizBin = @".\..\..\deps\graphviz-2.38\bin",
            string tempFile = @"tmp\graph.svg")
        {
            IRenderer renderer = new Renderer(pathToGraphvizBin);
            using (Stream file = File.Create(tempFile)) 
            {
                renderer.RunAsync(
                    graph, file,
                    RendererLayouts.Dot,
                    RendererFormats.Svg,
                    CancellationToken.None).Wait();
            }
            if (openAfterSave) OpenFile(tempFile);
        }

        /// <summary>
        /// Platform independent method to open a file in the default viewer
        /// </summary>
        /// <param name="tempFile"></param>
        private static void OpenFile(string file)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                Process.Start("cmd", $"/c {file}");
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                Process.Start("xdg-open", $"{file}");
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                Process.Start("open", $"{file}");
            }
            Thread.Sleep(250);  // wait for the imageviewer to display the file
                                // it is probably locked for while opening
        }

        static int id = 0;

        public static void buildGraphTree(Node<int> root, ref Graph graph)
        {
            id = 0;
            var nColor = isRightGrowing(root) ? "Sienna" : "skyBlue";
            string nID = $"{id}";

            graph = graph.Add(NodeStatement.For($"{nID}")
                .Set("label", $"ROOT\nheight: {root.Height}")
                .Set("style", "filled")
                .Set("fillcolor", nColor));
            buildGraphTree_iter(root, ref graph);
        }

        /// <summary>
        /// Builds the graph tree and uses the ref Graph object
        /// </summary>
        /// <param name="node"></param>
        /// <param name="graph"></param>
        public static void buildGraphTree_iter(Node<int> node, ref Graph graph)
        {
            string nID = $"{id}";

            var childs = new List<Node<int>>(2);
            childs.AddIfNotNull(node.Left);
            childs.AddIfNotNull(node.Right);

            foreach (var child in childs)
            {
                var newNID = $"{++id}";

                if (child.HasSymbol) // Is a real leaf
                {
                    // Mark leafes at levels above 0
                    var nColor = (child.Height >= 0) ? "GreenYellow" : "red";

                    var label = $"Val.: {child.Symbol.Value}\n"
                         + $"{string.Join("", child.Symbol.Code)} "
                         + $"'L:{child.Symbol.Code.Length}'\n"
                         + $"Freq: {child.Symbol.Frequency}"
                         + $" Heig.: {child.Height}";

                    graph = graph.Add(NodeStatement.For(newNID)
                        .Set("label", label)
                        .Set("style", "filled")
                        .Set("fillcolor", nColor));
                    graph = graph.Add(EdgeStatement.For(nID, newNID));
                }
                else // Is branch
                {
                    // Mark nodes at level 0 or below 
                    // and where it is not right growing tree
                    var nColor = (child.Height <= 0) ? "red"
                        : isRightGrowing(child) ? "DarkGoldenrod" : "skyBlue";

                    if (nColor == "skyBlue")
                    {
                        graph = graph.Add(EdgeStatement.For("0", newNID)
                            .Set("color", "red")
                            .Set("style", "dashed")
                            .Set("penwidth", "4.0"));
                    }

                    graph = graph.Add(NodeStatement.For(newNID)
                        .Set("label", $"height: {child.Height}\n" 
                        +$"ID: {child.Id}")
                        .Set("style", "filled")
                        .Set("fillcolor", nColor));

                    graph = graph.Add(EdgeStatement.For(nID, newNID));
                    buildGraphTree_iter(child, ref graph);
                }
            }
        }

        private static bool isRightGrowing<TSymbol>(Node<TSymbol> node)
        {
            var lHeight = node.Left == null ? 0 : node.Left.Height;
            return (lHeight <= minHeight(node.Right));
        }

        /// <summary>
        /// Function to calculate the minimum depth of the tree
        /// </summary>
        /// <typeparam name="TSymbol"></typeparam>
        /// <param name="node"></param>
        /// <returns></returns>
        private static int minHeight<TSymbol>(Node<TSymbol> node)
        {
            if (node == null) return 0;
            if (node.Left == null && node.Right == null) return 0;
            if (node.Left == null) return minHeight(node.Right) + 1;
            if (node.Right == null) return minHeight(node.Left) + 1;

            return Math.Min(minHeight(node.Left), minHeight(node.Right)) + 1;
        }
    }
}
