using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.CompilerServices;
using fhws.pixelschubser.jpg.encoder.Structures;
using fhws.pixelschubser.jpg.encoder.HuffmanEncoder.HuffmanTree;
using Priority_Queue;

namespace fhws.pixelschubser.jpg.encoder.HuffmanEncoder
{
    public struct HuffmanData<TSymbol>
    {
        public Node<TSymbol> Tree;
        public Dictionary<TSymbol, Symbol<TSymbol>> Codes;
        public List<Symbol<TSymbol>> Data;
    }

    public class HuffmanEncoder<TSymbol>
    {
        /// <summary>
        /// Encode given symbols with Huffman
        /// </summary>
        /// <param name="symbols"></param>
        /// <returns></returns>
        public HuffmanData<TSymbol> Encode(TSymbol[] symbols)
        {
            var tree = BuildHuffmanTree(symbols);
            var table = tree.GenerateHuffmanTable();
            var encoded = table.Encode(symbols);

            return new HuffmanData<TSymbol>
            {
                Tree = tree.Root,
                Codes = table.Codes,
                Data = encoded,
            };
        }

        public HuffmanTree<TSymbol> BuildHuffmanTree(TSymbol[] symbols)
        {
            var symbolCollection = CollectSymbols(symbols);
            return BuildHuffmanTree(symbolCollection);
        }

        public HuffmanTree<TSymbol> BuildHuffmanTree(
                Dictionary<TSymbol, Symbol<TSymbol>> symbolDict)
        {
            var root = GenerateHuffmanTree(symbolDict);
            return new HuffmanTree<TSymbol>(root);
        }

        public HuffmanTree<TSymbol> BuildHuffmanTreeForJpeg(
                TSymbol[] symbols,
                int maxHeight = 16)
        {
            var tree = BuildHuffmanTree(symbols);
            tree.LimitHeight(maxHeight);
            tree.GenerateCodes(maxHeight);
            tree.ReorderRightGrowing();
            // Rearanging the right-most leaf requires the heights to be
            // correct.
            tree.CalculateHeight();
            tree.RearangeRightmostLeaf();

            return tree;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Dictionary<TSymbol, Symbol<TSymbol>> CollectSymbols(TSymbol[] symbols)
        {
            var collection = new Dictionary<TSymbol, Symbol<TSymbol>>();

            foreach (var symbol in symbols)
            {
                if (!collection.ContainsKey(symbol))
                {
                    var initialFrequency = 1;
                    collection.Add(
                        symbol,
                        new Symbol<TSymbol>(symbol, initialFrequency));
                    continue;
                }
                collection[symbol].Frequency++;
            }

            return collection;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private FastPriorityQueue<Node<TSymbol>> BuildPriorityQueue(
            Dictionary<TSymbol, Symbol<TSymbol>> collection)
        {
            var queue = new FastPriorityQueue<Node<TSymbol>>(collection.Count);
            foreach (var symbol in collection.Values)
            {
                var leaf = new Node<TSymbol>(symbol);
                queue.Enqueue(leaf, leaf.Frequency);
            }
            return queue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Node<TSymbol> GenerateHuffmanTree(
            Dictionary<TSymbol, Symbol<TSymbol>> collection)
        {
            var queue = BuildPriorityQueue(collection);

            while (queue.Count > 1)
            {
                var right = queue.Dequeue();
                var left = queue.Dequeue();

                if (left.Height > right.Height)
                {
                    // Swap left / right
                    (left, right) = (right, left);
                }
                var frequency = right.Frequency + left.Frequency;
                var node = new Node<TSymbol>(left, right);

                queue.Enqueue(node, frequency);
            }

            return queue.Dequeue();
        }
    }
}
