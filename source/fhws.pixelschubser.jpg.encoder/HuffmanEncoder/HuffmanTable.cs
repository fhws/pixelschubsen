using System.Collections.Generic;

namespace fhws.pixelschubser.jpg.encoder.HuffmanEncoder
{
    public class HuffmanTable<TSymbol>
    {
        private Dictionary<TSymbol, Symbol<TSymbol>> _Codes;
        public Dictionary<TSymbol, Symbol<TSymbol>> Codes => _Codes;

        public HuffmanTable(Dictionary<TSymbol, Symbol<TSymbol>> codes)
        {
            _Codes = codes;
        }

        public Symbol<TSymbol> Encode(TSymbol symbol)
        {
            return _Codes[symbol];
        }

        public List<Symbol<TSymbol>> Encode(TSymbol[] symbols)
        {
            var encoded = new List<Symbol<TSymbol>>(symbols.Length);
            foreach (var symbol in symbols)
            {
                var code = _Codes[symbol];
                encoded.Add(code);
            }
            return encoded;
        }
    }
}