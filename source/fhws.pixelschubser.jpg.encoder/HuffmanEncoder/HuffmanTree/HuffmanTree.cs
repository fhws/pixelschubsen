using System;
using System.Collections.Generic;
using System.Linq;
using Priority_Queue;

namespace fhws.pixelschubser.jpg.encoder.HuffmanEncoder.HuffmanTree
{
    public class HuffmanTree<TSymbol>
    {
        public Node<TSymbol> Root;
        public int MaxHeight = int.MaxValue;

        public int Height { get => Root.Height; set => Root.Height = value; }

        public HuffmanTree(Node<TSymbol> root)
        {
            Root = root;
        }

        public void LimitHeight(int maxHeight)
        {
            MaxHeight = maxHeight;

            if (Height <= maxHeight) return;

            // --- STEP 1: Remove everything below L --- 
            var removedNodes = Root.Trim(maxHeight - 1);

            // --- STEP 2: Create a binary from removed leaves -------------
            var binaryTree = Node<TSymbol>.BuildBinaryTree(ref removedNodes);

            // --- STEP 3: Insert new y_p note in T^1 ---
            var level = maxHeight - binaryTree.Height - 1;
            if (level < 0)
            {
                throw new Exception("Huffman tree is too large to be "
                    + $"length limited to {maxHeight}");
            }
            var node = Root.GetNodesAtLevel(level)
                // Aggregate is the equivalent of the Reduce function in 
                // other languages (think of Map-Reduce)
                .Aggregate((n1, n2)
                    => n1.Frequency < n2.Frequency ? n1 : n2);

            var newNode = new Node<TSymbol>(null, null);
            if (level == 0)
            {
                // The node is the root node, so we create a new root
                newNode.Right = Root;
                Root = newNode;
            }
            else
            {
                var parent = Root.FindParentOf(node);

                newNode.Right = node;
                if (parent.Left == node) parent.Left = newNode;
                if (parent.Right == node) parent.Right = newNode;
            }

            // --- STEP 4: Insert tree T^2 under y* into T^3 ---
            newNode.Left = binaryTree;
        }

        public HuffmanTable<TSymbol> GenerateHuffmanTable()
        {
            var codes = GenerateCodes();
            return new HuffmanTable<TSymbol>(codes);
        }

        public void RearangeRightmostLeaf()
        {
            var rightNode = _RearangeRightmostLeaf(ref Root, MaxHeight);
            if (rightNode != null)
                throw new Exception("Found no place in the tree to insert the rightmost node");
        }

        private static Node<TSymbol> _RearangeRightmostLeaf(ref Node<TSymbol> node, int maxHeight)
        {
            if (node.IsLeaf) // Root is leaf
            {
                if (maxHeight <= 0)
                    throw new Exception("Tree has not enough space to rearange rightmost node");

                // Replace the root node
                node = new Node<TSymbol>(left: node);
                node.UpdateHeight();
                return null;
            }

            if (node.Right == null)
            {
                //node.UpdateHeight();
                return null; // The rightmost node is already removed
            }

            Node<TSymbol> rightNode;

            if (node.Right.IsLeaf)
            {
                if (maxHeight > 1)
                {
                    node.Right = new Node<TSymbol>(left: node.Right);
                    node.UpdateHeight();
                    return null; // End recursion
                }

                // Remove right node and bubble up
                rightNode = node.Right;
                node.Right = null;
                // NOTE: Do not update the heights, this would sabotage the InsertRightGrowing logic.
                return rightNode;
            }

            // Go for the rightmost node
            rightNode = _RearangeRightmostLeaf(ref node.Right, maxHeight - 1);

            if (rightNode == null)
            {
                node.UpdateHeight();
                return null; // Node has been inserted
            }

            if (node.Left.MinHeight < node.Right.MinHeight)
            {
                InsertRightGrowing(node, rightNode, maxHeight);
                node.UpdateHeight();
                return null; // End recursion
            }

            // Bubble up right node
            return rightNode;
        }

        private static void InsertRightGrowing(Node<TSymbol> tree, Node<TSymbol> newNode, int maxHeight = int.MaxValue)
        {
            var node = _InsertRightGrowing(tree, newNode, maxHeight);
            if (node != null)
                throw new Exception("Found no place in the tree to insert the new node");
        }

        private static Node<TSymbol> _InsertRightGrowing(Node<TSymbol> tree, Node<TSymbol> newNode, int maxHeight)
        {
            if (maxHeight < 0) return newNode; // Bubble up node
                                               //throw new Exception("There is no space in the tree to insert a right growing node");

            if (tree.IsLeaf)
            {
                // TODO: This is a strange case because, if the root node is a
                //       leaf with symbol, we would end up with a symbol that is
                //       not a leaf anymore... What should we do with it?
                tree.Right = newNode;
                tree.UpdateHeight();
                return null; // End recursion
            }

            if (tree.Right != null && tree.Left.MinHeight < tree.Right.MinHeight)
            {
                // Go Left
                if (tree.Left.IsLeaf)
                {
                    tree.Left = new Node<TSymbol>(left: tree.Left, right: newNode);
                    tree.UpdateHeight();
                    return null; // End recursion
                }

                newNode = _InsertRightGrowing(tree.Left, newNode, maxHeight - 1);
                if (newNode == null)
                {
                    tree.UpdateHeight();
                    return null; // Node has been inserted
                }
            }
            if (tree.Right == null || tree.Left.Height >= tree.Right.MinHeight)
            {
                // Go Right
                if (tree.Right == null)
                {
                    tree.Right = newNode;
                    tree.UpdateHeight();
                    return null; // End recursion
                }
                if (tree.Right.IsLeaf)
                {
                    tree.Right = new Node<TSymbol>(left: tree.Right, right: newNode);
                    tree.UpdateHeight();
                    return null; // End recursion
                }

                newNode = _InsertRightGrowing(tree.Right, newNode, maxHeight - 1);
                if (newNode == null)
                {
                    tree.UpdateHeight();
                    return null; // Node has been inserted
                }
            }

            // TODO: Can we reach this point? Is it right to bubble?
            return newNode; // Bubble up node
        }

        // NOTE: Height must be calculated correctly
        public void ReorderRightGrowing()
        {
            // --- STEP 5: Make it right growing again!
            var symbols = Root.GenerateCodes();
            var maxNumLeaves = (int)Math.Pow(2, Height);
            var queue = new FastPriorityQueue<Node<TSymbol>>(maxNumLeaves);
            // First reverse to have the nodes with same priority but more
            // frequency sorted at the end of the queue
            symbols.Values.Reverse()
                .Aggregate(queue, (queue, symbol) =>
                {
                    var targetLayer = symbol.Code.Length;
                    var prio = EncodePrioRightGrowing(targetLayer, 0, 0);
                    var node = new Node<TSymbol>(symbol);
                    queue.Enqueue(node, prio);
                    return queue;
                });

            Root = BuildRightGrowing(queue);
        }

        private static Node<TSymbol> BuildRightGrowing(FastPriorityQueue<Node<TSymbol>> leaves)
        {
            while (leaves.Count > 1)
            {
                var right = leaves.Dequeue(); // highest prio (prio order: level, height, minHeight
                var left = leaves.Dequeue();
                var (LayerRight, heightRight, minHeightRight) = DecodePrioRightGrowing(right.Priority);
                var (LayerLeft, heightLeft, minHeightLeft) = DecodePrioRightGrowing(left.Priority);

                if (LayerRight != LayerLeft)
                {
                    leaves.Enqueue(left, left.Priority);
                    var updatedPrio = EncodePrioRightGrowing(LayerRight - 1, heightRight, minHeightRight);
                    leaves.Enqueue(right, updatedPrio);
                    continue;
                }
                // on same TARGET LAYER

                var node = new Node<TSymbol>(left, right);
                var targetLayer = LayerRight - 1;

                var prio = EncodePrioRightGrowing(targetLayer, node.Height, Math.Min(minHeightLeft, minHeightRight) + 1);
                leaves.Enqueue(node, prio);
            }
            return leaves.Dequeue();
        }
        private static float EncodePrioRightGrowing(int targetLayer, int height, int minHeight)
        {
            var iPrio = targetLayer;
            iPrio = (iPrio * 100) + height;
            iPrio = (iPrio * 100) + minHeight;
            return -(float)iPrio;
        }

        /// <summary>
        /// returns (int targetLayer, int height)
        /// </summary>
        /// <param name="prio"></param>
        /// <returns></returns>
        private static (int, int, int) DecodePrioRightGrowing(float prio)
        {
            var iPrio = -((int)prio);

            var minHeight = iPrio % 100;
            iPrio = (iPrio - minHeight) / 100;

            var height = iPrio % 100;
            iPrio = (iPrio - height) / 100;

            var layer = iPrio;

            return (layer, height, minHeight);
        }

        public Dictionary<TSymbol, Symbol<TSymbol>> GenerateCodes(
                int maxCodeLength = -1,
                List<byte> prefix = null)
        {
            return Root.GenerateCodes(maxCodeLength, prefix);
        }

        public void CalculateHeight()
        {
            Root.CalculateHeight();
        }
    }
}
