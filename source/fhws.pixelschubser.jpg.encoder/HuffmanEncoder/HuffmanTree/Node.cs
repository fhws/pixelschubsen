using System;
using System.Collections.Generic;
using fhws.pixelschubser.jpg.encoder.Structures;
using Priority_Queue;

namespace fhws.pixelschubser.jpg.encoder.HuffmanEncoder.HuffmanTree
{
    public class Node<TSymbol> : FastPriorityQueueNode
    {
        public Node<TSymbol> Left;
        public Node<TSymbol> Right;
        public Symbol<TSymbol> Symbol;
        public int Id = -1;
        private static int nextId = 0;
        public int Frequency = 0;
        public int Height = 0;
        public int MinHeight = 0;
        public bool IsLeaf => Left == null && Right == null;
        public bool HasSymbol => Symbol != null;
        public IEnumerable<Node<TSymbol>> Children
        {
            get
            {
                if (Left != null) yield return Left;
                if (Right != null) yield return Right;
            }
        }

        public Node(Node<TSymbol> left = null, Node<TSymbol> right = null)
        {
            Id = nextId++;

            Left = left;
            Right = right;

            if (left != null) Frequency += left.Frequency;
            if (right != null) Frequency += right.Frequency;

            UpdateHeight();
        }

        public Node(Symbol<TSymbol> symbol)
        {
            Id = nextId++;

            Symbol = symbol;
            Frequency = symbol.Frequency;
        }

        public static Node<TSymbol> BuildBinaryTree(List<Node<TSymbol>> nodesList)
        {
            var queue = new Queue<Node<TSymbol>>(nodesList);
            return BuildBinaryTree(ref queue);
        }

        public static Node<TSymbol> BuildBinaryTree(ref Queue<Node<TSymbol>> nodesQueue)
        {
            while (nodesQueue.Count > 1)
            {
                var left = nodesQueue.Dequeue();
                var right = nodesQueue.Dequeue();
                nodesQueue.Enqueue(new Node<TSymbol>(left, right));
            }
            return nodesQueue.Dequeue();
        }

        /// <symmary>
        /// Searches the tree for the parent of the given child using a
        /// Breadth-First search.
        /// </summary>
        /// <returns>
        /// The parent Node or `null` if the (sub-) tree of `this` Node does not
        /// contain a parent of `child`
        /// </returns>
        public Node<TSymbol> FindParentOf(Node<TSymbol> child)
        {
            if (child == this) return null;

            var nextNodes = new Queue<Node<TSymbol>>();
            nextNodes.Enqueue(this);

            while (nextNodes.Count > 0)
            {
                var node = nextNodes.Dequeue();

                if (node.Left == child) return node;
                if (node.Right == child) return node;

                if (node.Left != null) nextNodes.Enqueue(node.Left);
                if (node.Right != null) nextNodes.Enqueue(node.Right);
            }

            return null;
        }

        // TODO: Test that Height and MinHeight is updated correctly
        public void CalculateHeight()
        {
            if (Left != null) Left.CalculateHeight();
            if (Right != null) Right.CalculateHeight();
            UpdateHeight();
        }

        public void UpdateHeight()
        {
            Height = 0;
            MinHeight = 0;

            if (IsLeaf) return;

            if (Left != null) Height = Left.Height;
            if (Right != null) Height = Math.Max(Height, Right.Height);
            Height++;

            if (Left != null && Right != null)
            {
                // Only if both children are not null, calculate the min height
                // otherwise just keep the min height of 0.
                MinHeight = Math.Min(Left.MinHeight, Right.MinHeight);
                MinHeight++;
            }
        }

        /// <symmary>
        /// Walk through the tree and generate the codes for the Symbols at the
        /// leaves.
        /// <par/>
        /// Make sure that the tree height is set properly before calling this
        /// method. It uses the current trees `Height` property as the maximum
        /// code length. If the `Height` property is incorrect and the tree is
        /// actually higher, an `IndexOutOfRangeException` will occur.
        /// </symmary>
        /// <param name="prefix">In the case that the given node is just a
        /// subtree, a code prefix can be provided as a list of bits</param>
        [Obsolete("Use `GenerateCodes()` without `maxCodeLength` param instead.")]
        public Dictionary<TSymbol, Symbol<TSymbol>> GenerateCodesUnsafe(List<byte> prefix = null)
        {
            var maxCodeLength = Height;
            return GenerateCodes(maxCodeLength, prefix);
        }

        /// <symmary>
        /// Walk through the tree and generate the codes for the Symbols at the
        /// leaves.
        /// <par/>
        /// <param name="maxCodeLength">This shoud be at least as large as the
        /// actual height of the given tree. Otherwise the internally used stack
        /// has to be enlarged and copied every time it runs out of space.
        /// If omitted, the `Height` property of the tree is used. Remember that
        /// the `Height` property is not guaranteed to be correct.</param>
        /// <param name="prefix">In the case that the given node is just a
        /// subtree, a code prefix can be provided as a list of bits</param>
        public Dictionary<TSymbol, Symbol<TSymbol>> GenerateCodes(
                int maxCodeLength = -1,
                List<byte> prefix = null)
        {
            if (maxCodeLength < 0) maxCodeLength = Height;
            var bits = new FastStack<byte>(maxCodeLength);
            var symbols = new Dictionary<TSymbol, Symbol<TSymbol>>();

            if (prefix != null)
            {
                prefix.ForEach(x => bits.Push(x));
            }
            if (IsLeaf && HasSymbol && bits.Count == 0)
            {
                // Special case when the tree is just a leaf
                Symbol.Code = new byte[] { 0 };
                symbols.Add(Symbol.Value, Symbol);
                return symbols;
            }

            _GenerateCodes(bits, symbols);
            return symbols;
        }

        private void _GenerateCodes(
                FastStack<byte> bits,
                Dictionary<TSymbol, Symbol<TSymbol>> leaves)
        {
            if (Left != null)
            {
                bits.Push(0);
                Left._GenerateCodes(bits, leaves);
                bits.Pop();
            }
            if (Right != null)
            {
                bits.Push(1);
                Right._GenerateCodes(bits, leaves);
                bits.Pop();
            }

            if (HasSymbol)
            {
                Symbol.Code = bits.ToArrayBottomTop();
                leaves.Add(Symbol.Value, Symbol);
            }

        }

        /// <summary>
        /// Remove nodes that are below the given max height
        /// </summary>
        public Queue<Node<TSymbol>> Trim(int maxHeight)
        {
            var queue = new Queue<Node<TSymbol>>();
            Trim(maxHeight, ref queue);
            return queue;
        }

        private void Trim(int maxHeight, ref Queue<Node<TSymbol>> removedLeaves)
        {
            if (IsLeaf) return;

            if (maxHeight > 0)
            {
                Left?.Trim(maxHeight - 1, ref removedLeaves);
                Right?.Trim(maxHeight - 1, ref removedLeaves);
                return;
            }

            if (Left != null && Left.HasSymbol)
                removedLeaves.Enqueue(Left);
            else
                Left?.Trim(maxHeight - 1, ref removedLeaves);

            if (Right != null && Right.HasSymbol)
                removedLeaves.Enqueue(Right);
            else
                Right?.Trim(maxHeight - 1, ref removedLeaves);

            Left = null;
            Right = null;
        }

        public List<Node<TSymbol>> GetNodesAtLevel(int level)
        {
            var list = new List<Node<TSymbol>>();
            _GetNodesAtLevel(level, ref list);
            return list;
        }

        private void _GetNodesAtLevel(int level, ref List<Node<TSymbol>> nodes)
        {
            if (level < 0) return;

            if (level == 0)
            {
                nodes.Add(this);
                return;
            }

            Left?._GetNodesAtLevel(level - 1, ref nodes);
            Right?._GetNodesAtLevel(level - 1, ref nodes);
        }
    }
}
