using System;
using fhws.pixelschubser.jpg.encoder.Structures;
using Priority_Queue;

namespace fhws.pixelschubser.jpg.encoder.HuffmanEncoder
{
    public class Symbol<T>
    {
        public readonly T Value;
        public byte[] Code;
        public int Frequency = 0;

        public Symbol(T value, int frequency = 0)
        {
            Value = value;
            Frequency = frequency;
        }
    }
}
