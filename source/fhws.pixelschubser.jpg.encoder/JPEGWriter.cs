﻿using fhws.pixelschubser.jpg.encoder.Dct;
using fhws.pixelschubser.jpg.encoder.Extensions;
using fhws.pixelschubser.jpg.encoder.HuffmanEncoder;
using fhws.pixelschubser.jpg.encoder.Models;
using fhws.pixelschubser.jpg.encoder.Structures;
using fhws.pixelschubser.jpg.encoder.Structures.Segments;
using System.Collections.Generic;
using System.Linq;
using static fhws.pixelschubser.jpg.encoder.DataEncoding;

namespace fhws.pixelschubser.jpg.encoder
{
    public class JPEGWriter
    {
        readonly BitWriter Writer;

        public JPEGWriter(BitWriter writer) => Writer = writer;

        public void EncodePicture(Picture<YCBCRPixel<int>> picture, CompressionLevel compressionLevel = CompressionLevel.Medium)
        {
            var subSampledPicture = SubSamplePicture(picture);
            var dctCoefficients = CalculateDct(subSampledPicture);

            var quantizedCoefficents = Quantization.Quantize(dctCoefficients, compressionLevel);

            var dcCategoryEncoded = DataEncoding.EncodeDC(quantizedCoefficents);
            var acCategoryEncoded = DataEncoding.EncodeAC(quantizedCoefficents);

            var huffmanDC = CreateDCHuffman(dcCategoryEncoded, dctCoefficients);
            var huffmanAC = CreateACHuffman(acCategoryEncoded);

            WriteSegments(picture, huffmanDC, huffmanAC, compressionLevel);
        }

        internal (int[,] Y, int[,] Cb, int[,] Cr) SubSamplePicture(Picture<YCBCRPixel<int>> picture)
        {
            var channels = picture.Data.SeparateColorChannels();

            // Sample with 4:2:2
            channels.Cb = channels.Cb.SubSample(2, 2);
            channels.Cr = channels.Cr.SubSample(2, 2);

            return channels;
        }

        internal (List<double>[,] Y, List<double>[,] Cb, List<double>[,] Cr) CalculateDct((int[,] Y, int[,] Cb, int[,] Cr) channels)
        {
            var Y = channels.Y.ConvertAll(item => (double)item);
            var Cb = channels.Cb.ConvertAll(item => (double)item);
            var Cr = channels.Cr.ConvertAll(item => (double)item);

            var dct = new DctArai();
            dct.TransformInplace(Y);
            dct.TransformInplace(Cb);
            dct.TransformInplace(Cr);

            return (
                Y: ZickZackSampler.ZickZackSampleAll(Y),
                Cb: ZickZackSampler.ZickZackSampleAll(Cb),
                Cr: ZickZackSampler.ZickZackSampleAll(Cr)
                );
        }

        internal HuffmanDataDC CreateDCHuffman(
                EncodedDc categoryEncodedDcLists,
                (List<double>[,] Y, List<double>[,] Cb, List<double>[,] Cr) dctCoefficients)
        {
            var CbCr = new List<(byte category, int bits)>();
            CbCr.AddRange(categoryEncodedDcLists.Cb);
            CbCr.AddRange(categoryEncodedDcLists.Cr);
            var huffman = new HuffmanEncoder<byte>();
            var dcHuffmanTrees = (
                Y: huffman.BuildHuffmanTreeForJpeg(categoryEncodedDcLists.Y.Select(x => x.category).ToArray()),
                CbCr: huffman.BuildHuffmanTreeForJpeg(CbCr.Select(x => x.category).ToArray())
            );
            var dcHuffmanTables = (
                Y: dcHuffmanTrees.Y.GenerateHuffmanTable(),
                CbCr: dcHuffmanTrees.CbCr.GenerateHuffmanTable()
            );
            var huffmanEncodedDcLists = (
                Y: categoryEncodedDcLists.Y.Select(
                    x => (encodedCategory: dcHuffmanTables.Y.Encode(x.category).Code, category: x.category, bits: x.bits)).ToList(),
                Cb: categoryEncodedDcLists.Cb.Select(
                    x => (encodedCategory: dcHuffmanTables.CbCr.Encode(x.category).Code, category: x.category, bits: x.bits)).ToList(),
                Cr: categoryEncodedDcLists.Cr.Select(
                    x => (encodedCategory: dcHuffmanTables.CbCr.Encode(x.category).Code, category: x.category, bits: x.bits)).ToList()
            );
            var huffmanEncodedDcCoefficients = (
                Y: huffmanEncodedDcLists.Y.Reshape((2, 2), dctCoefficients.Y.GetLength(1)),
                Cb: huffmanEncodedDcLists.Cb.Reshape(dctCoefficients.Cb.GetLength(1)),
                Cr: huffmanEncodedDcLists.Cr.Reshape(dctCoefficients.Cr.GetLength(1))
            );

            return new HuffmanDataDC(huffmanEncodedDcCoefficients, dcHuffmanTables);
        }

        internal HuffmanDataAC CreateACHuffman(EncodedAc combinedEncodedAcLists)
        {
            var combinedAcBytes = (Y: new List<byte>(), CbCr: new List<byte>());
            combinedEncodedAcLists.Y.ForAll((2, 2), x => combinedAcBytes.Y.AddRange(x.Select(x => x.combined).ToList()));
            combinedEncodedAcLists.Cb.ForAll(x => combinedAcBytes.CbCr.AddRange(x.Select(x => x.combined).ToList()));
            combinedEncodedAcLists.Cr.ForAll(x => combinedAcBytes.CbCr.AddRange(x.Select(x => x.combined).ToList()));

            var huffman = new HuffmanEncoder<byte>();

            var acHuffmanTrees = (
                Y: huffman.BuildHuffmanTreeForJpeg(combinedAcBytes.Y.ToArray()),
                CbCr: huffman.BuildHuffmanTreeForJpeg(combinedAcBytes.CbCr.ToArray())
            );
            var acHuffmanTables = (
                Y: acHuffmanTrees.Y.GenerateHuffmanTable(),
                CbCr: acHuffmanTrees.CbCr.GenerateHuffmanTable()
            );
            var huffmanEncodedAcLists = (
                Y: combinedEncodedAcLists.Y.ConvertAll(list => list.Select(
                   x => (encodedZeroesAndCategory: acHuffmanTables.Y.Encode(x.combined).Code, bits: x.bits, bitsLen: x.category)).ToList()),
                Cb: combinedEncodedAcLists.Cb.ConvertAll(list => list.Select(
                   x => (encodedZeroesAndCategory: acHuffmanTables.CbCr.Encode(x.combined).Code, bits: x.bits, bitsLen: x.category)).ToList()),
                Cr: combinedEncodedAcLists.Cr.ConvertAll(list => list.Select(
                   x => (encodedZeroesAndCategory: acHuffmanTables.CbCr.Encode(x.combined).Code, bits: x.bits, bitsLen: x.category)).ToList())
            );

            return new HuffmanDataAC(huffmanEncodedAcLists, acHuffmanTables);
        }

        internal struct HuffmanDataDC
        {
            public ((byte[] encodedCategory, byte category, int bits)[,] Y,
                    (byte[] encodedCategory, byte category, int bits)[,] Cb,
                    (byte[] encodedCategory, byte category, int bits)[,] Cr) huffmanEncodedCoefficents;

            public (HuffmanTable<byte> Y, HuffmanTable<byte> CbCr) huffmanTables;

            public HuffmanDataDC(((byte[] encodedCategory, byte category, int bits)[,] Y,
                (byte[] encodedCategory, byte category, int bits)[,] Cb,
                (byte[] encodedCategory, byte category, int bits)[,] Cr) coeff, (HuffmanTable<byte> Y, HuffmanTable<byte> CbCr) tables)
            {
                huffmanEncodedCoefficents = coeff;
                huffmanTables = tables;
            }
        }

        internal struct HuffmanDataAC
        {
            public (List<(byte[] encodedCategory, int bits, byte bitsLen)>[,] Y,
                 List<(byte[] encodedCategory, int bits, byte bitsLen)>[,] Cb,
                 List<(byte[] encodedCategory, int bits, byte bitsLen)>[,] Cr) huffmanEncodedCoefficents;

            public (HuffmanTable<byte> Y, HuffmanTable<byte> CbCr) huffmanTables;

            public HuffmanDataAC((List<(byte[] encodedCategory, int bits, byte bitsLen)>[,] Y,
                 List<(byte[] encodedCategory, int bits, byte bitsLen)>[,] Cb,
                 List<(byte[] encodedCategory, int bits, byte bitsLen)>[,] Cr) coeff, (HuffmanTable<byte> Y, HuffmanTable<byte> CbCr) tables)
            {
                huffmanEncodedCoefficents = coeff;
                huffmanTables = tables;
            }
        }

        internal void WriteSegments<T>(Picture<YCBCRPixel<T>> pic, HuffmanDataDC huffmanDataDC, HuffmanDataAC huffmanDataAC, CompressionLevel compressionLevel)
        {
            Writer.WriteUShort(SegmentMarker.SOI);
            new APP0Segment(Writer).WriteSegment();
            Writer.FillRemainingBitsWithOnes();

            var quantTables = Quantization.GetQuantTableZickZackSorted(compressionLevel);
            new DQTSegment(Writer, quantTables.yQuantTable.ToArray(), quantTables.CbCrQuantTable.ToArray()).WriteSegment();
            Writer.FillRemainingBitsWithOnes();

            var origHeight = (ushort)pic.Metadata.OrigHeight;
            var origWidth = (ushort)pic.Metadata.OrigWidth;
            new SOF0Segment(Writer, origHeight, origWidth, new SampleReduceRate()).WriteSegment();
            Writer.FillRemainingBitsWithOnes();

            new DHTSegment(Writer, numberHT: 0, huffmanDataDC.huffmanTables.Y.Codes.Values.ToList()).WriteSegment();
            Writer.FillRemainingBitsWithOnes(); //?
            new DHTSegment(Writer, numberHT: 0, huffmanDataAC.huffmanTables.Y.Codes.Values.ToList(), isAC: true).WriteSegment();
            Writer.FillRemainingBitsWithOnes(); //?
            new DHTSegment(Writer, numberHT: 1, huffmanDataDC.huffmanTables.CbCr.Codes.Values.ToList()).WriteSegment();
            Writer.FillRemainingBitsWithOnes(); //?
            new DHTSegment(Writer, numberHT: 1, huffmanDataAC.huffmanTables.CbCr.Codes.Values.ToList(), isAC: true).WriteSegment();
            Writer.FillRemainingBitsWithOnes(); //?

            new SOSSegment(Writer).WriteSegment();
            Writer.FillRemainingBitsWithOnes(); //?

            #region Write Data
            Writer._HACK_DEV_MAKE_BETTER_EscapeFFBytes = true;

            var mcuCountX = pic.Metadata.PaddedWidth / pic.Metadata.StepSize;
            var mcuCountY = pic.Metadata.PaddedHeight / pic.Metadata.StepSize;
            for (var y = 0; y < mcuCountY; y++)
            {
                for (var x = 0; x < mcuCountX; x++)
                {
                    var offsetX = x * 2;
                    var offsetY = y * 2;

                    var dcCoefficients = huffmanDataDC.huffmanEncodedCoefficents;
                    var acCoefficients = huffmanDataAC.huffmanEncodedCoefficents;

                    // Y1 Y2
                    // Y3 Y4
                    WriteCoefficients(Writer, dcCoefficients.Y, acCoefficients.Y, (offsetX, offsetY));
                    WriteCoefficients(Writer, dcCoefficients.Y, acCoefficients.Y, (offsetX + 1, offsetY));
                    WriteCoefficients(Writer, dcCoefficients.Y, acCoefficients.Y, (offsetX, offsetY + 1));
                    WriteCoefficients(Writer, dcCoefficients.Y, acCoefficients.Y, (offsetX + 1, offsetY + 1));

                    // Cb Cr
                    WriteCoefficients(Writer, dcCoefficients.Cb, acCoefficients.Cb, (x, y));
                    WriteCoefficients(Writer, dcCoefficients.Cr, acCoefficients.Cr, (x, y));
                }
            }

            Writer.FillRemainingBitsWithOnes();
            Writer._HACK_DEV_MAKE_BETTER_EscapeFFBytes = false;
            #endregion

            Writer.WriteUShort(SegmentMarker.EOI);
        }

        private void WriteCoefficients(BitWriter bw, (byte[] encodedCategory, byte category, int bits)[,] dcCoefficients, List<(byte[] encodedZeroesAndCategory, int bits, byte bitsLen)>[,] acCoefficients, (int X, int Y) offset)
        {
            var dcCoefficient = dcCoefficients[offset.Y, offset.X];
            bw.WriteBits(dcCoefficient.encodedCategory);
            var bits = dcCoefficient.bits;
            var count = dcCoefficient.category;
            var bitOffset = sizeof(int) * 8 - count;
            bw.WriteFromInt(bits, bitOffset, count);
            foreach (var acCoefficient in acCoefficients[offset.Y, offset.X])
            {
                bw.WriteBits(acCoefficient.encodedZeroesAndCategory);
                bits = acCoefficient.bits;
                count = acCoefficient.bitsLen;
                bitOffset = sizeof(int) * 8 - count;
                bw.WriteFromInt(bits, bitOffset, count);
            }
        }
    }
}
