﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fhws.pixelschubser.jpg.encoder.Models
{
    public struct RGBPixel<T>
    {
        public T R { get; set; }
        public T G { get; set; }
        public T B { get; set; }
    }
}
