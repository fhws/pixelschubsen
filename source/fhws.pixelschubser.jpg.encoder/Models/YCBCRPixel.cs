﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fhws.pixelschubser.jpg.encoder.Models
{
    public class YCBCRPixel<T>
    {
        public T Y { get; set; }
        public T CB { get; set; }
        public T CR { get; set; }
    }
}
