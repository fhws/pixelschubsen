﻿using fhws.pixelschubser.jpg.encoder.Models;
using fhws.pixelschubser.jpg.encoder.Structures;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace fhws.pixelschubser.jpg.encoder
{
    public static class PPMReader
    {
        public static Picture<RGBPixel<byte>> Read(string path, uint stepSize)
        {
            char[] whiteSpace = { ' ', '\t', '\n', '\r' };
            String line;

            var stream = new FileStream(path, FileMode.Open);
            var file = new FastBufferedBinaryReader(stream, 32768);

            var headerInfos = new List<string>(4);
            while (headerInfos.Count < 4)
            {
                line = file.ReadLine();
                if (line.Trim().StartsWith('#')) continue;
                headerInfos.AddRange(line.Split(whiteSpace,
                    StringSplitOptions.RemoveEmptyEntries));
            }

            if (headerInfos[0] != "P3") throw new FormatException("PPM needs to be Plain format");

            var header = new PictureMetadata
            {
                OrigWidth = uint.Parse(headerInfos[1]),
                OrigHeight = uint.Parse(headerInfos[2]),
                MaxColorValue = uint.Parse(headerInfos[3]),
                StepSize = stepSize
            };

            header.PaddedHeight = header.OrigHeight;
            if (header.PaddedHeight % stepSize > 0)
                header.PaddedHeight += (stepSize - header.OrigHeight % stepSize);
            header.PaddedWidth = header.OrigWidth;
            if (header.PaddedWidth % stepSize > 0)
                header.PaddedWidth += (stepSize - header.OrigWidth % stepSize);

            if (header.MaxColorValue != 255) throw new FormatException("PPM Pixels have to be in 8-bit Format");

            RGBPixel<byte>[,] data = ParseBody(ref file, header);
            file.Close();

            // repeat pixels at border
            for (uint i = header.OrigHeight; i < header.PaddedHeight; ++i)
            {
                for (uint j = 0; j < header.PaddedWidth; j++)
                {
                    var maxRow = header.OrigHeight - 1;
                    var pixel = data[maxRow, j];
                    data[i, j] = pixel;
                }
            }
            for (uint i = header.OrigWidth; i < header.PaddedWidth; ++i)
            {
                for (uint j = 0; j < header.PaddedHeight; j++)
                {
                    var maxColumn = header.OrigWidth - 1;
                    var pixel = data[j, maxColumn];
                    data[j, i] = pixel;
                }
            }

            return new Picture<RGBPixel<byte>>(data, header);
        }


        private static RGBPixel<byte>[,] ParseBody(ref FastBufferedBinaryReader file,
           PictureMetadata header)
        {
            int nextChar;
            byte rgbCounter = 0;
            uint col = 0, row = 0;
            var data = new RGBPixel<byte>[header.PaddedHeight, header.PaddedWidth];
            bool hasColor = false;
            byte color = 0;

            while (file.FillBuffer())
            {
                for (
                    var numReads = file.NumBytesAvailable / sizeof(byte);
                    numReads > 0;
                    --numReads
                )
                {
                    nextChar = file.ReadChar();
                    switch (nextChar)
                    {
                        case ' ':
                        case '\t':
                        case '\n':
                        case '\r':
                            if (!hasColor) continue;

                            switch (rgbCounter++) // fill R or G or B
                            {
                                case 0:
                                    data[row, col].R = color;
                                    break;
                                case 1:
                                    data[row, col].G = color;
                                    break;
                                case 2:
                                    data[row, col].B = color;
                                    rgbCounter = 0;
                                    col++; // advance
                                    if (col >= header.OrigWidth) // new pixel line
                                    {
                                        row++;   // LF
                                        col = 0; // CR
                                    }
                                    break;
                                default:
                                    break;
                            }

                            color = 0;
                            hasColor = false;
                            break;
                        case '#':
                            while (file.ReadChar() != '\n') numReads--; // gobbles to line end
                            break;
                        default:
                            color *= 10;
                            // Parse int val of char leveraging ASCII table
                            color += (byte)(nextChar - '0');
                            hasColor = true;
                            break;
                    }
                }
            }
            return data;
        }
    }
}
