﻿using fhws.pixelschubser.jpg.encoder.Models;
using fhws.pixelschubser.jpg.encoder.Structures;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace fhws.pixelschubser.jpg.encoder
{
    public static class PPMWriter
    {
        public static void Write(string path, Picture<RGBPixel<byte>> picture)
        {
            using StreamWriter file = new StreamWriter(path, false, Encoding.Default, 32768);
            StringBuilder sb = new StringBuilder(checked((int)picture.Metadata.OrigWidth) * 3 * 3 + 3 + 15);
            sb.AppendLine("P3");
            sb.AppendLine("# Created with our fancy PPMWriter"); 
            sb.AppendLine($"{picture.Metadata.OrigWidth} {picture.Metadata.OrigHeight}");
            sb.AppendLine("255");
            file.Write(sb);
            sb.Clear();

            for (int i = 0; i < picture.Metadata.OrigHeight; i++)
            {
                for (int j = 0; j < picture.Metadata.OrigWidth; j++)
                {
                    var pixel = picture.Data[i, j];
                    sb.Append(pixel.R);
                    sb.Append(' ');
                    sb.Append(pixel.G);
                    sb.Append(' ');
                    sb.Append(pixel.B);
                    sb.Append(' ');
                }
                sb.Append('\n');
                file.Write(sb);
                sb.Clear();
            }
        }
    }
}
