using fhws.pixelschubser.jpg.encoder.Dct;
using fhws.pixelschubser.jpg.encoder.Structures;
using Mono.Options;
using System;
using System.IO;
using System.Linq;

namespace fhws.pixelschubser.jpg.encoder
{
    class Program
    {
        static void Main(string[] args)
        {
            bool show_help = false;

            CompressionLevel compressionLevel = CompressionLevel.Low;
            string path = Environment.CurrentDirectory;

            var p = new OptionSet()
            {
                "Usage:",
                "  fhws.pixelschubser.jpg.encoder.exe [OPTIONS] path",
                "",
                "Options:",
                { "i|input=" , "(REQUIRED) Input path of a ppm file / path to a folder containing ppm files" ,
                    x => path = x},
                {"c|compression=" , "Compression level: 0 = None; 1 = Low; 2 = Medium 3 = High (default: 1)" ,
                    (int x) => compressionLevel = (CompressionLevel)x},
                { "h|help",  "Show this message and exit",
                    x => show_help = x != null },
            };

            try
            {
                p.Parse(args);
            }
            catch (OptionException e)
            {
                Console.Error.WriteLine("Try `--help` for more information.");
                Console.Error.WriteLine(e.Message);
                return;
            }

            if (show_help)
            {
                p.WriteOptionDescriptions(Console.Out);
                return;
            }

            if (path == null) {
                Console.Error.WriteLine("The `--input` argument is required. See `--help` for more information.");
                return;
            }

            if (File.Exists(path))
            {
                // Encode single file
                TryEncodePicture(path, compressionLevel);
            }
            else if (Directory.Exists(path))
            {
                // Encode all ppm files of directory
                var files = Directory.EnumerateFiles(path).Where(f => Path.GetExtension(f).ToLower().Equals(".ppm")).ToList();
                foreach (var (file, idx) in files.Select((idx, el) => (idx, el)))
                {
                    Console.WriteLine($"==> Processing file {idx+1}/{files.Count}: {file}");
                    TryEncodePicture(file, compressionLevel);
                }
            }
            else
            {
                Console.Error.WriteLine("Path does not exits.");
            }
        }

        private static void TryEncodePicture(string file, CompressionLevel compressionLevel)
        {
            try
            {
                EncodePicture(file, Path.ChangeExtension(file, ".jpg"), compressionLevel);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void EncodePicture(string pathIn, string pathOut, CompressionLevel compressionLevel = CompressionLevel.High)
        {
            if (!Path.GetExtension(pathIn).ToLower().Equals(".ppm"))
            {
                Console.WriteLine("Not a ppm file");
                return;
            }

            var pic = PPMReader.Read(pathIn, 16);
            var ycbcr = pic.createYCBCRPicture();

            using var fs = new FileStream(pathOut, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
            using var bw = new BitWriter(fs);

            var jpgWriter = new JPEGWriter(bw);
            jpgWriter.EncodePicture(ycbcr, compressionLevel);
        }
    }
}
