using System;
using fhws.pixelschubser.jpg.encoder.Models;
using fhws.pixelschubser.jpg.encoder.Structures;

namespace fhws.pixelschubser.jpg.encoder
{
    public class ResolutionReducer
    {
        Picture<YCBCRPixel<byte>> picture;

        public ResolutionReducer(ref Picture<YCBCRPixel<byte>> picture)
        {
            this.picture = picture;
        }

        public ReducedPicture ReduceBy(SampleReduceRate sampleReduceRate)
        {
            var origWidth = picture.Metadata.OrigWidth;
            var origHeight = picture.Metadata.OrigHeight;
            var width = picture.Metadata.PaddedWidth;
            var height = picture.Metadata.PaddedHeight;

            var pixelCount = width * height;

            var rateY = sampleReduceRate.Y;
            var rateCb = sampleReduceRate.Cb;
            var rateCr = sampleReduceRate.Cr;

            var lenY = (int)(pixelCount / rateY);
            var lenCb = (int)(pixelCount / rateCb);
            var lenCr = (int)(pixelCount / rateCr);

            var Y = new byte[lenY];
            var Cb = new byte[lenCb];
            var Cr = new byte[lenCr];

            var posY = 0;
            var posCb = 0;
            var posCr = 0;

            for (uint y = 0; y < height; y++)
            {
                for (uint x = 0; x < width; x++)
                {
                    if (x % rateY == 0)
                    {
                        Y[posY] = picture.Data[y, x].Y;
                        posY++;
                    }

                    if (x % rateCb == 0)
                    {
                        Cb[posCb] = picture.Data[y, x].CB;
                        posCb++;
                    }

                    if (x % rateCr == 0)
                    {
                        Cr[posCr] = picture.Data[y, x].CR;
                        posCr++;
                    }
                }
            }

            return new ReducedPicture(sampleReduceRate, Y, Cb, Cr,
                                      origWidth, origHeight,
                                      width, height);
        }
    }
}
