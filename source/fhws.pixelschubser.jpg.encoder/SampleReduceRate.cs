using System;

namespace fhws.pixelschubser.jpg.encoder
{

    public struct SampleReduceRate
    {
        public int Y;
        public int Cb;
        public int Cr;

        public SampleReduceRate(int Y, int Cb, int Cr)
        {
            this.Y = Y;
            this.Cb = Cb;
            this.Cr = Cr;
        }
    }

}