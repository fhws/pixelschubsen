using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace fhws.pixelschubser.jpg.encoder.Structures
{
    public class FastStack<T>
    {
        private const int DefaultCapacity = 8;

        T[] Stack;
        int Pointer = 0;

        public int Count => Pointer;
        public int Capacity => Stack.Length;
        public bool Empty => Pointer == 0;

        public FastStack()
        {
            Stack = new T[0];
        }

        public FastStack(int capacity)
        {
            Stack = new T[capacity];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Push(T elem)
        {
            if (Pointer >= Capacity)
            {
                ExpandMemory();
            }
            Stack[Pointer++] = elem;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private void ExpandMemory()
        {
            if (Capacity <= 0) {
                Stack = new T[DefaultCapacity];
                return;
            }

            var newCapacity = Capacity;

            // Prevent the int from overflowing when Capacity is too large
            if (newCapacity >= int.MaxValue / 2)
                newCapacity = int.MaxValue;
            else
                newCapacity *= 2;

            var tmp = Stack;
            Stack = new T[newCapacity];
            Buffer.BlockCopy(tmp, 0, Stack, 0, tmp.Length);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Pop()
        {
            return Stack[--Pointer];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Peek()
        {
            return Stack[Pointer - 1];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T[] ToArrayBottomTop()
        {
            var buffer = new T[Pointer];
            Buffer.BlockCopy(Stack, 0, buffer, 0, Pointer);
            return buffer;
        }
    }
}
