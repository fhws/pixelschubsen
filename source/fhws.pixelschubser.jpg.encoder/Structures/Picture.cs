using fhws.pixelschubser.jpg.encoder.Models;
using System;
using System.Threading.Tasks;
using static fhws.pixelschubser.jpg.encoder.Models.Types;

namespace fhws.pixelschubser.jpg.encoder.Structures
{
    public class Picture<P>
    {
        public P[,] Data { get; set; }
        public readonly PictureMetadata Metadata;

        [Obsolete("Using OriginalWidth is deprecated. Use `Metadata.OrigWidth` instead.")]
        public uint OriginalWidth { get => Metadata.OrigWidth; }
        [Obsolete("Using OriginalHeight is deprecated. Use `Metadata.OrigHeight` instead.")]
        public uint OriginalHeight { get => Metadata.OrigHeight; }
        [Obsolete("Using MaxValue is deprecated. Use `Metadata.MaxColorValue` instead.")]
        public uint MaxValue { get => Metadata.MaxColorValue; }
        [Obsolete("Using PaddedWidth is deprecated. Use `Metadata.PaddedWidth` instead.")]
        public uint PaddedWidth { get => Metadata.PaddedWidth; }
        [Obsolete("Using PaddedHeight is deprecated. Use `Metadata.PaddedHeight` instead.")]
        public uint PaddedHeight { get => Metadata.PaddedHeight; }
        [Obsolete("Using StepSize is deprecated. Use `Metadata.StepSize` instead.")]
        public uint StepSize { get => Metadata.StepSize; }

        public Picture(P[,] data, PictureMetadata metadata)
        {
            Data = data;
            Metadata = metadata;
        }

        [Obsolete("Using Picture constructor without metadata struct is deprecated. Use the constructor with `PictureMetadata` instead.")]
        public Picture(P[,] data, uint width, uint height, uint maxValue, uint swidth, uint sheight, uint stepSize)
        {
            Data = data;
            Metadata.OrigWidth = width;
            Metadata.OrigHeight = height;
            Metadata.MaxColorValue = maxValue;
            Metadata.PaddedWidth = swidth;
            Metadata.PaddedHeight = sheight;
            Metadata.StepSize = stepSize;
        }

        public P GetPixel(uint x, uint y)
        {
            return Data[y, x];
        }

        public P this[uint x, uint y]
        {
            get => Data[y, x];
            set => Data[y, x] = value;
        }

        public void ExecuteOnAll(ActionRef<P> func)
        {
            for (int i = 0; i < Data.GetLength(0); ++i)
            {
                for (int j = 0; j < Data.GetLength(1); ++j)
                {
                    func(ref Data[i, j]);
                }
            }
        }

        public Tuple<uint, uint> Size()
        {
            return new Tuple<uint, uint>(Metadata.OrigWidth, Metadata.OrigHeight);
        }
    }

    public static class PictureExtensions
    {
        private static YCBCRPixel<int> convertToYCBCRPixel(RGBPixel<byte> pixel)
        {
            Func<double, int> round = (double x) => (int)(x + 0.5);
            int norm = 128; // to center around zero

            return new YCBCRPixel<int>
            {
                // TODO: this will propably crash since I doubt that only values above 0 will be yielded.
                Y = (round(0.299 * pixel.R + 0.587 * pixel.G + 0.114 * pixel.B) - norm),
                CB = (round(128 + -0.1687 * pixel.R + -0.3312 * pixel.G + 0.5 * pixel.B) - norm),
                CR = (round(128 + 0.5 * pixel.R + -0.4186 * pixel.G + -0.0813 * pixel.B) - norm)
            };
        }

        public static Picture<YCBCRPixel<int>> createYCBCRPicture(this Picture<RGBPixel<byte>> self)
        {
            YCBCRPixel<int>[,] data = new YCBCRPixel<int>[self.Metadata.PaddedHeight, self.Metadata.PaddedWidth];

            var result  = Parallel.For(0, data.GetLength(0), (int i) =>
            {
                for (int j = 0; j < data.GetLength(1); ++j)
                {
                    data[i, j] = convertToYCBCRPixel(self.Data[i, j]);
                }
            });

            return new Picture<YCBCRPixel<int>>(data, self.Metadata);
        }
    }
}
