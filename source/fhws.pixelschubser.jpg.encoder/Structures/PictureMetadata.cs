using System;

namespace fhws.pixelschubser.jpg.encoder.Structures
{
    public struct PictureMetadata
    {
        public uint OrigWidth;
        public uint OrigHeight;
        public uint PaddedHeight;
        public uint PaddedWidth;
        public uint MaxColorValue;
        public uint StepSize;
    }
}
