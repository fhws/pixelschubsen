using System;
using fhws.pixelschubser.jpg.encoder.Models;

namespace fhws.pixelschubser.jpg.encoder.Structures
{
    public class ReducedPicture
    {
        SampleReduceRate colorReduceFactor;
        public byte[] Y;
        public byte[] Cb;
        public byte[] Cr;
        public uint originalWidth;
        public uint originalHeight;
        public uint width;
        public uint height;

        public ReducedPicture(
            SampleReduceRate colorReduceFactor,
            byte[] dataY, byte[] dataCb, byte[] dataCr,
            uint origWidth, uint origHeight,
            uint width, uint height
        )
        {
            this.colorReduceFactor = colorReduceFactor;
            this.Y = dataY;
            this.Cb = dataCb;
            this.Cr = dataCr;
            this.originalWidth = origWidth;
            this.originalHeight = origHeight;
            this.width = width;
            this.height = height;
        }
    }
}
