using System;

namespace fhws.pixelschubser.jpg.encoder.Structures
{
    public class Ref<T>
    {
        public T Value;

        public Ref(T value)
        {
            Value = value;
        }
    }
}
