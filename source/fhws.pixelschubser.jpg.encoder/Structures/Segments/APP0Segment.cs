﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fhws.pixelschubser.jpg.encoder.Structures.Segments
{
    public class APP0Segment : IJPGSegment
    {
        private BitWriter Writer;

        private readonly byte[] jfif = Encoding.ASCII.GetBytes("JFIF\0");
        private const byte majorVersion = 1;
        private const byte minorVersion = 1;
        private const byte pixelDensity = 0;
        private const ushort densityX = 72;
        private const ushort densityY = 72;
        private const byte sizePreviewPictureX = 0;
        private const byte sizePreviewPictureY = 0;
        
        public ushort Marker { get => SegmentMarker.APP0; }
        
        public ushort Length
        {
            get
            {
                int size = 2     // Length
                         + 5     // jfif.Length
                         + 1 + 1 // Version (major + minor)
                         + 1 + 2 + 2 // (pixelDensity + densityX + densityY)
                         + 1 + 1 // sizePreviewPictureX + sizePreviewPictureY
                         + 0;    // Preview image data (which we don't have and write becaus it's stupid and no one does it so we will not do it ok? Why are you still reading this? Get lost!)

                return (ushort)size;
            }
        }

        public APP0Segment(BitWriter writer)
        {
            Writer = writer;
        }

        public void WriteSegment()
        {
            Writer.WriteUShort(Marker);
            Writer.WriteUShort(Length);

            // JFIF
            Writer.WriteBytes(jfif);

            // Version
            Writer.WriteByte(majorVersion);
            Writer.WriteByte(minorVersion);

            // Density
            Writer.WriteByte(pixelDensity);
            Writer.WriteUShort(densityX);
            Writer.WriteUShort(densityY);

            // Preview Picture
            Writer.WriteByte(sizePreviewPictureX);
            Writer.WriteByte(sizePreviewPictureY);
        }
    }
}
