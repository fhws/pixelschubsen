using fhws.pixelschubser.jpg.encoder.HuffmanEncoder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fhws.pixelschubser.jpg.encoder.Structures.Segments
{
    public class DHTSegment : IJPGSegment
    {
        public ushort Marker { get => SegmentMarker.DHT; }
        public ushort Length;
        public byte NumberHT = 0;
        public byte IsAC = 0;
        public readonly byte[] AmountSymbols = new byte[16];
        public byte[] Symbols;
        public BitWriter Writer;

        /// <summary>
        /// The `codes` list must be sorted by code: Shorter codes first, codes
        /// of same length should be ordered by their binary value (e.g. `0011`
        /// should come before `0101`).
        /// </summary>
        public DHTSegment(BitWriter writer, int numberHT, List<Symbol<byte>> codes, bool isAC = false)
        {
            if (numberHT > 3 || numberHT < 0)
            {
                throw new ArgumentException( $"The number of the Huffman tree "
                    +  $"'numberHT' must be between [0, 3] but was {numberHT}");
            }
            Writer = writer;
            NumberHT = (byte)numberHT;
            IsAC = (byte)(isAC ? 1 : 0);

            // Get amount of symbols per codeLength
            codes.GroupBy(symbol => symbol.Code.Length, (codeLength, symbols)
                    => (codeLength: codeLength, count: symbols.Count()))
                .ToList().ForEach(group
                    => AmountSymbols[group.codeLength - 1] = (byte)group.count) ;

            Symbols = codes.ConvertAll((symbol) => (byte)symbol.Value).ToArray();

            Length = (ushort)(2  // Length
                            + 1  // HTInformation
                            + 16 // AmountSymbols
                            + Symbols.Length);
        }

        public void WriteSegment()
        {
            Writer.WriteUShort(Marker);
            Writer.WriteUShort(Length);
            // HT Info byte
            Writer.Write(0x0, offset: 5, count: 3);      // LSB: bit 8..6 unused
            Writer.WriteBit(IsAC);                       // LSB: bit 5
            Writer.Write(NumberHT, offset: 4, count: 4); // LSB: bit 4..1
            Writer.WriteBytes(AmountSymbols);
            Writer.WriteBytes(Symbols);
        }
    }
}
