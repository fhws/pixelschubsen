﻿using fhws.pixelschubser.jpg.encoder.HuffmanEncoder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static fhws.pixelschubser.jpg.encoder.ZickZackSampler;

namespace fhws.pixelschubser.jpg.encoder.Structures.Segments
{
    public class DQTSegment : IJPGSegment
    {
        public ushort Marker { get => SegmentMarker.DQT; }
        public ushort Length;
        public byte QTY = 0;
        public byte QTCbCr = 0;
        public BitWriter Writer;
        public byte[] YQCoeffs = new byte[64 * (0 + 1)];    // precision in byte + 1
        public byte[] CbCrQCoeffs = new byte[64 * (0 + 1)]; // precision in byte + 1

        public static readonly byte[,] defaultYQuantTable = new byte[8, 8]
        {
            {16, 11, 10, 16,  24,  40,  51,  61 },
            {12, 12, 14, 19,  26,  58,  60,  55 },
            {14, 13, 16, 24,  40,  57,  69,  56 },
            {14, 17, 22, 29,  51,  87,  80,  62 },
            {18, 22, 37, 56,  68,  109, 103, 77 },
            {24, 35, 55, 64,  81,  104, 113, 92 },
            {49, 64, 78, 87,  103, 121, 120, 101},
            {72, 92, 95, 98,  112, 100, 103, 99 }
        };

        public static readonly byte[,] defaultCbCrQuantTable = new byte[8, 8]
        {
            {17, 18, 24, 47,  99, 99, 99, 99},
            {18, 21, 26, 66,  99, 99, 99, 99},
            {24, 26, 56, 99,  99, 99, 99, 99},
            {47, 66, 99, 99,  99, 99, 99, 99},
            {99, 99, 99, 99,  99, 99, 99, 99},
            {99, 99, 99, 99,  99, 99, 99, 99},
            {99, 99, 99, 99,  99, 99, 99, 99},
            {99, 99, 99, 99,  99, 99, 99, 99}
        };

        public DQTSegment(BitWriter writer) 
            : this(writer, 
                   ZickZackSampleChunk(defaultYQuantTable,    0, 0).ToArray(),
                   ZickZackSampleChunk(defaultCbCrQuantTable, 0, 0).ToArray())
        {
            
        }

        public DQTSegment(BitWriter writer,
                          byte[] zickZackQuantCoeffsY,
                          byte[] zickZackQuantCoeffsCbCr)
        {
            Writer = writer;
            YQCoeffs = zickZackQuantCoeffsY;
            CbCrQCoeffs = zickZackQuantCoeffsCbCr;

            Length = (ushort)(2    // Length
                            + 1    // YQTInfo
                            + 64   // AmountOfQuantCoeffsForY
                            + 1    // CbCrQTInfo
                            + 64); // AmountOfQuantCoeffsForCbCr
        }

        public void WriteSegment()
        {
            Writer.WriteUShort(Marker);
            Writer.WriteUShort(Length);

            // DQT Y Info byte
            Writer.Write(0x0, offset: sizeof(byte) * 8 - 4, count: 4); // [Precision 8bit (0b0)] LSB: bit 8..5 == bit 4..7 (auf folie)
            Writer.Write(0x0, offset: sizeof(byte) * 8 - 4, count: 4); // [Number of QT]         LSB: bit 4..1 == bit 0..3 (auf folie)
            Writer.WriteBytes(Array.ConvertAll(YQCoeffs, item => (byte)item)); // Y Quantisierungskoeffizienten Zick-Zack sorted!

            // DQT CbCr Info byte
            Writer.Write(0x0, offset: sizeof(byte) * 8 - 4, count: 4); // [Precision 8bit (0b0)] LSB: bit 8..5 == bit 4..7 (auf folie)
            Writer.Write(0x1, offset: sizeof(byte) * 8 - 4, count: 4); // [Number of QT]         LSB: bit 4..1 == bit 0..3 (auf folie)
            Writer.WriteBytes(Array.ConvertAll(CbCrQCoeffs, item => (byte)item)); // CbCr Quantisierungskoeffizienten Zick-Zack sorted!
        }
    }
}
