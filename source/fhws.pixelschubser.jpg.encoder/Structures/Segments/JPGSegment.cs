﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fhws.pixelschubser.jpg.encoder.Structures
{
    public interface IJPGSegment
    {
        public ushort Marker { get; }
        public void WriteSegment();
    }

    public static class SegmentMarker
    {
        public const ushort SOI = 0xffd8;
        public const ushort EOI = 0xffd9;
        public const ushort APP0 = 0xffe0;
        public const ushort SOF0 = 0xffc0;
        public const ushort DHT = 0xffc4;
        public const ushort DQT = 0xffdb;
        public const ushort SOS = 0xffda;
    }
}
