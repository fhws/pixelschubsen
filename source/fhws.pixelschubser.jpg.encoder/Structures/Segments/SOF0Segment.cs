﻿using System;

namespace fhws.pixelschubser.jpg.encoder.Structures.Segments
{
    public class SOF0Segment : IJPGSegment
    {
        private BitWriter Writer;
        private ushort SizeX;
        private ushort SizeY;
        private SampleReduceRate SubsamplingRate;
        private byte ColorDepth;
        private bool GreyPicture;
        private byte AmountComponents;
        public ushort Marker { get => SegmentMarker.SOF0; }
        ushort Length = 0;

        public SOF0Segment(BitWriter writer, ushort sizeX, ushort sizeY, 
                           SampleReduceRate subsamplingRate, 
                           byte colorDepth = 8, bool greyPicture = false)
        {
            if (sizeX == 0 || sizeY == 0)
                throw new ArgumentOutOfRangeException(
                    "size X or size Y can not be 0");

            SizeX = sizeX;
            SizeY = sizeY;
            SubsamplingRate = subsamplingRate;
            ColorDepth = colorDepth;
            Writer = writer;
            GreyPicture = greyPicture;

            AmountComponents = (byte)(greyPicture ? 1 : 3);
            
            Length = (ushort)(2 // Length
                            + 1 // ColorDepth
                            + 2 // SizeX
                            + 2 // SizeY
                            + 1 // AmountComponents
                            + AmountComponents * 3); // Per component: ID, SubSampling, QuantTable
        }

        public void WriteSegment()
        {
            Writer.WriteUShort(Marker);
            Writer.WriteUShort(Length);
            Writer.WriteByte(ColorDepth);
            Writer.WriteUShort(SizeX);
            Writer.WriteUShort(SizeY);
            Writer.WriteByte(AmountComponents);

            // Y component
            Writer.WriteByte(0x01); // Component ID
            Writer.WriteByte(0x22); // No subsampling
            Writer.WriteByte(0x00); // Quantisation table

            if(!GreyPicture)
            {
                // CB component
                Writer.WriteByte(0x02); // Component ID
                Writer.WriteByte(0x11); // No subsampling
                Writer.WriteByte(0x01); // Quantisation table
                // TODO: use 0x01 (CBCR TABLE)

                // CR component
                Writer.WriteByte(0x03); // Component ID
                Writer.WriteByte(0x11); // No subsampling
                Writer.WriteByte(0x01); // Quantisation table
            }
        }
    }
}
