﻿namespace fhws.pixelschubser.jpg.encoder.Structures.Segments
{
    public class SOSSegment : IJPGSegment
    {
        public ushort Marker { get => SegmentMarker.SOS; }
        public ushort Length;
        public BitWriter Writer;
        private byte componentCount;
        private byte yDCHuffTableIdx;
        private byte yACHuffTableIdx;
        private byte CbDCHuffTableIdx;
        private byte CbACHuffTableIdx;
        private byte CrDCHuffTableIdx;
        private byte CrACHuffTableIdx;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="componentCount">default is 3 for Y,Cb,Cr 
        /// if you want to use Greyscale: choose 1 for only Y</param>
        public SOSSegment(BitWriter writer, byte componentCount = 3,
                          byte yDCHuffTableIdx  = 0, byte yACHuffTableIdx  = 0,
                          byte CbDCHuffTableIdx = 1, byte CbACHuffTableIdx = 1,
                          byte CrDCHuffTableIdx = 1, byte CrACHuffTableIdx = 1)
        {
            Writer = writer;
            this.componentCount   = componentCount;
            this.yDCHuffTableIdx  = yDCHuffTableIdx;
            this.yACHuffTableIdx  = yACHuffTableIdx;
            this.CbDCHuffTableIdx = CbDCHuffTableIdx;
            this.CbACHuffTableIdx = CbACHuffTableIdx;
            this.CrDCHuffTableIdx = CrDCHuffTableIdx;
            this.CrACHuffTableIdx = CrACHuffTableIdx;

            Length = (ushort)(6 + 2 * componentCount);
        }

        public void WriteSegment()
        {
            Writer.WriteUShort(Marker);
            Writer.WriteUShort(Length);
            Writer.WriteByte(componentCount); // Component Count

            Writer.WriteByte(1); // Component index same as in SOF0
            Writer.Write(yDCHuffTableIdx, offset: sizeof(byte) * 8 - 4, count: 4); // bit 4..7 (auf folie)
            Writer.Write(yACHuffTableIdx, offset: sizeof(byte) * 8 - 4, count: 4); // bit 0..3 (auf folie)

            if (componentCount == 3)
            {
                Writer.WriteByte(2); // Component index same as in SOF0
                Writer.Write(CbDCHuffTableIdx, offset: sizeof(byte) * 8 - 4, count: 4); // bit 4..7 (auf folie)
                Writer.Write(CbACHuffTableIdx, offset: sizeof(byte) * 8 - 4, count: 4); // bit 0..3 (auf folie)

                Writer.WriteByte(3); // Component index same as in SOF0
                Writer.Write(CrDCHuffTableIdx, offset: sizeof(byte) * 8 - 4, count: 4); // bit 4..7 (auf folie)
                Writer.Write(CrACHuffTableIdx, offset: sizeof(byte) * 8 - 4, count: 4); // bit 0..3 (auf folie)
            }

            Writer.WriteBytes(new byte[]{ 0x00, 0x3f, 0x00 }); // bytes with no use for us

            // TODO: Write data with special case: 0xff has to be replaced by 0xff 0x00 
        }
    }
}
