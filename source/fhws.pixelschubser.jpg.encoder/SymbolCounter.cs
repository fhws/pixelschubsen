using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace fhws.pixelschubser.jpg.encoder
{
    public class SymbolCounter<TSymbol>
    {
        private readonly Dictionary<TSymbol, int> _Result = new Dictionary<TSymbol, int>();
        public virtual Dictionary<TSymbol, int> Result => _Result;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Count(TSymbol symbol)
        {
            if (!_Result.ContainsKey(symbol))
            {
                _Result.Add(symbol, 1);
                return;
            }

            _Result[symbol]++;
        }

        public void Reset()
        {
            _Result.Clear();
        }
    }
}
