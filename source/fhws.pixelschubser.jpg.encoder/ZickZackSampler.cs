﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fhws.pixelschubser.jpg.encoder
{
    public static class ZickZackSampler
    {
        const int CHUNK_SIZE_X = 8;
        const int CHUNK_SIZE_Y = 8;

        public static List<T>[,] ZickZackSampleAll<T>(T[,] data)
        {
            var outSizeX = data.GetLength(1) / CHUNK_SIZE_X;
            var outSizeY = data.GetLength(0) / CHUNK_SIZE_Y;

            var outp = new List<T>[outSizeY, outSizeX];

            var offsetX = 0;
            var offsetY = 0;
            for (var outY = 0; outY < outSizeY; outY++, offsetY += CHUNK_SIZE_Y)
            {
                offsetX = 0;
                for (var outX = 0; outX < outSizeX; outX++, offsetX += CHUNK_SIZE_X)
                {
                    outp[outY, outX] = ZickZackSampleChunk(data, offsetX, offsetY);
                }
            }

            return outp;
        }

        public static List<T> ZickZackSampleChunk<T>(T[,] array, int offsetX,
                                                     int offsetY) {
            List<T> ret = new List<T>();

            ret.Add(array[offsetY + 0, offsetX + 0]);

            ret.AddRange(DiagDown(ref array, offsetX, offsetY, X: 1, XMax: 0));
            ret.AddRange(DiagUp(ref array  , offsetX, offsetY, Y: 2, YMax: 0));

            ret.AddRange(DiagDown(ref array, offsetX, offsetY, X: 3, XMax: 0));
            ret.AddRange(DiagUp(ref array,   offsetX, offsetY, Y: 4, YMax: 0));

            ret.AddRange(DiagDown(ref array, offsetX, offsetY, X: 5, XMax: 0));
            ret.AddRange(DiagUp(ref array,   offsetX, offsetY, Y: 6, YMax: 0));

            ret.AddRange(DiagDown(ref array, offsetX, offsetY, X: 7, XMax: 0));

            // diag point

            ret.AddRange(DiagUp(ref array,   offsetX, offsetY, Y: 7, YMax: 1, X: 1));
            ret.AddRange(DiagDown(ref array, offsetX, offsetY, X: 7, XMax: 2, Y: 2));

            ret.AddRange(DiagUp(ref array,   offsetX, offsetY, Y: 7, YMax: 3, X: 3));
            ret.AddRange(DiagDown(ref array, offsetX, offsetY, X: 7, XMax: 4, Y: 4));

            ret.AddRange(DiagUp(ref array,   offsetX, offsetY, Y: 7, YMax: 5, X: 5));
            ret.AddRange(DiagDown(ref array, offsetX, offsetY, X: 7, XMax: 6, Y: 6));

            ret.Add(array[offsetY + 7, offsetX + 7]);

            return ret;
        }

        private static List<T> DiagUp<T>(ref T[,] array,
                                        int offsetX, int offsetY,
                                        int Y, int YMax,
                                        int X = 0) {
            List<T> retList = new List<T>();
            while (Y >= YMax) retList.Add(array[offsetY + Y--, offsetX + X++]);
            return retList;
        }

        private static List<T> DiagDown<T>(ref T[,] array,
                                          int offsetX, int offsetY,
                                          int X, int XMax,
                                          int Y = 0) {
            List<T> retList = new List<T>();
            while (X >= XMax) retList.Add(array[offsetY + Y++, offsetX + X--]);
            return retList;
        }
    }
}
